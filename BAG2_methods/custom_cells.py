import os
import subprocess
import pdb
import ast
from bag.layout.util import BBox

class CustomCell():
    '''
    Interface for including analog macros into BAG generators.

    Parameters:
    cellname: str
        Name of cell to be imported
    libname: str
        Name of library from which cell is to be imported
    modulepath: str
        Path of master module (the generator)
    use_cli: bool
        True to parse LEF with command line interface. Currently only supported option
    '''
    def __init__(self, cellname, libname, resolution, use_cli=False):
        try:
            self.work_dir=os.environ['BAG_WORK_DIR']
        except KeyError:
            print("BAG_WORK_DIR env. variable not set! Exiting")
            exit()
        self.libname=libname
        self.cellname=cellname
        self.resolution=resolution
        self.has_cli_interface=use_cli

    def get_immutable_key(self):
        '''
        This function exists to support passing this class as a parameter to BAG
        '''
        return (self.__class__.__name__, self.libname, self.cellname)

    
    @property
    def read_lef(self):
        '''
        If true, read in the LEF and export pins to Python. If False,
        try to open previously read pins from self.file. If self.file doesn't exist, read
        LEF.
        '''
        if not hasattr(self, '_read_lef'):
            self._read_lef=False
        return self._read_lef

    @read_lef.setter
    def read_lef(self, val):
        self._read_lef=val


    @property
    def pins(self):
        '''
        Dictionary mapping port labels to their bounding boxes.
        '''
        if not hasattr(self, '_pins'):
            self._pins=self.import_pins()
        return self._pins
    @pins.setter
    def pins(self, val):
        self._pins=val


    @property
    def path(self):
        '''
        Path to output file, which stores the pin information in Pythonic format
        '''
        if not hasattr(self, '_path'):
            self._path = '%s_%s_pins.txt' % (self.libname, self.cellname)
        return self._path

    @path.setter
    def path(self, val):
        self._path=val


    def lef2py(self):
        '''
        Parse LEF file to Python
        '''
        if self.has_cli_interface:
            cwd = os.getcwd()
            os.chdir(self.work_dir) 
            print("Parsing LEF for cell %s in library %s. This may take a while!" % (self.libname, self.cellname))
            ret=subprocess.check_output('./shell/lef2py.sh -l %s -c %s' % (self.libname, self.cellname), shell=True).decode('utf-8')
            os.chdir(cwd)
            pins = {}
            replaceables = ['\n', ')', '(']
            path = self.path
            with open(path, 'w') as f:
                if ret:
                    print("Parsed LEF succesfully! Parsing pins...")
                    layer=None
                    for i,line in enumerate(ret.split('\n')):
                        if line:
                            parts = line.split(':')
                            if len(parts) == 2:
                                coords = []
                                if parts[0] == 'BBox':
                                    pbox = parts[1]
                                    for r in replaceables:
                                        pbox = pbox.replace(r,'')
                                    layer=None
                                    coords.append(tuple(map(float,pbox.split(','))))
                                else:
                                    layer= parts[1].split(',')[0]
                                    pboxes = parts[1][len(layer)+1:].split('),(')
                                    for pbox in pboxes:
                                        for r in replaceables:
                                            pbox = pbox.replace(r,'')
                                        coords.append(tuple(map(float,pbox.split(','))))
                                label=parts[0]
                                if isinstance(self.resolution, float):
                                    info = (layer, [BBox(*box, self.resolution) for box in coords])
                                else:
                                    info = (layer, [box for box in coords])
                                f.write('%s:%s:%s\n' % (label,layer, ':'.join(['(%s,%s,%s,%s)' % box for box in coords])))
                                pins[label] = info
                            else:
                                print("WARN: Line %d contained too many elements\
                                        (%d, expected 2)! Omitting line." % (i,len(parts)))
                    print("Pins parsed.")
                else:
                    print("LEF parsing failed.")
            self.pins=pins
            return pins
        else:
            raise Exception("Implement LEF parser with python! CLI currently supported.")

    def import_pins(self):
        '''
        Import pins and device dimensions from plain text to self.pins dictionary.

        NOTE: This assumes that lef parser outputs pins coordinates in layout units. Is this always
        the case?
        '''
        path = self.path
        if self.read_lef:
            return self.lef2py() 
        else:
            print("read_lef was False. Trying to open pin info from file %s" % os.path.abspath(path))
            if os.path.exists(path) and os.path.getsize(path) > 0:
                pins = {}
                print("Found existing pin info file!")
                with open(path, 'r') as f:
                    for line in f.readlines():
                        parts = line.split(':')
                        label = parts[0]
                        layer = parts[1]
                        boxes = parts[2:]
                        boxes = [ast.literal_eval(box.strip('\n')) for box in boxes] 
                        if label not in pins.keys():
                            if isinstance(self.resolution, float):
                                pins[label] = (layer, [BBox(*box, self.resolution) for box in boxes])
                            else:
                                pins[label] = (layer, [box for box in boxes])
                        else:
                            if isinstance(self.resolution, float):
                                pins.update({label : (layer, [BBox(*box, self.resolution) for box in boxes])})
                            else:
                                pins.update({label : (layer, [box for box in boxes])})
                self.pins=pins
                return pins
            else:
                print("Couldn't find existing pin file. Reading LEF")
                return self.lef2py()

    def replace_custom_cell_in_schematic(self, sch, instname, term_map, libname=None, cellname=None):
        '''
        Conveience function for substituting the custom_cell with symbol of
        analog macro into schematic.

        Parameters:
        sch: bag.design.Module
            Instance of the module class (schematic generator)
        instname: str
            Schematic instance to be replaced
        term_map: Dict[str,str]
            Dictionary mapping analog macro pins to schematic nets 
        libname: str
            Library of analog macro. Default: self.libname
        cellname: str
            Name of analog macro cell. Default: self.cellname
        '''
        if not libname:
            libname=self.libname
        if not cellname:
            cellname=self.cellname
        sch.replace_instance_master(instname, libname, cellname, static=True)
        for term, net in term_map.items():
            sch.reconnect_instance_terminal(instname, term, net)

