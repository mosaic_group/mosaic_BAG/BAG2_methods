#! /usr/bin/env python3

"""This module define utility classes for distinguishing between virtuoso / opensource DB based flows.
"""

from __future__ import annotations
from enum import Enum
import os
from typing import *
import unittest
import yaml

# -------------------------------------- Enums -------------------------------------


class DbAccessClass(str, Enum):
    OPEN_SOURCE = "opensource.bag.interface.opensource.OpenSourceInterface"
    VIRTUOSO = "bag.interface.skill.SkillInterface"


class BAGFlowVariant(str, Enum):
    BAG2_OPEN_SOURCE = 'opensource'
    BAG2_VIRTUOSO = 'virtuoso'

    @classmethod
    def from_bag_config(cls) -> BAGFlowVariant:
        if hasattr(cls, '_cached_variant'):
            return cls._cached_variant

        bag_config_yaml_path = os.environ["BAG_CONFIG_PATH"]
        with open(bag_config_yaml_path, "r") as f:
            config_dict = yaml.load(f, Loader=yaml.FullLoader)
            variant = cls.from_bag_config_dict(config_dict)
            cls._cached_variant = variant
            return variant

    @classmethod
    def from_bag_config_dict(cls, config_dict: Dict[str, Any]) -> BAGFlowVariant:
        db_class = config_dict['database']['class']
        variant: BAGFlowVariant
        match db_class:
            case DbAccessClass.VIRTUOSO:
                variant = BAGFlowVariant.BAG2_VIRTUOSO

            case DbAccessClass.OPEN_SOURCE:
                variant = BAGFlowVariant.BAG2_OPEN_SOURCE

            case _:
                raise ValueError(f"unsupported DbAccess database backend class: {db_class}")
        return variant

# -------------------------------------- Unit Tests -------------------------------------

class TestBAGFlowVariant(unittest.TestCase):
    def test_from_bag_config_dict__opensource(self):
        config_dict: Dict[str, Any] = {
            'database': {
                'class': DbAccessClass.OPEN_SOURCE
            }
        }
        self.assertEquals(BAGFlowVariant.from_bag_config_dict(config_dict), BAGFlowVariant.BAG2_OPEN_SOURCE)

    def test_from_bag_config_dict__virtuoso(self):
        config_dict: Dict[str, Any] = {
            'database': {
                'class': DbAccessClass.VIRTUOSO
            }
        }
        self.assertEquals(BAGFlowVariant.from_bag_config_dict(config_dict), BAGFlowVariant.BAG2_VIRTUOSO)


if __name__ == '__main__':
    unittest.main()
