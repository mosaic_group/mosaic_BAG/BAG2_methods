from bag.design import Module
from bag.math import float_to_si_string

class MOMCapModuleBase(Module):
    """The base design class for a metal-on-metal capacitor.

    Parameters
    ----------
    database : ModuleDB
        the design database object.
    yaml_file : str
        the netlist information file name.
    **kwargs :
        additional arguments
    """

    def __init__(self, database, yaml_file, **kwargs):
        Module.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            w='cap finger width, in meters.',
            l='cap finger length, in meters.',
            nf='number of cap fingers (int).',
            sp='spacing between fingers, in meters.',
            stm='metal start layer',
            spm='metal stop layer',
            )

    def design(self, w=1e-6, l=1e-6, nf=10, sp=1e-6, stm=1, spm=6, sub_type='ptap'):
        pass

    def get_schematic_parameters(self):
        # type: () -> Dict[str, str]
        w = self.params['w']
        l = self.params['l']
        nf = self.params['nf']
        sp = self.params['sp']
        stm = self.params['stm']
        spm = self.params['spm']
        wstr = w if isinstance(w, str) else float_to_si_string(w)
        lstr = l if isinstance(l, str) else float_to_si_string(l)
        nfstr = nf if isinstance(nf, str) else "%s" % int(nf)
        spstr = sp if isinstance(sp, str) else float_to_si_string(sp)
        stmstr = stm if isinstance(stm, str) else "%s" % int(stm)
        spmstr = spm if isinstance(spm, str) else "%s" % int(spm)

        return dict(w=wstr, l=lstr, nf=nfstr, sp=spstr, stm=stmstr, spm=spmstr)

    def get_cell_name_from_parameters(self):
        # type: () -> str
        return 'cap_mom'

    def is_primitive(self):
        # type: () -> bool
        return True

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['w'] == 0 or self.params['l'] == 0 or self.params['nf'] == 0

class AccurateMOMCapModuleBase(Module):
    """The base design class for a accurate metal-on-metal capacitor.

    Parameters
    ----------
    database : ModuleDB
        the design database object.
    yaml_file : str
        the netlist information file name.
    **kwargs :
        additional arguments
    """

    def __init__(self, database, yaml_file, **kwargs):
        Module.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            w='cap finger width, in meters.',
            l='cap finger length, in meters.',
            nf='number of cap fingers (int).',
            sp='spacing between fingers, in meters.',
            stm='metal start layer',
            spm='metal stop layer',
            sub_type='Capacitor substrate type',
            )

    def design(self, w=1e-6, l=1e-6, nf=10, sp=1e-6, stm=1, spm=6, sub_type='ptap'):
        pass

    def get_schematic_parameters(self):
        # type: () -> Dict[str, str]
        w = self.params['w']
        l = self.params['l']
        nf = self.params['nf']
        sp = self.params['sp']
        stm = self.params['stm']
        spm = self.params['spm']
        sub_type = self.params['sub_type']
        wstr = w if isinstance(w, str) else float_to_si_string(w)
        lstr = l if isinstance(l, str) else float_to_si_string(l)
        nfstr = nf if isinstance(nf, str) else "%s" % int(nf)
        spstr = sp if isinstance(sp, str) else float_to_si_string(sp)
        stmstr = stm if isinstance(stm, str) else "%s" % int(stm)
        spmstr = spm if isinstance(spm, str) else "%s" % int(spm)
        sub_type = sub_type

        return dict(w=wstr, l=lstr, nf=nfstr, sp=spstr, stm=stmstr, spm=spmstr, sub_type=sub_type)

    def get_cell_name_from_parameters(self):
        # type: () -> str
        sub_type=self.params['sub_type']
        if sub_type=='ntap':
            return 'cap_mom_accurate_nw'
        else:
            return 'cap_mom_accurate'

    def is_primitive(self):
        # type: () -> bool
        return True

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['w'] == 0 or self.params['l'] == 0 or self.params['nf'] == 0

class StdCellBase(Module):
    """The base design class for standard cells.

    Parameters
    ----------
    database : ModuleDB
        the design database object.
    yaml_file : str
        the netlist information file name.
    **kwargs :
        additional arguments
    """

    def __init__(self, database, yaml_file, **kwargs):
        Module.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            )

    @property
    def static(self):
        self._static=True

    def design(self):
        pass

    def get_schematic_parameters(self):
        # type: () -> Dict[str, str]
        return dict()
    
    def get_cell_name_from_parameters(self):
        raise ValueError('Implement in child class!')


    def is_primitive(self):
        # type: () -> bool
        return True

    def should_delete_instance(self):
        # type: () -> bool
        pass

class StdCellFFModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
            reset=False,
            set=False,
            negated_output=True,
            nonnegated_output=True,
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            strength='Drive strength',
            reset='Generate reset pin?',
            set='Generate with set pin?',
            negated_output='Generate with negated output?',
            nonnegated_output='Generate with negated output?',
            intent='Standard cell intent',
            )

    @property
    def allowed_str(self):
        '''
        List of allowed drive strengths
        '''
        if not hasattr(self, '_allowed_str'):
            self._allowed_str=[0,1,2,4,8]
        return self._allowed_str

    def get_cell_name_from_parameters(self):
        # type: () -> str
        strength = self.params['strength']
        reset = self.params['reset']
        set = self.params['set']
        negated_output = self.params['negated_output']
        nonnegated_output = self.params['nonnegated_output']
        intent = self.params['intent']
        intent = '' if intent=='standard' else intent
        if negated_output and nonnegated_output:
            postfix=''
        elif negated_output and not nonnegated_output:
            postfix='_qn'
        elif not negated_output and nonnegated_output:
            postfix='_q'
        else:
            raise ValueError("negated_output and nonnegated_output cannot both be False!")   
        if reset and set:
            extra_pins = 'rbsb'
        elif reset and not set:
            extra_pins = 'rb'
        elif not reset and set:
            extra_pins = 'sb'
        else:
            extra_pins= ''
        if not isinstance(strength, int):
            if isinstance(strength, str):
                strength=int(strength)
            else:
                raise ValueError('Parameter strength should be integer or string')
        if strength not in self.allowed_str:
            raise ValueError("Drive strength must be in %s" % self.allowed_str)
        return 'ff%s_%sx%s' % (extra_pins,strength,postfix) if intent=='' else 'ff%s_%sx_%s_%s' % (extra_pins,celltype,strength,postfix,intent)

    def design(self,strength='1', reset=False, set=False, negated_output=True, nonnegated_output=True, intent=''):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['strength'] == 0 or self.params['strength'] == '0'

class StdCellMuxModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            strength='Drive strength',
            intent='Standard cell intent',
            )

    @property
    def allowed_str(self):
        '''
        List of allowed drive strengths
        '''
        if not hasattr(self, '_allowed_str'):
            self._allowed_str=[0,1,2,4,6,8]
        return self._allowed_str

    @allowed_str.setter
    def allowed_str(self, val):
        self._allowed_str=val


    def get_cell_name_from_parameters(self):
        # type: () -> str
        strength = self.params['strength']
        if not isinstance(strength, int):
            if isinstance(strength, str):
                strength=int(strength)
            else:
                raise ValueError('Parameter strength should be integer or string')
        intent = self.params['intent']
        intent = '' if intent=='standard' else intent
        if strength not in self.allowed_str:
            raise ValueError("Drive strength must be in %s" % self.allowed_str)
        else:
            return 'mux2_%sx' % strength if intent=='' else 'mux2_%sx_%s' % (strength,intent)

    def design(self,strength='1',intent=''):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return int(self.params['strength']) not in self.allowed_str


class StdCellAndModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
                num_input=2
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            strength='Drive strength',
            num_input='Number of inputs',
            intent='Standard cell intent',
            )
    
    @property
    def allowed_num_inputs(self):
        if not hasattr(self, '_allowed_num_inputs'):
            self._allowed_num_inputs=[2,3]
        return self._allowed_num_inputs

    @property
    def allowed_str(self):
        '''
        List of allowed drive strengths
        '''
        if not hasattr(self, '_allowed_str'):
            self._allowed_str=[0,1,2,4,6,8,12,16]
        return self._allowed_str

    @allowed_str.setter
    def allowed_str(self, val):
        self._allowed_str=val
    
    def get_cell_name_from_parameters(self):
        # type: () -> str
        strength = self.params['strength']
        num_input = self.params['num_input']
        intent = self.params['intent']
        intent = '' if intent=='standard' else intent
        if not isinstance(num_input, int):
            if isinstance(num_input, str):
                num_input=int(num_input)
            else:
                raise ValueError('Parameter num_input should be integer or string')
        if not isinstance(strength, int):
            if isinstance(strength, str):
                strength=int(strength)
            else:
                raise ValueError('Parameter strength should be integer or string')
        if strength not in self.allowed_str:
            raise ValueError("Drive strength must be in %s" % self.allowed_str)
        if num_input not in self.allowed_num_inputs:
            raise ValueError("Drive strength must be in %s" % self.allowed_str)
        else:
            return 'and%s_%sx' % (num_input, strength) if intent=='' else 'and%s_%sx_%s' % (num_input,strength,intent)

    def design(self,strength='1',num_input='2',intent=''):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return int(self.params['strength']) not in self.allowed_str

class StdCellIAndModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        '''
        Design module for a NAND standard cell with one inverted input
        '''
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            strength='Drive strength',
            num_input='Number of inputs',
            intent='Standard cell intent',
            )

    @property
    def allowed_num_inputs(self):
        if not hasattr(self, '_allowed_num_inputs'):
            self._allowed_num_inputs=[2,3]
        return self._allowed_num_inputs

    @property
    def allowed_str(self):
        '''
        List of allowed drive strengths
        '''
        if not hasattr(self, '_allowed_str'):
            self._allowed_str=[0,1,2,4,8,16]
        return self._allowed_str

    def get_cell_name_from_parameters(self):
        # type: () -> str
        strength = self.params['strength']
        num_input = self.params['num_input']
        intent = self.params['intent']
        intent = '' if intent=='standard' else intent
        if not isinstance(num_input, int):
            if isinstance(num_input, str):
                num_input=int(num_input)
            else:
                raise ValueError('Parameter num_input should be integer or string')
        if not isinstance(strength, int):
            if isinstance(strength, str):
                strength=int(strength)
            else:
                raise ValueError('Parameter strength should be integer or string')
        if strength not in self.allowed_str:
            raise ValueError("Drive strength must be in %s" % self.allowed_str)
        if num_input not in self.allowed_num_inputs:
            raise ValueError("Drive strength must be in %s" % self.allowed_str)
        else:
            return 'iand%s_%sx' % (num_input, strength) if intent=='' else 'iand%s_%sx_%s' % (num_input,strength,intent)

    def design(self,strength='1',num_input='2',intent=''):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return int(self.params['strength']) not in self.allowed_str

class StdCellNandModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        '''
        Design module for a NAND standard cell.
        '''
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            strength='Drive strength',
            num_input='Number of inputs',
            intent='Standard cell intent',
            )

    @property
    def allowed_num_inputs(self):
        if not hasattr(self, '_allowed_num_inputs'):
            self._allowed_num_inputs=[2,3]
        return self._allowed_num_inputs

    @property
    def allowed_str(self):
        '''
        List of allowed drive strengths
        '''
        if not hasattr(self, '_allowed_str'):
            self._allowed_str=[0,1,2,4,8,16]
        return self._allowed_str

    def get_cell_name_from_parameters(self):
        # type: () -> str
        strength = self.params['strength']
        num_input = self.params['num_input']
        intent = self.params['intent']
        intent = '' if intent=='standard' else intent
        if not isinstance(num_input, int):
            if isinstance(num_input, str):
                num_input=int(num_input)
            else:
                raise ValueError('Parameter num_input should be integer or string')
        if not isinstance(strength, int):
            if isinstance(strength, str):
                strength=int(strength)
            else:
                raise ValueError('Parameter strength should be integer or string')
        if strength not in self.allowed_str:
            raise ValueError("Drive strength must be in %s" % self.allowed_str)
        if num_input not in self.allowed_num_inputs:
            raise ValueError("Drive strength must be in %s" % self.allowed_str)
        else:
            return 'nand%s_%sx' % (num_input, strength) if intent=='' else 'nand%s_%sx_%s' % (num_input,strength,intent)

    def design(self,strength='1', num_input='2',intent=''):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return int(self.params['strength']) not in self.allowed_str

class StdCellBufModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            strength='Drive strength',
            intent='Standard cell intent',
            )

    def get_cell_name_from_parameters(self):
        # type: () -> str
        strength = self.params['strength']
        intent = self.params['intent']
        intent = '' if intent=='standard' else intent
        if not isinstance(strength, int):
            if isinstance(strength, str):
                strength=int(strength)
            else:
                raise ValueError('Parameter strength should be integer or string')
        allowed = [0,1,2,3,4,6,8,12,16,20,24]

        if strength not in allowed:
            raise ValueError("Drive strength must be in %s" % allowed)
        else:
            return 'buf_%sx' % strength if intent=='' else 'buf_%sx_%s' % (strength,intent)

    def design(self,strength='0',intent=''):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['strength'] == -1 or self.params['strength'] == '-1'

class StdCellInvModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            strength='Drive strength',
            intent='Standard cell intent',
            )

    def get_cell_name_from_parameters(self):
        # type: () -> str
        strength = self.params['strength']
        intent = self.params['intent']
        intent = '' if intent=='standard' else intent
        if not isinstance(strength, int):
            if isinstance(strength, str):
                strength=int(strength)
            else:
                raise ValueError('Parameter strength should be integer or string')
        allowed = [0,1,2,4,6,8,16,20]

        if strength not in allowed:
            raise ValueError("Drive strength must be in %s" % allowed)
        else:
            return 'inv_%sx' % strength if intent=='' else 'inv_%sx_%s' % (strength,intent)

    def design(self,strength='0',intent=''):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['strength'] == -1 or self.params['strength'] == '-1'

class StdCellDecapModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            size='Decap size',
            intent='Standard cell intent',
            )

    def get_cell_name_from_parameters(self):
        # type: () -> str
        size = self.params['size']
        intent = self.params['intent']
        intent = '' if intent=='standard' else intent
        if not isinstance(size, int):
            if isinstance(size, str):
                size=int(size)
            else:
                raise ValueError('Parameter size should be integer or string')
        allowed = [1,3,4,5,8,9,16,32,64]

        if size not in allowed:
            raise ValueError("Decap size must be in %s" % allowed)
        else:
            return 'decap_%sx' % size if intent=='' else 'decap_%sx_%s' % (size,intent)

    def design(self,size='0',intent=''):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['size'] == 0 or self.params['size'] == '0'

class BoundaryModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            boundary='Which boundary (e.g. left/right/bot_left/top_right)',
            intent='Standard cell intent',
            )

    def get_cell_name_from_parameters(self):
        # type: () -> str
        boundary = self.params['boundary']
        intent = self.params['intent']
        intent = '' if intent=='standard' else intent
        cellname='boundary'
        allowed_boundaries=['left','right','left_bot','left_top','right_bot','right_top','top','bot']
        if f'{boundary}' in allowed_boundaries:
            cellname+=f'_{boundary}'
        else:
            raise ValueError(f"Boundary type '{boundary}' not allowed.") 
        return cellname if intent=='' else f'{cellname}_{intent}'

    def design(self,boundary='',intent=''):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return False


class WelltapModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    def get_cell_name_from_parameters(self):
        return 'welltap'

    def design(self):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return False

class SwitchIdealModuleBase(Module):
    def __init__(self, database, yaml_file, **kwargs):
        Module.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
                ropen='Switch open resistance in Ohms',
                rclosed='Switch closed resistance in Ohms',
                vopen='Voltage applied between control terminals to open switch (Volts)',
                vclosed='Voltage applied between control terminals to close switch (Volts)',
            )

    def design(self, ropen=10e9, rclosed=10, vopen=0, vclosed=5):
        pass

    def get_schematic_parameters(self):
        # type: () -> Dict[str, str]
        ropen = self.params['ropen']
        rclosed = self.params['rclosed']
        vopen = self.params['vopen']
        vclosed = self.params['vclosed']
        ropen = ropen if isinstance(ropen, str) else float_to_si_string(ropen)
        rclosed = rclosed if isinstance(rclosed, str) else float_to_si_string(rclosed)
        vopen = vopen if isinstance(vopen, str) else float_to_si_string(vopen)
        vclosed = vclosed if isinstance(vclosed, str) else float_to_si_string(vclosed)

        return dict(ropen=ropen, rclosed=rclosed, vopen=vopen, vclosed=vclosed)

    def get_cell_name_from_parameters(self):
        # type: () -> str
        return 'switch_ideal'

    def is_primitive(self):
        # type: () -> bool
        return True

    def should_delete_instance(self):
        # type: () -> bool
        return False

class CapIdealModuleBase(Module):
    def __init__(self, database, yaml_file, **kwargs):
        Module.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
                cap='Capacitance in Farads',
            )

    def design(self, cap=1e-12):
        pass

    def get_schematic_parameters(self):
        # type: () -> Dict[str, str]
        cap = self.params['cap']
        cap = cap if isinstance(cap, str) else float_to_si_string(cap)

        return dict(cap=cap)

    def get_cell_name_from_parameters(self):
        # type: () -> str
        return 'cap_ideal'

    def is_primitive(self):
        # type: () -> bool
        return True

    def should_delete_instance(self):
        # type: () -> bool
        return False

class ResIdealModuleBase(Module):
    def __init__(self, database, yaml_file, **kwargs):
        Module.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
                res='Resistance in Ohms',
            )

    def design(self, res=50):
        pass

    def get_schematic_parameters(self):
        # type: () -> Dict[str, str]
        res = self.params['res']
        res = res if isinstance(res, str) else float_to_si_string(res)

        return dict(res=res)

    def get_cell_name_from_parameters(self):
        # type: () -> str
        return 'res_ideal'

    def is_primitive(self):
        # type: () -> bool
        return True

    def should_delete_instance(self):
        # type: () -> bool
        return False

class ResPolyBase(Module):
    """The base design class for a poly resistor.

    Parameters
    ----------
    database : ModuleDB
        the design database object.
    yaml_file : str
        the netlist information file name.
    **kwargs :
        additional arguments
    """

    def __init__(self, database, yaml_file, **kwargs):
        Module.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        """Returns a dictionary containing default parameter values.
        Override this method to define default parameter values.  As good practice,
        you should avoid defining default values for technology-dependent parameters
        (such as channel length, transistor width, etc.), but only define default
        values for technology-independent parameters (such as number of tracks).
        Returns
        -------
        default_params : Dict[str, Any]
            dictionary of default parameter values.
        """
        return dict(
                ser=1,
                bp=3,
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            w='cap finger width, in meters.',
            l='cap finger length, in meters.',
            ser='number of resistor stripes.',
            bp='backplate designation',
            )

    def design(self, w=1e-6, l=1e-6, ser=1, bp=3):
        pass

    def get_schematic_parameters(self):
        # type: () -> Dict[str, str]
        w = self.params['w']
        l = self.params['l']
        ser = self.params['ser']
        bp = self.params['bp']
        wstr = w if isinstance(w, str) else float_to_si_string(w)
        lstr = l if isinstance(l, str) else float_to_si_string(l)
        serstr = ser if isinstance(ser, str) else float_to_si_string(ser)
        bpstr = bp if isinstance(bp, str) else float_to_si_string(bp)
        return dict(w=wstr, l=lstr, ser=serstr, bp=bpstr)

    def get_cell_name_from_parameters(self):
        # type: () -> str
        return 'res_standard_accurate'

    def is_primitive(self):
        # type: () -> bool
        return True

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['w'] == 0 or self.params['l'] == 0

class TlineIdealModuleBase(Module):
    def __init__(self, database, yaml_file, **kwargs):
        Module.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
                res='Characteristic impedance in ohms',
                frequency='Transmitted signal frequency in Hz',
                length='Normalized transmission line length (electrical)',
            )

    def design(self, res=50, frequency=1e9, length=0.25):
        pass

    def get_schematic_parameters(self):
        # type: () -> Dict[str, str]
        res = self.params['res']
        res = res if isinstance(res, str) else float_to_si_string(res)
        frequency = self.params['frequency']
        frequency = frequency if isinstance(frequency, str) else float_to_si_string(frequency)
        length = self.params['length']
        length = length if isinstance(length, str) else float_to_si_string(length)

        return dict(res=res,frequency=frequency,length=length)

    def get_cell_name_from_parameters(self):
        # type: () -> str
        return 'tline_ideal'

    def is_primitive(self):
        # type: () -> bool
        return True

    def should_delete_instance(self):
        # type: () -> bool
        return False

class ADCIdealModuleBase(Module):
    '''
    Design module for ideal ADC Verilog-A model.

    TODOs: parameterize number of bits! This seems to possible, if cell is made into
    a PCell.
    '''
    def __init__(self, database, yaml_file, **kwargs):
        Module.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
                tdel='Delay from falling clock edge to output transition in seconds',
                tr='Output risetime in seconds',
                tf='Output falltime in seconds',
                logic_high='Output logic high level',
                logic_low='Output logic low level',
                vref='ADC reference voltage',
                vtrans_clk='Voltage level at which clock edge is registered',
                voffs='Offset voltage of input signal w.r.t zero',
                differential='Is the ADC differential?',
            )

    def design(self, tdel=0, tr=0, tf=0, logic_high=5, logic_low=0, vref=1, vtrans_clk=2.5, voffs=0, differential=False):
        pass

    def get_schematic_parameters(self):
        # type: () -> Dict[str, str]
        tdel = self.params['tdel']
        tr = self.params['tr']
        tf = self.params['tf']
        logic_high = self.params['logic_high']
        logic_low = self.params['logic_low']
        vref = self.params['vref']
        vtrans_clk = self.params['vtrans_clk']
        voffs = self.params['voffs']
        tdel = tdel if isinstance(tdel, str) else float_to_si_string(tdel)
        tr = tr if isinstance(tr, str) else float_to_si_string(tr)
        tf = tf if isinstance(tf, str) else float_to_si_string(tf)
        logic_high = logic_high if isinstance(logic_high, str) else float_to_si_string(logic_high)
        logic_low = logic_low if isinstance(logic_low, str) else float_to_si_string(logic_low)
        vref = vref if isinstance(vref, str) else float_to_si_string(vref)
        vtrans_clk = vtrans_clk if isinstance(vtrans_clk, str) else float_to_si_string(vtrans_clk)
        voffs = voffs if isinstance(voffs, str) else float_to_si_string(voffs)

        return dict(tdel=tdel, trise=tr, tfall=tf, vlogic_high=logic_high, vlogic_low=logic_low, vref=vref, vtrans_clk=vtrans_clk, voffs=voffs)

    def get_cell_name_from_parameters(self):
        # type: () -> str
        if self.params['differential']:
            return 'adc_ideal_diff'
        else:
            return 'adc_ideal'

    def is_primitive(self):
        # type: () -> bool
        return True

    def should_delete_instance(self):
        # type: () -> bool
        return False

class StdCellXorModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        '''
        Design module for a XOR standard cell.
        '''
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            strength='Drive strength',
            num_input='Number of inputs',
            intent='Standard cell intent',
            )

    @property
    def allowed_num_inputs(self):
        if not hasattr(self, '_allowed_num_inputs'):
            self._allowed_num_inputs=[2]
        return self._allowed_num_inputs

    @property
    def allowed_str(self):
        '''
        List of allowed drive strengths
        '''
        if not hasattr(self, '_allowed_str'):
            self._allowed_str=[0,1,2,4,8,16]
        return self._allowed_str

    def get_cell_name_from_parameters(self):
        # type: () -> str
        strength = self.params['strength']
        num_input = self.params['num_input']
        intent = self.params['intent']
        intent = '' if intent=='standard' else intent
        if not isinstance(num_input, int):
            if isinstance(num_input, str):
                num_input=int(num_input)
            else:
                raise ValueError('Parameter num_input should be integer or string')
        if not isinstance(strength, int):
            if isinstance(strength, str):
                strength=int(strength)
            else:
                raise ValueError('Parameter strength should be integer or string')
        if strength not in self.allowed_str:
            raise ValueError("Drive strength must be in %s" % self.allowed_str)
        if num_input not in self.allowed_num_inputs:
            raise ValueError("Drive strength must be in %s" % self.allowed_num_inputs)
        else:
            return 'xor%s_%sx' % (num_input, strength) if intent=='' else 'xor%s_%sx_%s' % (num_input, strength, intent)

    def design(self,strength='1', num_input='2',intent=''):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return int(self.params['strength']) not in self.allowed_str

class StdCellNorModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        '''
        Design module for a NOR standard cell.
        '''
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            strength='Drive strength',
            num_input='Number of inputs',
            intent='Standard cell intent',
            )

    @property
    def allowed_num_inputs(self):
        if not hasattr(self, '_allowed_num_inputs'):
            self._allowed_num_inputs=[2]
        return self._allowed_num_inputs

    @property
    def allowed_str(self):
        '''
        List of allowed drive strengths
        '''
        if not hasattr(self, '_allowed_str'):
            self._allowed_str=[0,1,2,4,8,16]
        return self._allowed_str

    def get_cell_name_from_parameters(self):
        # type: () -> str
        strength = self.params['strength']
        num_input = self.params['num_input']
        intent = self.params['intent']
        if not isinstance(num_input, int):
            if isinstance(num_input, str):
                num_input=int(num_input)
            else:
                raise ValueError('Parameter num_input should be integer or string')
        if not isinstance(strength, int):
            if isinstance(strength, str):
                strength=int(strength)
            else:
                raise ValueError('Parameter strength should be integer or string')
        if strength not in self.allowed_str:
            raise ValueError("Drive strength must be in %s" % self.allowed_str)
        if num_input not in self.allowed_num_inputs:
            raise ValueError("Drive strength must be in %s" % self.allowed_num_inputs)
        else:
            return 'nor%s_%sx' % (num_input, strength) if intent=='' else 'nor%s_%sx_%s' % (num_input, strength, intent)

    def design(self,strength='1', num_input='2', intent=''):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return int(self.params['strength']) not in self.allowed_str
