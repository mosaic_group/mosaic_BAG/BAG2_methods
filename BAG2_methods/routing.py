"""
BAG2 methods --- routing.py
======================

Helper functions for instance routing in layout generators
written with the Berkeley Analog Generator (BAG) framework.

Created on 09.03.2021 by Santeri Porrasmaa, santeri.porrasmaa@aalto.fi

"""

from bag.layout.routing import TrackID
from bag.layout.routing.base import WireArray
from bag.layout.util import BBox 

import pdb

class routing_helper():
    """
    Helper class to aggregate helper functions for routing.

    Parameters
    ----------

    grid : bag.layout.routing.RoutingGrid
        The grid used in the template
    tech_info : bag.layout.core.TechInfo
        TechInfo class used to access process specific parameters

    """
    def __init__(self, grid):
        self.grid = grid 
        self.tech_info = grid.tech_info

    def sort_wires(self, warrs):
        '''
        Sort wires of same direction in ascending order.
        '''
        warr_dirs = set([self.grid.get_direction(warr.track_id.layer_id) for warr in warrs])
        if len(warr_dirs)>1:
            raise ValueError("Sort wires works only on WireArrays of same routing direction")
        return sorted(warrs, key=lambda w: self.grid.track_to_coord(w.track_id.layer_id,
            w.track_id.base_index, unit_mode=True))

    def get_closest_available_track(self, template, lay, tid, lower, upper, search_dir=0, width=1, unit_mode=True, half_tracks=False, max_iters=100, margin=0):
        '''
        Get closest available track with respect to tid by searcing to the positive and negative
        directions. Search scope is limited by max_iters, so that each direction of the search
        will progress max_iters / 2 tracks from the original track (hence searching max_iters tracks
        in total).

        Returns the track_id which is available if found, otherwise returns None.

        Parameters:
        -----------
        
        template: TemplateBase
            the layout template to find the track in
        lay: int
            Layer ID of track
        tid: int, float
            The original track index
        lower: int,float
            The lower coord of desired track
        upper: int,float
            The upper coord of desired track
        search_dir: int
            Direction in which available track will searched for
            negative integers indicate search directions left or down,
            postivie integers indicdate search direction right or up,
            integer zero searches in both directions, starting from right
        width: int
            width of track in number of tracks
        unit_mode: bool
            lower, upper given in resolution units?
        half_tracks: bool
            Allow iteration on half tracks?
        max_iters: int
            Search by max_iters / 2 tracks to either direction
        margin: int
            Desired number of tracks between tid and possible other wires
        '''
        margin = margin*self.grid.get_track_pitch(lay,unit_mode=unit_mode)
        count = 1
        if search_dir > -1:
            dir=True
        else:
            dir=False

        if search_dir==0:
            both_directions=True
        else:
            both_directions=False

        step = 0.5 if half_tracks else 1
        tid_orig = tid
        while not template.is_track_available(lay, tid, lower, upper, width=width, unit_mode=unit_mode,sp=margin) and count < max_iters//2 + 1:
            if dir:
                tid=tid_orig + count*step
            else:
                tid=tid_orig - count*step
            if both_directions:
                dir = not dir
            count += 1
        # Check that we actually found the tracks or if iteration ended due to max_iters constraint.
        if template.is_track_available(lay, tid, lower, upper, width=width, unit_mode=unit_mode):
            return tid
        else:
            return None

    def get_min_via_enc(self, top_lay, wire_w, ext_dir):
        '''
        Minimize via size orthogonal to extension direction
        '''
        via_name = self.tech_info.get_via_name(top_lay-1)
        via_config = self.tech_info.config['via']['via_units'][via_name].square
        #dim = via_config['dim']
        enc = via_config['top_enc']
        dim = via_config['dim']
        sp = via_config['sp']
        for w, enct in zip(enc['w_list'], enc['enc_list']):
            if wire_w <= w:
                break
        #via_ext_dir = 'x' if dir=='x' else 'y' 
        min_idx = 0 if ext_dir == 'y' else 1

        if isinstance(sp[0], list):
            sp_min = min(sp, key=lambda t: t[min_idx])
        else:
            sp_min = sp
        if isinstance(dim[0], list):
            dim_min = min(dim, key=lambda t: t[min_idx])
        else:
            dim_min = dim
        enc_min = min(enct, key=lambda t: t[min_idx])
        return dim_min, sp_min, enc_min


    def connect_helper(self, template, warr, track_id, num_via, ext_mode=0):
        '''
        Helper function to make multicut via connection between warr and track_id.

        Parameters
        ----------
        warr: WireArray
            WireArray to connect to given track_id
        track_id: TrackID
            TrackID to connect to
        num_via: int
            Number of vias to make
        ext_mode: int
            Extension direction of via enclosure to fit num_via. By default,
            extend to both directions along track_id layer direction. If ext_mode
            -1, extend to minus direction (left or down). If ext_mode 1, extend
            to positive direction (right or up).

        '''
        lay1 = warr.track_id.layer_id
        lay2 = track_id.layer_id
        res = self.grid.resolution
        if self.grid.get_direction(lay1) == self.grid.get_direction(lay2):
            raise ValueError("Connect helper works only on orthogonal layers!")
        if abs(lay1-lay2) > 1:
            raise ValueError("Connect helper works only for adjacent layers!")
        dir_warr = self.grid.get_direction(lay1)
        dir_tid = self.grid.get_direction(lay2)
        top_lay = max(lay1, lay2)
        bot_lay = min(lay1, lay2)
        top_lay_name = self.tech_info.get_layer_name(top_lay)
        bot_lay_name = self.tech_info.get_layer_name(bot_lay)
        boxarr = warr.get_bbox_array(self.grid)
        via_boxes = []
        for box in boxarr:
            left, right = (box.left_unit, box.right_unit) if dir_warr == 'y' else track_id.get_bounds(self.grid,True)
            bot, top = track_id.get_bounds(self.grid, True) if dir_warr=='y' else box.bottom_unit, box.top_unit
            via_ext_dir = dir_warr 
            wire_w = box.width_unit if dir_warr == 'y' else box.height_unit
            dim,sp,enc=self.get_min_via_enc(top_lay, wire_w, via_ext_dir)
            if via_ext_dir == 'x':
                via_box_w = num_via*(dim[0]+sp[0]) - sp[0] + 2*enc[0]
                ext = via_box_w - wire_w
                left -= ext // 2
                right += ext // 2
                top += enc[1]
                bot -= enc[1]
            else:
                via_box_h = num_via*(dim[1]+sp[1]) - sp[1] + 2*enc[1]
                ext = via_box_h - wire_w
                bot -= ext // 2
                top += ext // 2
                left -= enc[0]
                right += enc[0]
            via_boxes.append(BBox(left, bot, right, top, res, True))
            template.add_via(via_boxes[-1], bot_lay_name, top_lay_name, bot_dir=dir_warr, top_dir=dir_warr, extend=False)
            template.mark_bbox_used(top_lay, via_boxes[-1])
            template.mark_bbox_used(bot_lay, via_boxes[-1])
        if dir_tid =='y':
            lower = min(via_boxes, key=lambda box: box.bottom_unit).bottom_unit
            upper = max(via_boxes, key=lambda box: box.top_unit).top_unit
        else:
            lower = min(via_boxes, key=lambda box: box.left_unit).left_unit
            upper = max(via_boxes, key=lambda box: box.right_unit).right_unit
        return template.add_wires(track_id.layer_id, track_id.base_index, lower, upper,
                track_id.width, track_id.num, track_id.pitch, unit_mode=True)

    def get_parallel_run_length(self, wire_lower, wire_upper, adj_wire, unit_mode=False):
        """ 
        Helper function to find the parallel run length between a wire to be
        drawn and a wire adjacent to that wire. Assumes that wires are routed
        in the same direction.
        
        Parameters
        ----------
        wire_lower: Union[int, float]
            Lower coordinate of wire to be drawn
        wire_upper : Union[int, float]
            Upper coordinate of wire to be drawn
        adj_wire : WireArray
            WireArray adjacent to the one being drawn
        unit_mode : boolean
            If true, upper and lower coordinates were given in resolution
            units
        
        Returns
        -------
        par_len : Union[int, float]
            Parallel run length of the two wires
        """
        grid = self._grid
        res = grid.resolution
        if not isinstance(adj_wire, WireArray):
            raise Exception('Invalid type %s for argument adj_wires!' % type(adj_wires))
        if not unit_mode:
            wire_upper = int(wire_upper/res)
            wire_lower = int(wire_lower/res)
        if wire_upper < wire_lower:
            raise Exception('Non-physical wire end points given (e.g. upper < lower)!')
        par_len = min(wire_upper, adj_wire.upper_unit) - max(wire_lower, adj_wire.lower_unit) 
        if par_len < 0: # If the run length is negative, the y-projections do not overlap
            par_len = 0
        return par_len if unit_mode else par_len * res

    def get_parallel_run_length_primitive(self, wire_lower, wire_upper, adj_box, direction, unit_mode=False):
        """
        Helper function to find the parallel run length between a wire to be
        drawn and a primitive wire adjacent to that wire.

        Parameters
        ----------
        wire_lower: Union[int, float]
            Lower coordinate of wire to be drawn
        wire_upper : Union[int, float]
            Upper coordinate of wire to be drawn
        adj_box : BBox 
            Bounding box of the primitive wire 
        direction : str
            Routing direction, either 'x' or 'y'.
        unit_mode : boolean
            If true, upper and lower coordinates were given in resolution
            units
        Returns:
        --------
        par_len : Union[int, float]
            Parallel run length of the two wires
        """
        grid = self._grid
        res = grid.resolution
        valid_dirs = set(['x', 'y'])
        if not isinstance(adj_box, BBox):
            raise Exception('Invalid type %s for argument adj_box!' % type(adj_box))
        if direction not in valid_dirs:
            raise Exception('Direction must be either x or y!')
        if wire_upper < wire_lower:
            raise Exception('Non-physical wire end points given (e.g. upper < lower)!')
        if not unit_mode:
            wire_upper = int(wire_upper/res)
            wire_lower = int(wire_lower/res)
        if direction == 'x':
            par_len = min(wire_upper, adj_box.right_unit) - max(wire_lower, adj_box.left_unit) 
        else:
            par_len = min(wire_upper, adj_box.top_unit) - max(wire_lower, adj_box.bottom_unit) 
        if par_len < 0: # If the run length is negative, the y-projections do not overlap
            par_len = 0
        return par_len if unit_mode else par_len * res
         
    def get_min_space_parallel(self, layer_id, width, length, same_color=False, unit_mode=False):
        """
        Helper function to find minimum spacing between two parallel wires

        Parameters
        ----------
        layer_id : int
            routing grid layer id
        width : Union[int, float]
            maximum width of two parallel wires 
        length : Union[int, float]
            parallel run length of two parallel wires 
        same_color : boolean 
            True to use same-color spacing 
        unit_mode : boolean
            If true, width and length were given in resolution
            units

        Returns:
        --------
        sp : Union[int, float]
            minimum required spacing between two parallel wires 
        """

        tech_info = self.tech_info
        parfunc = getattr(tech_info, 'get_parallel_space', None)
        if not callable(parfunc):
            raise Exception('Cannot find parallel spacing! Function get_parallel_space not in tech.def!')
        lay_name = tech_info.get_layer_name(layer_id)
        if isinstance(lay_name, tuple):
            lay_name = lay_name[0]
        lay_type = tech_info.get_layer_type(lay_name)
        return tech_info.get_parallel_space(lay_type, width, length, same_color, unit_mode)

    def fill_flipwell_dummy(self,**kwargs):
        # type: (...) -> Tuple[List[WireArray], List[WireArray]]
        """Draw dummies on flipwell unused transistors. Does not do fill, so
        transistors between devices have to be taken into account manually (or implemented here).

        Call PMOS and NMOS separately using the row_type parameter

        This method should be called before using fill dummy.

        Parameters
        ----------
        rows : List[int] 
            The integers for rows (e.g [0,1])
        row_type : str
            Row types ('nch' or 'pch' depending on PMOS or NMOS)
        left_stop : int
            Final finger from left side which is dummy. 
            The dummies are filled from finger 0 to left_stop 
        right_start : int
            First finger which is dummy filled on the right side
        right_stop : int
            Final finger dummy filled (Probably fg_tot-right_start)
        source_dirs : Optional[int]
            Direction of the source node (0s or 2s,default [2]*len(rows))
        drain_dirs : Optional[int]
            Direction of the drain node (0s or 2s, default [0]*len(rows))
        sup_width : int
            Width of the supply wire 
        sup_tids : int
            Supply wire track id
        left_source_tid : List[int]
            Left source node track id
        left_drain_tid : List[int]
            Left drain node track id
        right_source_tid : List[int]
            Right source node track id
        right_drain_tid : List[int]
            Right drain node track id
        right_drain_tid : List[int]
            Right drain node track id
        orientations : List[str]
            Orientations of the given row (list of 'MX' and 'R0' strings
        left_d_width : Optional[List[int]]
            Width of the drain track used in left side of the given row
        left_s_width : Optional[List[int]]
            Width of the source track used in left side of the given row
        right_d_width : Optional[List[int]]
            Width of the drain track used in right side of the given row
        right_s_width : Optional[List[int]]
            Width of the source track used in right side of the given row
        left_source_dirs : Optional[List[int]]
            Source directions for left side (overrides left_source_dir if given)
        right_source_dirs: Optional[List[int]]
            Source directions for right side (overrides left_source_dir if given)
        left_drain_dirs : Optional[List[int]]
            Drain directions for left side (overrides left_source_dir if given)
        right_drain_dirs: Optional[List[int]]
            Drain directions for right side (overrides left_source_dir if given)
        conn_layer : Optional[int]
            Horizontal connect layer. Defaults to ana_conn_layer+2

        Returns:
        --------
        supp_warr : List[WireArray]
             Wire array for the supply
        dummy_finger_info : List[int]
             How many additional fingers were added to each row
        """
        ## type: AnalogBase
        rows=kwargs.get('rows') # type: [Union[int, List[int]]]
        row_type=kwargs.get('row_type') # type: str, 'nch' or 'pch'
        left_stop=kwargs.get('left_stop') # type: Union[int, List[int]]
        right_start=kwargs.get('right_start') # type: Union[int, List[int]]
        right_stop=kwargs.get('right_stop') # type: Union[int, List[int]]
        source_dirs=kwargs.get('source_dirs',[2]*len(rows)) # type: Union[int, List[int]] (0 or 2)
        drain_dirs=kwargs.get('drain_dirs',[0]*len(rows)) # type: Union[int, List[int]] (0 or 2)
        sup_width=kwargs.get('sup_width')  # type: int
        sup_tid=kwargs.get('sup_tids')  # type: List[Union[int]]
        left_source_tid=kwargs.get('left_source_tid')  # type: List[Union[int]]
        left_drain_tid=kwargs.get('left_drain_tid')  # type: List[Union[int]]
        right_source_tid=kwargs.get('right_source_tid')  # type: List[Union[int]]
        right_drain_tid=kwargs.get('right_drain_tid')  # type: List[Union[int]]
        orientations=kwargs.get('orientations')  # type: List[Union[str]]
        left_d_widths=kwargs.get('left_d_widths',[1]*len(rows)) # type: List[Union[int]]
        right_d_widths=kwargs.get('right_d_widths',[1]*len(rows)) # type: List[Union[int]]
        left_s_widths=kwargs.get('left_s_widths',[1]*len(rows)) # type: List[Union[int]]
        right_s_widths=kwargs.get('right_s_widths',[1]*len(rows)) # type: List[Union[int]]
        left_drain_dirs=kwargs.get('left_drain_dirs',[None]*len(rows)) # type: List[Union[int]]
        right_drain_dirs=kwargs.get('right_drain_dirs',[None]*len(rows)) # type: List[Union[int]]
        left_source_dirs=kwargs.get('left_source_dirs',[None]*len(rows)) # type: List[Union[int]]
        right_source_dirs=kwargs.get('right_source_dirs',[None]*len(rows)) # type: List[Union[int]]
        conn_layer=kwargs.get('conn_layer',
                self.grid.tech_info.tech_params['layout']['mos_tech_class'].mos_config['ana_conn_layer']+2)

        net='VDD' if row_type=='pch' else 'VSS'
        dummy_finger_info=[]
        left_dummies=[]
        right_dummies=[]
        left_source_tids=[]
        left_source_warrs=[]
        right_source_tids=[]
        right_source_warrs=[]
        left_drain_tids=[]
        left_drain_warrs=[]
        right_drain_tids=[]
        right_drain_warrs=[]
        left_gate_tids=[]
        left_gate_warrs=[]
        right_gate_tids=[]
        right_gate_warrs=[]
        loclefts=[]
        locrights=[]
        for i in range(len(rows)):
            # Dummies to the left of actual devices
            type=row_type
            row=rows[i]
            start_col=0
            stop_col=left_stop[i]
            source_dir=source_dirs[i] if left_source_dirs[i]==None else left_source_dirs[i]
            drain_dir=drain_dirs[i]   if left_drain_dirs[i]==None else left_drain_dirs[i]
            s_net=net
            d_net=net
            left_dummies.append(self.draw_mos_conn(type, row, start_col, stop_col,
                source_dir, drain_dir, s_net=s_net, d_net=d_net))
            # Dummies to the right of actual devices
            type=row_type
            row=rows[i]
            start_col=right_start[i]
            stop_col=right_stop[i]
            source_dir=source_dirs[i] if right_source_dirs[i]==None else right_source_dirs[i]
            drain_dir=drain_dirs[i]   if right_drain_dirs[i]==None else right_drain_dirs[i]
            s_net=net
            d_net=net
            right_dummies.append(self.draw_mos_conn(type, row, start_col, stop_col,
                source_dir, drain_dir, s_net=s_net, d_net=d_net))

            # How many dummies have been added
            dummy_finger_info.append(left_stop[i]+right_stop[i])

            # Add the supply on the lowest / highest row
            # TODO: add a possibility to set which row the supply track is implementted to
            if (row_type=='pch' and i==len(rows)-1) or (row_type=='nch' and i==0):
                right_drain_dirs[i]=drain_dirs[i] if right_drain_dirs[i]==None else right_drain_dirs[i]
                left_drain_dirs[i]=drain_dirs[i] if left_drain_dirs[i]==None else left_drain_dirs[i]
                right_source_dirs[i]=source_dirs[i] if right_source_dirs[i]==None else right_source_dirs[i]
                left_source_dirs[i]=source_dirs[i] if left_source_dirs[i]==None else left_source_dirs[i]
                if left_drain_dirs[i]==0 and orientations[i]=='MX':
                    left_d_width=1
                    left_s_width=sup_width
                elif left_drain_dirs[i]==2 and orientations[i]=='MX':
                    left_d_width=sup_width
                    left_s_width=1
                elif left_drain_dirs[i]==0 and orientations[i]=='R0':
                    left_d_width=sup_width
                    left_s_width=1
                else:
                    left_d_width=1
                    left_s_width=sup_width
                left_s_tidx=left_source_tid[i]   if left_d_width==sup_width else sup_tid
                left_d_tidx=left_drain_tid[i]    if left_d_width==1 else sup_tid
                if right_drain_dirs[i]==0 and orientations[i]=='MX':
                    right_d_width=1
                    right_s_width=sup_width
                elif right_drain_dirs[i]==2 and orientations[i]=='MX':
                    right_d_width=sup_width
                    right_s_width=1
                elif right_drain_dirs[i]==0 and orientations[i]=='R0':
                    right_d_width=sup_width
                    right_s_width=1
                else:
                    right_d_width=1
                    right_s_width=sup_width
                right_s_tidx=right_source_tid[i] if right_d_width==sup_width else sup_tid
                right_d_tidx=right_drain_tid[i]  if right_d_width==1 else sup_tid

                # Sources 
                left_source_tids.append(self.make_track_id(row_type, row_idx=rows[i], tr_type='ds',  tr_idx=left_s_tidx, width=left_s_width))
                left_source_warrs.append(self.connect_to_tracks(left_dummies[i]['s'], left_source_tids[i],min_len_mode=-1))
                right_source_tids.append(self.make_track_id(row_type, row_idx=rows[i], tr_type='ds', tr_idx=right_s_tidx, width=right_s_width))
                right_source_warrs.append(self.connect_to_tracks(right_dummies[i]['s'], right_source_tids[i],min_len_mode=1))
                # Drains 
                left_drain_tids.append(self.make_track_id(row_type, row_idx=rows[i], tr_type='ds',  tr_idx=left_d_tidx, width=left_d_width))
                left_drain_warrs.append(self.connect_to_tracks(left_dummies[i]['d'], left_drain_tids[i],min_len_mode=-1))
                right_drain_tids.append(self.make_track_id(row_type, row_idx=rows[i], tr_type='ds', tr_idx=right_d_tidx, width=right_d_width))
                right_drain_warrs.append(self.connect_to_tracks(right_dummies[i]['d'], right_drain_tids[i],min_len_mode=1))
                # Short the gates to their respective drain/source (to the track of the supply)
                left_tid=left_drain_tids[i] if left_d_width==1 else left_source_tids[i]
                right_tid=right_drain_tids[i] if right_d_width==1 else right_source_tids[i]
                left_gate_warrs.append(self.connect_to_tracks(left_dummies[i]['g'], left_tid,min_len_mode=-1))
                right_gate_warrs.append(self.connect_to_tracks(right_dummies[i]['g'], right_tid,min_len_mode=1))
                # Which wires to connect to the VDD substrate
                connleft=left_drain_warrs if left_d_width==sup_width else left_source_warrs
                connright=right_drain_warrs if right_d_width==sup_width else right_source_warrs
                connarr=connleft+connright
                sup_warr=self.connect_wires(connarr)
            else: # Just route the dummies together
                # Sources 
                left_source_tids.append(self.make_track_id(row_type, row_idx=rows[i], tr_type='ds',  tr_idx=left_source_tid[i], width=left_s_widths[i]))
                left_source_warrs.append(self.connect_to_tracks(left_dummies[i]['s'], left_source_tids[i],min_len_mode=-1))
                right_source_tids.append(self.make_track_id(row_type, row_idx=rows[i], tr_type='ds', tr_idx=right_source_tid[i], width=right_s_widths[i]))
                right_source_warrs.append(self.connect_to_tracks(right_dummies[i]['s'], right_source_tids[i],min_len_mode=1))
                # Drains 
                left_drain_tids.append(self.make_track_id(row_type, row_idx=rows[i], tr_type='ds',  tr_idx=left_drain_tid[i], width=left_d_widths[i]))
                left_drain_warrs.append(self.connect_to_tracks(left_dummies[i]['d'], left_drain_tids[i],min_len_mode=-1))
                right_drain_tids.append(self.make_track_id(row_type, row_idx=rows[i], tr_type='ds', tr_idx=right_drain_tid[i], width=right_d_widths[i]))
                right_drain_warrs.append(self.connect_to_tracks(right_dummies[i]['d'], right_drain_tids[i],min_len_mode=1))
                # Short the gates to their respective drain/source
                if drain_dirs[i]==0 and orientations[i]=='MX':
                    left_tid=left_drain_tids[i]   
                    right_tid=right_drain_tids[i] 
                elif drain_dirs[i]==2 and orientations[i]=='MX':
                    left_tid=left_source_tids[i]   
                    right_tid=right_source_tids[i] 
                elif drain_dirs[i]==0 and orientations[i]=='R0':
                    left_tid=left_source_tids[i]   
                    right_tid=right_source_tids[i] 
                else:
                    left_tid=left_drain_tids[i]   
                    right_tid=right_drain_tids[i] 
                left_gate_warrs.append(self.connect_to_tracks(left_dummies[i]['g'], left_tid,min_len_mode=-1))
                right_gate_warrs.append(self.connect_to_tracks(right_dummies[i]['g'], right_tid,min_len_mode=1))
            # Compute the leftmost point (either source or drain)
            loclefts.append((min(left_drain_warrs[i].get_bbox_array(self.grid).left,
                    left_source_warrs[i].get_bbox_array(self.grid).left)))
            locrights.append((max(right_drain_warrs[i].get_bbox_array(self.grid).right,
                    right_source_warrs[i].get_bbox_array(self.grid).right)))

        # Left side
        idx=self.grid.coord_to_nearest_track(layer_id=conn_layer, coord=min(loclefts))
        ver_id = TrackID(layer_id=conn_layer, track_idx=idx, width=1)
        left_warr = self.connect_to_tracks(left_source_warrs+left_drain_warrs, ver_id)
        # Right side
        idx=self.grid.coord_to_nearest_track(layer_id=conn_layer, coord=max(locrights))
        ver_id = TrackID(layer_id=conn_layer, track_idx=idx, width=1)
        right_warr = self.connect_to_tracks(right_source_warrs+right_drain_warrs, ver_id)

        return sup_warr,dummy_finger_info

    def interdigitate_transistors(self,**kwargs):
        # type: (...) -> Tuple[List[WireArray], List[WireArray]]
        """Interdigitate transistors

        Parameters
        ----------
        type : str
            the row type, one of 'nch', 'pch', 'ntap', or 'ptap'.
        row : int
            the center row index.  0 is the bottom-most row.
        start_col : int
            leftmost transistor index (0 is the left-most transistor)
        total_nf : int
            how many fingers to be interdigitated in total (e.g. 2*diff_pair_nf)
        source_dir : int
            source connection direction. 0 for down, 1 for middle, 2 for up
        drain_dir : int
            drain connection direction. 0 for down, 1 for middle, 2 for up
        s_nets : list[str]
            list of source net names (e.g. ['BIAS','BIAS'] for common source)
        d_nets : list[str]
            list of drain net names (e.g. ['INP','INM'] )
        finger_split_coeff : int
            How many fingers are the interdigitation split in

        Returns:
        --------
        xright : List[self.draw_mos_conn returns]
             List of transistor names returned by draw_mos_conn
        xleft : List[self.draw_mos_conn returns]
             List of transistor names returned by draw_mos_conn
        """
        ## type: AnalogBase
        type=kwargs.get('type') #transistor row type (str, 'nch' or 'pch')
        row=kwargs.get('row') #transistor row (int)
        start_col=kwargs.get('start_col') #starting column (int)
        total_nf=kwargs.get('total_nf') #how many fingers in total to be interdigited (e.g. 2*dp_nf)
        source_dir=kwargs.get('source_dir') # source direction (0 or 2)
        drain_dir=kwargs.get('drain_dir') # drain direction (0 or 2)
        s_nets=kwargs.get('s_nets') # List of the source net connections (e.g. ['BIAS','BIAS'])
        d_nets=kwargs.get('d_nets') # List of the drain net connections (e.g. ['INP','INM'])
        finger_split_coeff=kwargs.get('finger_split_coeff',2) # How many fingers are interdigited (optional, default 2)

        xright=[]
        xleft=[]
        # Right transistor first
        start_col=start_col
        stop_col=int(finger_split_coeff) #Offset from start col
        s_net=s_nets[0]
        d_net=d_nets[0]
        xright.append(self.draw_mos_conn(type, row, start_col, stop_col,
            source_dir, drain_dir, s_net=s_net, d_net=d_net ))
        for i in range(int(total_nf/finger_split_coeff)-1):
            start_col=int(start_col+stop_col)
            stop_col=int(finger_split_coeff) #Offset from start col
            if i%2==0:
                s_net=s_nets[1]
                d_net=d_nets[1]
                xleft.append(self.draw_mos_conn(type, row, start_col,
                    stop_col, source_dir, drain_dir, s_net=s_net, d_net=d_net ))
            else:
                s_net=s_nets[0]
                d_net=d_nets[0]
                xright.append(self.draw_mos_conn(type, row, start_col,
                    stop_col, source_dir, drain_dir, s_net=s_net, d_net=d_net ))
        
        return xright,xleft

    def route_y_tree_helper(self, template, warrs, route_dir=1, max_iters=99):
        layers = list(set([warr.track_id.layer_id for warr in warrs]))
        if len(layers) > 1:
            raise ValueError("Y-tree warrs must be on same layer!")
        layer = layers[0]
        width = list(set([warr.track_id.width for warr in warrs]))
        if len(width) > 1:
            raise ValueError("Y-tree warrs must have the same width!")
        width = width[0]
        if route_dir not in [-1,1]:
            raise ValueError("Route direction must either 1 or -1")

        dir = 'y' if self.grid.get_direction(layers[0]) == 'x' else 'x'

        conn_mid = [warr.middle_unit for warr in warrs]
        conn_pairs = [(mid1, mid2) for mid1, mid2 in zip(conn_mid[::2], conn_mid[1::2])]

        sp = template.grid.get_num_space_tracks(layer, width)
        sp_tr = sp+width
        next_tid = max([warr.track_id.base_index for warr in warrs])# + sp_tr * route_dir

        # Check that the next level of the three is routable and proceed when it is
        iters = 0
        track_availability = [False for i in range(len(conn_pairs))]
        while len(set(track_availability)) > 1 or False in set(track_availability):
        #for i, pair in enumerate(conn_pairs):
            next_tid += sp_tr * route_dir
            track_availability = [template.is_track_available(layer, next_tid, left, right,
                width, unit_mode=True) for left,right in conn_pairs]
            iters += 1
            if iters >= max_iters:
                raise ValueError("Cannot route y-tree, max_iters exceeded when find tracks")

        lower = min([warr.track_id.get_bounds(template.grid,unit_mode=True)[0] for warr in warrs])
        upper = template.grid.track_to_coord(layer, next_tid, unit_mode=True) + \
                template.grid.get_track_width(layer, width, unit_mode=True) // 2
        track_availability = [False for i in range(2*len(conn_pairs))]
        tid_offs = -sp_tr
        iters = 0
        while len(set(track_availability)) > 1 or False in set(track_availability):
            tid_offs += sp_tr
            left_tids = [template.grid.find_next_track(layer+1, pair[0], width, half_track=True,
                mode=1, unit_mode=True) + tid_offs for pair in conn_pairs]
            track_availability[::2] = [template.is_track_available(layer+1, tid, lower, upper,
                width, unit_mode=True) for tid in left_tids]
            right_tids = [template.grid.find_next_track(layer+1, pair[1], width, half_track=True,
                mode=-1, unit_mode=True) - tid_offs for pair in conn_pairs]
            track_availability[1::2] = [template.is_track_available(layer+1, tid, lower, upper,
                width, unit_mode=True) for tid in right_tids]
            iters+=1
            if iters >= max_iters:
                raise ValueError("Cannot route y-tree, max_iters exceeded when find tracks")

        warrs_new = [template.add_wires(layer, next_tid, left, right, width, unit_mode=True)
                for left, right in conn_pairs]
        tid_pairs = [(TrackID(layer+1, left, width), TrackID(layer+1, right, width)) for left,right in zip(left_tids, right_tids)]
        # Finally, make the connections
        for new, old, tid in zip(warrs_new, warrs[::2], tid_pairs):
            template.connect_to_tracks([new, old], tid[0])
        for new, old, tid in zip(warrs_new, warrs[1::2], tid_pairs):
            template.connect_to_tracks([new, old], tid[1])
        if len(warrs_new) == 1:
            return warrs_new[0]
        else:
            return self.route_y_tree_helper(template, warrs_new)


    def route_y_tree(self, template, warrs):
        """
        Route wire arrays as y-tree.

        Parameters:
        Template: TemplateDB, the template in which layout resides in
        warrs: WireArrays to be routed
        """
        from math import ceil,floor
        num_inputs = len(warrs)
        ispow2 = not bool(num_inputs & num_inputs - 1)
        # First, crete dummy warrs if the number of inputs is not exactly power of two
        if not ispow2:
            num_inputs = 2**ceil(np.log2(num_inputs))
            ndummy = num_inputs - len(warrs)
            spx = warrs[1].lower - warrs[0].lower
            tid = warrs[0].track_id
            for i in range(- round(ndummy / 2), 0):
                base_idx = tid.base_index
                width = tid.width
                layer = tid.layer_id
                warr = warrs[0]
                lower = warr.lower + i*spx
                upper = warr.upper + i*spx
                if lower < self.bound_box.left:
                    diff_lower = self.bound_box.left - lower
                    lower += diff_lower
                    upper += diff_lower
                if upper > self.bound_box.right:
                    diff_upper = upper - self.bound_box.right
                    lower -= diff_upper
                    upper -= diff_upper
                base_idx=self.get_closest_available_track(template, layer, base_idx, lower, upper, search_dir=0, width=width, unit_mode=False)
                warrs.insert(0,self.add_wires(layer, base_idx, lower, upper, width))
            end = floor(ndummy/2)
            if ndummy % 2:
                end+=1
            for i in range(end):
                base_idx = tid.base_index
                width = tid.width
                layer = tid.layer_id
                warr = warrs[-1]
                lower = warr.lower + (i+1)*spx
                upper = warr.upper + (i+1)*spx
                if lower < self.bound_box.left:
                    diff_lower = self.bound_box.left - lower
                    lower += diff_lower
                    upper += diff_lower
                if upper > self.bound_box.right:
                    diff_upper = upper - self.bound_box.right
                    lower -= diff_upper
                    upper -= diff_upper
                base_idx=self.get_closest_available_track(template, layer, base_idx, lower, upper, search_dir=0, width=width, unit_mode=False)
                warrs.append(self.add_wires(layer, base_idx, lower, upper, width))
        else:
            width = warrs[0].track_id.width
        try:
            warr=self.route_y_tree_helper(template, warrs)
        except ValueError:
            raise ValueError("WARN: Couldn't route CLK as y-tree. Congested routing?")
        return warr
