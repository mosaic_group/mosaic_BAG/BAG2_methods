#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Metric SI helper module
=======================

This module defines helpers to use SI metric values.

PROBLEM:
    `BAG2_technology_definition` properties often contain values
    which have to be provided in units (of the manufacturing grid).
    Design Rule Manuals, often the source of truth for these values,
    in constrast typically are metric values (µm).
    This mismatch (by directly coding unit-based values) harms readability.

SOLUTION:
    Offer helper classes to allow easy conversion from metric SI prefixed values to unit values.

Example `BAG2_technology_definition` properties in `layer_parameters.py`:
>>> # ...
>>> from BAG2_methods.tech.si import µm, SIConverter
>>>
>>> conv = SIConverter(layout_unit_in_meters=1e-6, resolution_in_layout_units=0.005)
>>>
>>> class layer_parameters(layer_parameters_template):
>>> # ...
>>>     @property
>>>     def sp_le_min(self) -> Dict[str, Dict[str, List[Union[int, float]]]]:
>>>         si_dict = {
>>>             '1': {
>>>                 'w_list':  [0.3 * µm,  0.7 * µm,  float('inf')]
>>>                 'sp_list': [0.03 * µm, 0.09 * µm, 1.2 * µm]
>>>             },
>>>             # ...
>>>         }
>>>         unit_dict = conv.convert_si_to_unit(si_dict)
>>>         return unit_dict

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

Created on 27.04.2023 by Martin Köhler <martin.koehler@silicon-austria.com>
"""

import forallpeople as si
import math
from typing import *
import unittest


# Typical metric units for IC design:
µm = si.m * 1e-6
nm = si.m * 1e-9


class SIConverter:
    def __init__(self,
                 layout_unit_in_meters: float,
                 resolution_in_layout_units: float):
        self.layout_unit_in_meters = layout_unit_in_meters
        self.resolution_in_layout_units = resolution_in_layout_units

    def convert_si_to_unit(self, obj: Union[si.Physical, List, Tuple, Dict]) -> Union[int, List, Tuple, Dict]:
        """
        Convert all SI Physical values in object graph from SI to units.
        As SI Physical values "know" their dimensions (i.e. 1 for length VS 2 for area),
        this can be taken into account automatically.

        Parameters
        ----------
        obj :
            object to convert (plain Physical or container)

        Returns
        -------
        An object of the same structure, but with all SI Physical values converted to int.
        """
        match obj:
            case si.Physical():
                si_val: si.Physical = obj
                self.__validate_physical_are_metric(si_val)
                denom_per_dim = (self.layout_unit_in_meters * self.resolution_in_layout_units)
                # NOTE: width/length values are 1-dimensional,
                #       but for example area constraints can be 2-dimensional
                unit_val: si.Physical = si_val / (denom_per_dim**si_val.dimensions.m)
                return math.ceil(unit_val.value)
            case list():
                return [self.convert_si_to_unit(v) for v in obj]
            case dict():
                return {self.convert_si_to_unit(k): self.convert_si_to_unit(v) for k, v in obj.items()}
            case tuple():
                return tuple(map(lambda v: self.convert_si_to_unit(v), obj))
            case _:
                return obj

    def convert_unit_to_si(self,
                           obj: Union[int, List, Tuple, Dict],
                           dimensions: int = 1) -> Union[si.Physical, List, Tuple, Dict]:
        """
        Convert all integers in object graph from units to SI.
        This is only used to semi-automatically convert legacy unit-based
        technology definitions to SI based ones,
        as the plain int values don't "know" their dimensions (i.e. length VS area).

        Parameters
        ----------
        obj : Union[int, List, Tuple, Dict]
            object to convert (plain int or container)

        dimensions : int
            The number of dimensions (i.e. 1 for length/width, 2 for areas)
            that the int values found in the given are interpreted in.

        Returns
        -------
        An object of the same structure, but with all ints converted to SI.
        """
        def convert(v):
            return self.convert_unit_to_si(v, dimensions)

        match obj:
            case int():
                factor = self.layout_unit_in_meters * self.resolution_in_layout_units * si.m
                si_val: si.Physical = obj * factor**dimensions
                return si_val
            case list():
                return [convert(v) for v in obj]
            case dict():
                return {
                    convert(k): convert(v) for k, v in obj.items()
                }
            case tuple():
                return tuple(map(convert, obj))
            case _:
                return obj

    @staticmethod
    def __validate_physical_are_metric(val: si.Physical):
        d = val.dimensions
        match (d.kg, d.m, d.s, d.A, d.cd, d.K, d.mol):
            case (0,   _,   0,   0,    0,   0,     0):
                return  # only metric is fine
            case _:
                raise TypeError(f"only metric SI values are supported, but obtained {d}")


# -------------------------- unit tests --------------------------------

class SIConverterTest(unittest.TestCase):
    def setUp(self):
        self.converter = SIConverter(layout_unit_in_meters=1e-6,
                                     resolution_in_layout_units=0.005)

    def test_convert_si_to_unit_Physical(self):
        self.assertEqual(20, self.converter.convert_si_to_unit(100 * nm))

    def test_convert_si_to_unit_list(self):
        self.assertEqual([20], self.converter.convert_si_to_unit([100 * nm]))

    def test_convert_si_to_unit_dict(self):
        self.assertEqual({'k': 20}, self.converter.convert_si_to_unit({'k': 100 * nm}))

    def test_convert_si_to_unit_tuple(self):
        self.assertEqual((20, 40),
                         self.converter.convert_si_to_unit(
                             (100 * nm, 200 * nm)
                         ))

    def test_convert_unit_to_si_int(self):
        self.assertEqual(100 * nm, self.converter.convert_unit_to_si(20))

    def test_convert_unit_to_si_list(self):
        self.assertEqual([100 * nm], self.converter.convert_unit_to_si([20]))

    def test_convert_unit_to_si_dict(self):
        self.assertEqual({'k': 100 * nm}, self.converter.convert_unit_to_si({'k': 20}))

    def test_convert_unit_to_si_tuple(self):
        self.assertEqual((100 * nm, 200 * nm),
                         self.converter.convert_unit_to_si(
                             (20, 40)
                         ))


if __name__ == '__main__':
    unittest.main()
