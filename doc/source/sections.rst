=============================
BAG2 methods interface module
=============================

.. automodule:: BAG2_methods
   :members:
   :undoc-members:

.. automodule:: BAG2_methods.bag_design
   :members:
   :undoc-members:

