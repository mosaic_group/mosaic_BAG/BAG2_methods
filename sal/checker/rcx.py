#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from enum import Enum
from typing import *


@dataclass
class RCXExtractionType(str, Enum):
    RCC = 'RCC'
    RC = 'RC'
    R = 'R'
    CC = 'CC'
    NoRC = 'NoRC'

    DEFAULT = RCC

    def to_bag_dict(self) -> Dict:
        """
        Returns
        -------
        Dictionary to overlay bag_config.yaml key database/checker/rcx_params
        See bag.verification.virtuoso.py for details
        """

        return {
            #  'extract': { 'type': 'rc_coupled'  }
            'netlist_type': str(self)
        }


@dataclass
class RCXNetlistType(str, Enum):
    CALIBREVIEW = 'CALIBREVIEW'
    NETLIST = 'SPECTRE'
