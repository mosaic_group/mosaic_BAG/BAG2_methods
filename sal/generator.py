#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from typing import Dict, List, Optional

import sal.gentool.tool

if __name__ == '__main__':
    sal.gentool.tool.main()
