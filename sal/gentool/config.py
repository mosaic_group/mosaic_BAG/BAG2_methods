from __future__ import annotations  # allow class type hints within same class

import sys
from dataclasses import dataclass
from enum import Enum
from typing import Dict, List, Optional
import yaml

import sal.log
from sal.env import Env
from sal.log import *
from sal.pkg.git_source import *


@dataclass
class Config:
    git_sources: GitSourceList
    default_git_source: str

    @classmethod
    def default_config(cls) -> Config:
        return Config(
            git_sources=GitSourceList([
                GitSource(name="mosaic",
                          protocol=Protocol.HTTPS,
                          git_server_url="https://gitlab.com",
                          subgroup_path="mosaic_group/mosaic_BAG/generators",
                          subgroup_id=52541524,
                          repo_blacklist=['inverter_gen']),
                GitSource(name="mosaic_opamp",
                          protocol=Protocol.HTTPS,
                          git_server_url="https://gitlab.com",
                          subgroup_path="mosaic_group/mosaic_BAG/generators/two_stage_opamp",
                          subgroup_id=61729127,
                          repo_blacklist=[]),
                GitSource(name="mosaic_tb",
                          protocol=Protocol.HTTPS,
                          git_server_url="https://gitlab.com",
                          subgroup_path="mosaic_group/mosaic_BAG/testbenches",
                          subgroup_id=59327749,
                          repo_blacklist=[]),
            ]),
            default_git_source="mosaic"
        )

    @classmethod
    def load(cls, path: str) -> Config:
        with open(path, 'r') as f:
            obj = yaml.load(f, Loader=yaml.Loader)
            return obj

    def dump(self,
             path: Optional[str] = None) -> str:
        if path is None:
            return yaml.dump(self, sys.stdout)
        else:
            with open(path, 'w') as f:
                return yaml.dump(self, f)

    @staticmethod
    def custom_config_file_path() -> Optional[str]:
        if Env.gen_tool_config_path:
            return Env.gen_tool_config_path

        sal.log.error(
            f"ERROR: environmental variable {Env.gen_tool_config_path_variable} is not set\n"
            f"\tThis is required to store the modified custom configuration.\n"
            f"\tTherefore we only dump it to stdout.\n",
            file=sys.stderr
        )
        return None

    def add_source(self,
                   name: str,
                   url: str,
                   access_token: Optional[str] = None):
        self.git_sources.add_source(
            name=name,
            url=url,
            access_token=access_token
        )
        self.dump(path=self.custom_config_file_path())

    def remove_source(self,
                      name: str):
        self.git_sources.remove_source(name=name)
        self.dump(path=self.custom_config_file_path())
