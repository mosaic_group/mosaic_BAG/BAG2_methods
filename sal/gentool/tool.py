#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import annotations  # allow class type hints within same class
import argparse
from dataclasses import dataclass
from enum import Enum
import glob
import os
import re
import shutil
import sys
import subprocess
from typing import Dict, List, Optional
from urllib.parse import urlparse

from sal.log import *
from sal.log_analyzer import *
from sal.env import Env
from sal.gentool.config import Config
from sal.pkg.generator_package import GeneratorPackage
from sal.pkg.testbench_package import TestbenchPackage
from sal.simulation.simulation_mode import SimulationMode
from sal.simulation.simulation_result import SimulationResult
from sal.simulation.sim_access_additions import SimAccessAdditions
from sal.checker.rcx import *
from BAG2_methods.flow_variant import BAGFlowVariant


class ReportOpenPolicy(str, Enum):
    ALWAYS = 'always'
    NEVER = 'never'
    ON_ERROR = 'on_error'
    DEFAULT = ON_ERROR

    def shall_show(self, successful: bool) -> bool:
        switcher = {
            ReportOpenPolicy.ALWAYS: True,
            ReportOpenPolicy.ON_ERROR: not successful,
            ReportOpenPolicy.NEVER: False
        }
        return switcher[self]


class ExistingFileStrategy(str, Enum):
    SKIP = 'skip'
    OVERWRITE = 'overwrite'
    WARN_AND_ABORT = 'warn'


class NetlistExportSource(str, Enum):
    TEMPLATE = 'template'
    GENERATED = 'generated'
    # EXTRACTED = 'extracted'  # TODO: klayout extracted netlist
    DEFAULT = GENERATED


class NetlistFormat(str, Enum):
    SPICE = 'spice'
    # SPECTRE = 'spectre'  # TODO: other formats
    DEFAULT = SPICE


class LayoutFormat(str, Enum):
    GDS = 'gds'
    # TODO: other formats
    DEFAULT = GDS


class Args:
    def __init__(self, arg_list: List[str] = None):
        main_parser = argparse.ArgumentParser(description="BAG2 Common Generator Tool: "
                                                          "Generic driver for workflow steps "
                                                          "(integrating generators and testbenches)",
                                              epilog=f"See 'gen <subcommand> -h' for help on subcommand")
        subparsers = main_parser.add_subparsers(dest="command", help="Sub-commands help")

        parser_init = subparsers.add_parser("init", help="Initialize run directories (library files, server port, etc)")
        parser_init.add_argument("--force", "-f", action="store_true", default=False, help="Overwrite existing files")

        parser_config = subparsers.add_parser("config", help="Configuration")
        subparsers_config = parser_config.add_subparsers(dest="config_command", metavar="command",
                                                         help="Config sub-commands help",
                                                         required=True)

        parser_config_dump = subparsers_config.add_parser("dump", help="Dump config file")
        group = parser_config_dump.add_mutually_exclusive_group()
        group.add_argument("--current", action="store_true", default=False,
                           help="Dump current configuration")
        group.add_argument("--default", action="store_true", default=False,
                           help="Dump default configuration")

        parser_sources_list = subparsers_config.add_parser("sources-list", help="List GIT sources")
        parser_sources_list.add_argument("--name-only", "-1", dest="list_name_only",
                                         action="store_true", default=False,
                                         help="Only list names of GIT sources")

        parser_sources_add = subparsers_config.add_parser("sources-add", help="Add GIT source")
        parser_sources_add.add_argument("--url", "-u", dest="url", required=True,
                                        help="URL to GIT subproject containing packages")
        parser_sources_add.add_argument("--name", "-n", dest="name", required=True,
                                        help="Name of the GIT source")
        parser_sources_add.add_argument("--token", "-t", dest="access_token", default=None,
                                        help="Access token for Gitlab")

        parser_sources_remove = subparsers_config.add_parser("sources-remove", help="Remove GIT source")
        parser_sources_remove.add_argument("--name", "-n", dest="name", required=True,
                                           help="Name of the GIT source")

        parser_avail = subparsers.add_parser("avail", help="List available packages (generators, testbenches)")
        parser_avail.add_argument("--verbose", "-v", dest="verbose", default=False, help="Verbose mode")

        parser_list = subparsers.add_parser("list", help="List installed packages (generators, testbenches)")
        parser_list.add_argument("--verbose", "-v", dest="verbose", default=False, help="Verbose mode")

        parser_add = subparsers.add_parser("add",
                                           help="Add package (generator, testbench)",
                                           epilog=f"See 'gen avail' for available packages")
        parser_add.add_argument("--verbose", "-v", action="store_true", default=False, help="Verbose mode")

        group = parser_add.add_mutually_exclusive_group(required=True)
        group.add_argument("--all-of", "-a", dest="add_all_of_git_source", metavar="GIT_SOURCE",
                           default=None, required=False,
                           help="Add all available packages of a GIT source, e.g. mosaic_opamp")
        group.add_argument('packages', metavar='PACKAGE', type=str, nargs='*', default=[],
                           help='Package names (f.ex. mosaic/tgate_gen)')

        parser_new = subparsers.add_parser("new", help="Create a new package directory (generator, testbench)")
        parser_new.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                help="Verbose mode")
        parser_new.add_argument("--ref", dest="reference_package", default="mosaic/tgate_gen",
                                help="Reference package to use as a starting point (default is mosaic/tgate_gen)")
        parser_new.add_argument("--overwrite", dest="overwrite", action="store_true", default=False,
                                help="Overwrite if already existent")
        parser_new.add_argument('packages', metavar='PACKAGE', type=str, nargs=1,
                                help='New package name (f.ex. mosaic/my_fancy_gen)')
        #     parser_new.add_argument("--dry", "-d", dest="dry_run", action='store_true', default=False,
        #                             help="Dry run (no real changes to the file system)")

        show_gui_help = self.render_enum_help(topic='Show report GUI', enum_cls=ReportOpenPolicy)

        parser_make = subparsers.add_parser("make", help="Build generator")
        parser_make.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                 help="Verbose mode")
        parser_make.add_argument("--path", "-p", dest="params_path", default=None, help="Input params file path")
        parser_make.add_argument("--gui", "-g", dest="show_report_gui", type=str,
                                 default=ReportOpenPolicy.DEFAULT.value,
                                 help=show_gui_help)
        parser_make.add_argument('generators', metavar='GENERATOR', type=str, nargs='+',
                                 help='generator names (f.ex. tgate_gen)')

        enum_help_netlist_source = self.render_enum_help(topic='Netlist Source', enum_cls=NetlistExportSource)
        enum_help_netlist_format = self.render_enum_help(topic='Netlist Format', enum_cls=NetlistFormat)
        enum_help_layout_format = self.render_enum_help(topic='Layout Format', enum_cls=LayoutFormat)

        parser_export_netlist = subparsers.add_parser("export-netlist", help="Export netlist file")
        parser_export_netlist.add_argument("--output-path", "-o", dest="output_path", required=True,
                                           help="Output file path")
        parser_export_netlist.add_argument("--source", "-s", dest="source", type=str,
                                           default=NetlistExportSource.DEFAULT.value,
                                           help=enum_help_netlist_source)
        parser_export_netlist.add_argument("--format", "-f", dest="format", type=str,
                                           default=NetlistFormat.DEFAULT.value,
                                           help=enum_help_netlist_format)
        parser_export_netlist.add_argument('generators', metavar='GENERATOR', type=str, nargs=1,
                                           help='generator names (f.ex. tgate_gen)')

        parser_export_layout = subparsers.add_parser("export-layout", help="Export layout file")
        parser_export_layout.add_argument("--output-path", "-o", dest="output_path", required=True,
                                          help="Output file path")
        parser_export_layout.add_argument("--format", "-f", dest="format", type=str,
                                          default=LayoutFormat.DEFAULT.value,
                                          help=enum_help_layout_format)
        parser_export_layout.add_argument('generators', metavar='GENERATOR', type=str, nargs=1,
                                          help='generator names (f.ex. tgate_gen)')

        parser_dump = subparsers.add_parser("dump", help="Dump default parameters as YAML")
        parser_dump.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                 help="Verbose mode")
        parser_dump.add_argument("--path", "-p", dest="params_path", default=None, help="Output params dump file path")
        parser_dump.add_argument('generators', metavar='GENERATOR', type=str, nargs=1,
                                 help='generator name (f.ex. tgate_gen)')

        parser_parse = subparsers.add_parser("parse", help="Parse parameters from YAML")
        parser_parse.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                  help="Verbose mode")
        parser_parse.add_argument("--path", "-p", dest="params_path", required=True, help="Input params file path")
        parser_parse.add_argument('generators', metavar='GENERATOR', type=str, nargs=1,
                                  help='generator name (f.ex. tgate_gen)')

        parser_lvs = subparsers.add_parser("lvs", help="Run LVS on existing layout")
        parser_lvs.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                help="Verbose mode")
        parser_lvs.add_argument("--gui", "-g", dest="show_report_gui", type=str,
                                default=ReportOpenPolicy.DEFAULT.value,
                                help=show_gui_help)
        parser_lvs.add_argument('generators', metavar='GENERATOR', type=str, nargs='+',
                                help='generator names (f.ex. tgate_gen)')

        parser_drc = subparsers.add_parser("drc", help="Run DRC on existing layout")
        parser_drc.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                help="Verbose mode")
        parser_drc.add_argument("--gui", "-g", dest="show_report_gui", type=str,
                                default=ReportOpenPolicy.DEFAULT.value,
                                help=show_gui_help)
        parser_drc.add_argument('generators', metavar='GENERATOR', type=str, nargs='+',
                                help='generator names (f.ex. tgate_gen)')

        parser_rcx = subparsers.add_parser("rcx", help="Run RCX on existing layout")
        # TODO: netlist formats
        # parser_rcx.add_argument("--format", "-f", dest="rcx_extraction_format", type=str,
        #                        help="Extraction netlist format")
        rcx_help = f"Extraction type ∈ {set([name for name, member in RCXExtractionType.__members__.items()])}." \
                   f"\nDefaults to {RCXExtractionType.DEFAULT.value}"
        parser_rcx.add_argument("--type", "-t", dest="rcx_extraction_type", type=str,
                                default=RCXExtractionType.DEFAULT.value,
                                help=rcx_help)
        parser_rcx.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                help="Verbose mode")
        parser_rcx.add_argument('generators', metavar='GENERATOR', type=str, nargs='+',
                                help='generator names (f.ex. tgate_gen)')

        parser_sim = subparsers.add_parser("sim", help="Run simulations")
        parser_sim.add_argument("--list", "-l", dest="list", action="store_true", default=False,
                                help="List available testbenches and simulation modes")
        parser_sim.add_argument("--mode", "-s", dest="sim_mode", type=str, default="sch",
                                help="Simulation Mode (sch/rcx), defaults to sch")
        parser_sim.add_argument("--measurement", "-m", dest="measurement", type=str, default=None,
                                help="Measurement, defaults to all")
        parser_sim.add_argument("--path", "-p", dest="params_path", default=None, help="Input params file path")
        parser_sim.add_argument("--only-postprocess", "-o", dest="only_postprocess", action="store_true", default=False,
                                help="Only postprocess an existing hdf5 result file")
        parser_sim.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                help="Verbose mode")
        parser_sim.add_argument('generators', metavar='GENERATOR', type=str, nargs='?',
                                help='generator name (f.ex. tgate_gen)')

        parser_hdf5 = subparsers.add_parser("hdf5", help="HDF5 file I/O")
        parser_hdf5.add_argument("--input", "-i", dest="input_path", required=True, help="Input HDF5 file")
        parser_hdf5.add_argument("--output", "-o", dest="output_path", default=None,
                                 help="Output text file (if omitted, output is written to stdout)")

        # parser_gds = subparsers.add_parser("gds", help="Export GDS for existing layout (Not yet implemented)")
        # parser_gds.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
        #                         help="Verbose mode")
        # parser_gds.add_argument("--path", "-p", dest="params_path", default=None, help="Output path")
        # parser_gds.add_argument('generators', metavar='GENERATOR', type=str, nargs='+',
        #                         help='generator names (f.ex. tgate_gen)')

        #     parser_xor = subparsers.add_parser("xor", help="Diff two GDS streams")
        #     parser_xor.add_argument("--generator", "-g", required=True, help="Path to the generator")
        #     parser_xor.add_argument('--old', dest="old_gds_path", required=True, help="Path to the old GDS")
        #     parser_xor.add_argument('--new', dest="new_gds_path", required=True, help="Path to the new GDS")

        if arg_list is None:
            arg_list = sys.argv[1:]

        args = main_parser.parse_args(arg_list)
        if args.command is None:
            main_parser.print_usage()
            sys.exit(1)
        self.command = args.command

        if Env.gen_tool_config_path:
            self.config = Config.load(Env.gen_tool_config_path)
        else:
            self.config = Config.default_config()

        self.verbose = getattr(args, "verbose", None)

        if self.command in ('init',):
            self.force = args.force

        if self.command in ('add', 'new'):
            # remove trailing slashes in generator names
            self.packages = list(map(lambda g: g[:-1] if g.endswith('/') else g,
                                     args.packages))

            if self.command == 'add':
                self.add_all_of_git_source = args.add_all_of_git_source

            if self.command == 'new':
                self.overwrite = args.overwrite
                self.reference_package = args.reference_package

        if self.command in ('make', 'export-netlist', 'export-layout', 'dump',
                            'parse', 'lvs', 'drc', 'rcx'):
            # remove trailing slashes in generator names
            self.generators = list(map(lambda g: g[:-1] if g.endswith('/') else g,
                                       args.generators))

        if self.command in ('make', 'lvs', 'drc'):
            self.show_report_gui = ReportOpenPolicy(args.show_report_gui)

        if self.command in ('export-netlist', 'export-layout'):
            self.output_path = args.output_path
            self.format = args.format

        if self.command in ('export-netlist',):
            self.source = args.source

        if self.command == 'rcx':
            self.rcx_extraction_type = args.rcx_extraction_type

        if self.command in ('dump', 'parse', 'make', 'sim'):
            self.params_path = args.params_path

        if self.command == 'config':
            self.config_command = args.config_command
            match self.config_command:
                case 'dump':
                    self.config_dump_default = args.default
                    self.config_dump_current = args.current
                    if not self.config_dump_default and not self.config_dump_current:
                        self.config_dump_current = True
                case 'sources-list':
                    self.config_sources_list_name_only = args.list_name_only
                case 'sources-add':
                    self.config_sources_add_name = args.name
                    self.config_sources_add_url = args.url
                    self.config_sources_add_access_token = args.access_token
                case 'sources-remove':
                    self.config_sources_remove_name = args.name
                case _:
                    raise ValueError(f"INTERNAL ERROR: match does not handle {self.config_command}")

        if self.command == 'sim':
            self.sim_mode = SimulationMode(args.sim_mode)
            self.measurement = args.measurement
            self.list = args.list
            self.only_postprocess = args.only_postprocess
            if not self.list:
                missing_args = []
                if args.generators is None:
                    missing_args.append("GENERATOR")
                if len(missing_args) > 0:
                    parser_sim.error(f"the following arguments are required: {', '.join(missing_args)}")

                if args.generators is not None:
                    if isinstance(args.generators, str):
                        self.generators = [args.generators]
                    else:
                        # remove trailing slashes in generator names
                        self.generators = list(map(lambda g: g[:-1] if g.endswith('/') else g,
                                                   args.generators))

        if self.command == 'hdf5':
            self.input_path = args.input_path
            self.output_path = args.output_path

    @classmethod
    def render_enum_help(cls,
                         topic: str,
                         enum_cls: Type[Enum]) -> str:
        if not hasattr(enum_cls, 'DEFAULT'):
            raise ValueError("Enum must declare case 'DEFAULT'")
        enum_help = f"{topic} ∈ {set([name.lower() for name, member in enum_cls.__members__.items()])}." \
                    f"\nDefaults to '{enum_cls.DEFAULT.value}'"
        return enum_help


class Tool:
    def __init__(self,
                 args: Args):
        self.args = args
        self.logger = StepLogger()

    def run(self):
        switcher = {
            'init': self.init,
            'config': self.config,
            'avail': self.list_available_packages,
            'list': self.list_installed_packages,
            'add': self.add_package,
            'new': self.new_generator,
            'make': self.make_generator,
            'export-netlist': self.export_netlist,
            'export-layout': self.export_layout,
            'lvs': self.run_lvs,
            'drc': self.run_drc,
            'rcx': self.run_rcx,
            'dump': self.dump_defaults,
            'parse': self.parse_parameters,
            'sim': self.run_measurements,
            'hdf5': self.hdf5_convert,
        }
        switcher[self.args.command]()

    @property
    def git_sources(self) -> GitSourceList:
        return self.args.config.git_sources

    def init(self):
        existing_file_strategy = ExistingFileStrategy.OVERWRITE if self.args.force \
                                                                else ExistingFileStrategy.WARN_AND_ABORT
        self.init_lib_files_and_run_dir(existing_file_strategy=existing_file_strategy)

    def init_lib_files_and_run_dir(self,
                                   existing_file_strategy: ExistingFileStrategy):
        candidates = [
            Env.bag_libs_def,
            Env.bag_server_port_path,
        ]
        match BAGFlowVariant.from_bag_config():
            case BAGFlowVariant.BAG2_OPEN_SOURCE:
                candidates.append(Env.xschemrc_path)
            case BAGFlowVariant.BAG2_VIRTUOSO:
                pass
            case _:
                raise NotImplementedError(f"unsupported flow variant {flow_variant}")

        existing_paths = list(filter(lambda cp: os.path.exists(cp), candidates))
        if len(existing_paths) >= 1:
            match existing_file_strategy:
                case ExistingFileStrategy.WARN_AND_ABORT:
                    for p in existing_paths:
                        warn(f"WARNING: already existing {p}")
                        error("ERROR: aborting, already existing files detected, use --force to overwrite")
                        sys.exit(1)
                case ExistingFileStrategy.OVERWRITE:
                    for p in existing_paths:
                        warn(f"WARNING: overwriting already existing {p}")
                        os.remove(p)

        if not os.path.exists(Env.generator_root):
            self.logger.step(f"Creating {Env.generator_root}")
            os.makedirs(Env.generator_root, exist_ok=True)

        # TODO: VIRTUOSO_RUN_DIR should be renamed to something tool agnostic,
        #       like BAG_RUN_DIR (see also comments in sourceme.sh)
        if not os.path.exists(Env.virtuoso_run_dir):
            self.logger.step(f"Creating {Env.virtuoso_run_dir}")
            os.makedirs(Env.virtuoso_run_dir, exist_ok=True)

        if not os.path.exists(Env.bag_libs_def):
            self.logger.step(f"Creating {Env.bag_libs_def}")
            with open(Env.bag_libs_def, 'w') as f:
                f.write('BAG_prim ${BAG_TECH_CONFIG_DIR}/Tech_primitives')

        if not os.path.exists(Env.bag_temp_path):
            self.logger.step(f"Creating {Env.bag_temp_path}")
            os.makedirs(Env.bag_temp_path, exist_ok=True)

        if not os.path.exists(Env.bag_server_port_path):
            self.logger.step(f"Creating {Env.bag_server_port_path}")
            with open(Env.bag_server_port_path, 'w') as f:
                f.write('0')

        match BAGFlowVariant.from_bag_config():
            case BAGFlowVariant.BAG2_OPEN_SOURCE:
                if not os.path.exists(Env.xschemrc_path):
                    self.logger.step(f"Creating {Env.xschemrc_path}")
                    os.makedirs(os.path.dirname(Env.xschemrc_path), exist_ok=True)
                    shutil.copy(Env.xschemrc_in_path, Env.xschemrc_path)
            case BAGFlowVariant.BAG2_VIRTUOSO:
                # NOTE: this is handled by the subcommand add
                pass
            case _:
                raise NotImplementedError(f"unsupported flow variant {flow_variant}")

        print("")

    def validate_libs_and_run_dir_exist(self):
        self.init_lib_files_and_run_dir(existing_file_strategy=ExistingFileStrategy.SKIP)

    def config(self):
        match self.args.config_command:
            case 'dump': self.config_dump()
            case 'sources-list': self.config_sources_list()
            case 'sources-add': self.config_sources_add()
            case 'sources-remove': self.config_sources_remove()
            case _:
                raise ValueError(f"INTERNAL ERROR: match does not handle {self.args.config_command}")

    def config_dump(self):
        # Fallback to dumping default config if no custom config is available
        if self.args.config_dump_current:
            if Env.gen_tool_config_path is None or not os.path.exists(Env.gen_tool_config_path):
                self.args.config_dump_current = False
                self.args.config_dump_default = True

        config: Config

        if self.args.config_dump_current:
            self.logger.important(f"Dumping config from {Env.gen_tool_config_path_variable}={Env.gen_tool_config_path}")
            config = self.args.config
        elif self.args.config_dump_default:
            self.logger.important(f"Dumping default config")
            self.logger.important(f"\tNOTE: customize environmental variable {Env.gen_tool_config_path_variable} "
                                  f"to point to a yaml file")
            config = Config.default_config()
        else:
            raise Exception("INTERNAL ERROR: either current or default config should be configured to be dumped")

        config.dump()

    def config_sources_list(self):
        config = self.args.config
        if self.args.config_sources_list_name_only:
            for src in config.git_sources:
                self.logger.important(src.name)
        else:
            self.logger.step("GIT sources")
            for src in config.git_sources:
                TermColor.GREEN.print(f"\t{src.name}")

            self.logger.step("Hint (for a collegue to add those)")
            for src in config.git_sources:
                self.logger.info(f"\tgen config sources-add --name {src.name} "
                                 f"--url \"{src.base_url}\"")

    def config_sources_add(self):
        config = self.args.config

        name = self.args.config_sources_add_name
        url = self.args.config_sources_add_url
        access_token = self.args.config_sources_add_access_token

        existing_names = set([src.name for src in config.git_sources])
        if name in existing_names:
            self.logger.error(f"ERROR: GIT source named '{name}' already exists")
            self.logger.info("\tplease remove this source if you're sure about it:")
            self.logger.info(f"\tgit config sources-remove --name {name}")
            exit(1)

        config.add_source(name=name, url=url, access_token=access_token)

    def config_sources_remove(self):
        config = self.args.config
        config.remove_source(name=self.args.config_sources_remove_name)

    def list_available_packages(self):
        self.init_lib_files_and_run_dir(existing_file_strategy=ExistingFileStrategy.SKIP)

        from sal.pkg.git_source import GitSource
        installed_pkg_names = list(map(lambda g: g.name, self.git_sources.installed_packages()))

        self.logger.info("Available packages to add:")
        for source in self.git_sources:
            pkgs_grouped_by_kind = GitSource.group_packages_by_kind(source.available_packages)
            for kind, packages in pkgs_grouped_by_kind.items():
                self.logger.info(f"\n\t-------------------- Source: ", end='')
                TermColor.GREEN.print(f"{source.name} ", end='')
                self.logger.info(f"[{kind.name}] --------------------")
                for package in packages:
                    self.logger.info(f"\tgen add {source.name}/{package.name}", end='')
                    if package.name in installed_pkg_names:
                        self.logger.info("   (already installed)")
                    else:
                        self.logger.info("")

                self.logger.important(f"\n\tTo install the whole library, call:\n\t\tgen add --all-of {source.name}\n")

        self.logger.info("\nFor details please call:\n\tgen add --help")

    def list_installed_packages(self):
        self.init_lib_files_and_run_dir(existing_file_strategy=ExistingFileStrategy.SKIP)

        self.logger.important("Installed packages:")
        names = list(map(lambda g: f"{g.source.name}/{g.name}", self.git_sources.installed_packages()))
        names.sort()
        for name in names:
            self.logger.info(f"\t{name}")

        self.logger.info("\nTo generate, please call:\n\tgen make <generator>")

    def cmd(self, args):
        self.logger.info(f"Calling {' '.join(args)}")

        s = subprocess.run(args)
        if s.returncode != 0:
            raise Exception(f'Command failed with status code {s.returncode}')

    def add_package(self):
        self.init_lib_files_and_run_dir(existing_file_strategy=ExistingFileStrategy.SKIP)

        if self.args.add_all_of_git_source:
            src = self.git_sources.source_by_name(name=self.args.add_all_of_git_source)
            pkgs = src.available_packages
            for pkg in pkgs:
                self.add_single_package(pkg=pkg)
        else:
            for name in self.args.packages:
                try:
                    pkg = self.git_sources.package_by_name(name=name,
                                                           default_git_source=self.args.config.default_git_source)
                    self.add_single_package(pkg=pkg)
                except Exception as e:
                    self.logger.error(f"ERROR: {e}")

    def __patch(self,
                path: str,
                filter_pattern: str,
                prepend_lines: Optional[List[str]] = None,
                append_lines: Optional[List[str]] = None,
                use_set_semantics: bool = True,
                ):
        if prepend_lines is None:
            prepend_lines = []
        if append_lines is None:
            append_lines = []
        original_lines = []
        filtered_lines = []
        if os.path.exists(path):
            with open(path, 'r') as f:
                original_lines = f.readlines()
                # remove previous entries of this generator
                regex = re.compile(filter_pattern)
                filtered_lines = list(filter(lambda l: regex.search(l) is None, original_lines))
        new_lines = prepend_lines + filtered_lines + append_lines

        if use_set_semantics and set(original_lines) == set(new_lines):
            self.logger.warn("\tNothing changed")
            return  # nothing to do

        tmp_path = path + ".new"
        with open(tmp_path, 'w') as f:
            f.writelines(new_lines)
        os.rename(tmp_path, path)

    def __patch_cds_lib(self,
                        pkg: PackageBase):
        self.logger.step("Patching cds.lib")
        self.logger.info(f"\tat {Env.cds_lib_path}")

        pattern: str
        if isinstance(pkg, GeneratorPackage):
            pattern = r"^\s*DEFINE\s+(" + pkg.templates_basename_virtuoso + "|" + pkg.generated_basename + r")\s+.*$"
        elif isinstance(pkg, TestbenchPackage):
            pattern = r"^\s*DEFINE\s+(" + pkg.templates_basename_virtuoso + r")\s+.*$"
        else:
            pattern = ""

        lines = [f"DEFINE {pkg.templates_basename_virtuoso} "
                 f"$BAG_GENERATOR_ROOT/{pkg.name}/{pkg.templates_basename_virtuoso}\n"]
        self.__patch(path=Env.cds_lib_path,
                     filter_pattern=pattern,
                     append_lines=lines)

    def __patch_xschemrc(self,
                         pkg: PackageName):
        self.logger.step("Patching xschemrc")
        self.logger.info(f"\tat {Env.xschemrc_path}")

        pattern: str
        if isinstance(pkg, GeneratorPackage):
            pattern = r"^\s*append\s+XSCHEM_LIBRARY_PATH\s+:.*(" + \
                      pkg.templates_basename_xschem + "|" + pkg.generated_basename + r")\s+.*$"
        elif isinstance(pkg, TestbenchPackage):
            pattern = r"^\s*append\s+XSCHEM_LIBRARY_PATH\s+:.*(" + pkg.templates_basename_xschem + r")\s+.*$"
        else:
            pattern = ""

        lines = [f"append XSCHEM_LIBRARY_PATH :$env(BAG_GENERATOR_ROOT)/{pkg.name}/{pkg.templates_basename_xschem}\n"]

        self.__patch(path=Env.xschemrc_path,
                     filter_pattern=pattern,
                     append_lines=lines)

    def __patch_bag_libs_def(self,
                             pkg: PackageBase):
        self.logger.step("Patching bags_lib.def")
        self.logger.info(f"\tat {Env.bag_libs_def}")

        self.__patch(path=Env.bag_libs_def,
                     filter_pattern=r"^\s*(BAG_prim|" + pkg.templates_libname + r")\s+.*$",
                     prepend_lines=["BAG_prim ${BAG_TECH_CONFIG_DIR}/Tech_primitives\n"],
                     append_lines=[f"{pkg.templates_libname} {Env.bag_modules_path_meta}\n"])

    def __patch_lib_files(self,
                          pkg: PackageBase):
        flow_variant = BAGFlowVariant.from_bag_config()
        match flow_variant:
            case BAGFlowVariant.BAG2_VIRTUOSO:
                self.__patch_cds_lib(pkg)
            case BAGFlowVariant.BAG2_OPEN_SOURCE:
                self.__patch_xschemrc(pkg)
            case _:
                raise NotImplementedError(f"unsupported flow variant {flow_variant}")

        self.__patch_bag_libs_def(pkg)

        self.logger.step(f"Creating BagModules/{pkg.templates_libname}")

        python_templates = os.path.join(Env.generator_root, "BagModules", pkg.templates_libname)
        os.makedirs(python_templates, exist_ok=True)

        self.logger.info("------------------------------------------------------")

    def add_single_package(self,
                           pkg: PackageBase):
        repo_path = os.path.join(Env.generator_root, pkg.name)

        self.logger.info(f"Adding package {pkg.url} as {pkg.name} ...")

        self.logger.step(f"Cloning GIT repo at {pkg.url}")

        if os.path.exists(repo_path):
            self.logger.warn(f"\tAlready existent directory at {repo_path} is kept as-is")
        else:
            self.cmd(["git", "clone", pkg.url, repo_path])

        self.__patch_lib_files(pkg=pkg)

    # NOTE: previously, generators were launched using configure/Makefile
    #       which basically called the __init__.py script:
    #
    #           main_script = os.path.join(path, gen.name, "__init__.py")
    #           args = ["python3", main_script]
    #           if Env.is_finfet:  # TODO: make this tech agnostic!
    #               args.append("--finfet")
    #           self.cmd(args)
    #
    #       => in the SDF flow the generator module is expected to
    #           - have a 'design' module
    #           - the 'design' module contains a class called 'design'
    #           - this base class is derived from sal.design_base.DesignBase
    #

    def instantiate_generator(self, generator: GeneratorPackage) -> DesignBase:
        if not os.path.isdir(generator.template_cell_path):
            msg = f"\nERROR: Template schematic/symbol library "\
                  f"'{os.path.basename(os.path.dirname(generator.template_cell_path))}' is missing.\n"
            if BAGFlowVariant.from_bag_config() == BAGFlowVariant.BAG2_OPEN_SOURCE:
                msg += "This generator package is not yet ready for the opensource flow.\n"
            self.logger.error(msg)
            self.logger.info(f"Expected at {generator.template_cell_path}")
            sys.exit(1)

        inst = generator.new_instance()

        if hasattr(self.args, 'params_path') and self.args.params_path is not None:
            import sal.params_base
            inst.params = generator.design_class.parameter_class().parse_dump(
                path=self.args.params_path,
                dump_format=sal.params_base.DumpFormat.YAML
            )
            self.logger.info(f"Using params from YAML file: {self.args.params_path}")

        return inst

    def make_generator(self):
        self.validate_libs_and_run_dir_exist()

        for gen in self.git_sources.installed_packages_by_names(names=self.args.generators):
            self.logger.step(f"Generating layout/schematic")

            inst = self.instantiate_generator(gen)

            inst.generate(run_lvs=False)  # NOTE: we rather use the lvs handling of the "gen lvs" cmd
                                          # TODO: this should be consolidated with design_base
            self.lvs_and_report(generator=gen, instance=inst)

    def export_netlist(self):
        self.validate_libs_and_run_dir_exist()

        for gen in self.git_sources.installed_packages_by_names(names=self.args.generators):
            inst = self.instantiate_generator(gen)

            lib_name: str
            cell_name: str
            out_file = os.path.abspath(self.args.output_path)

            match self.args.source:
                case NetlistExportSource.TEMPLATE:
                    lib_name = inst.template_library_name
                    cell_name = inst.name
                case NetlistExportSource.GENERATED:
                    lib_name = inst.implementation_library_name
                    cell_name = inst.name
                case _:
                    raise ValueError(f"Unsupported netlist source {self.args.source}")

            checker_args = inst.bag_project.bag_config['database']['checker']
            checker_cls = checker_args['checker_cls']

            self.logger.step(f"Exporting netlist for {lib_name}/{cell_name}")

            # NOTE: for now, only SPICE is supported
            match self.args.format:
                case NetlistFormat.SPICE:
                    import bag.verification
                    from bag.concurrent.core import batch_async_task

                    # NOTE: Virtuoso checker creates log and tmp files in the same directory as the netlist path,
                    #       so we first extract the netlist into a run directory,
                    #       then we copy the file to the final destination
                    run_dir = os.path.join(Env.virtuoso_run_dir, "export_netlist", lib_name)
                    os.makedirs(run_dir, exist_ok=True)

                    netlist_path = os.path.join(run_dir, "schematic.net")

                    checker_args.pop("checker_cls")
                    
                    checker = bag.verification.make_checker(checker_cls, os.environ["BAG_TEMP_DIR"], **checker_args)
                    checker._interface = inst.bag_project.impl_db
                    coro = checker.async_export_schematic(lib_name,
                                                          cell_name,
                                                          netlist_path,
                                                          view_name='schematic')
                    results = batch_async_task([coro])
                    if results is None or isinstance(results[0], Exception):
                        self.logger.error(f"Failed to export netlist due to error: {results[0]}")
                        sys.exit(1)

                    log_fname = results[0]
                    self.logger.important(f"Exporting netlist:")
                    self.logger.info(f"\tCell: {lib_name}/{cell_name}")
                    self.logger.info(f"\tOutput Path: {out_file}")
                    self.logger.info(f"\tLog: {log_fname}")

                    if not os.path.isfile(netlist_path):
                        self.logger.error(f"Exported netlist file was not created at: {netlist_path}")
                        sys.exit(1)

                    shutil.copy(netlist_path, out_file)

                case _:
                    raise ValueError(f"Unsupported netlist format {self.args.format}")

    def export_layout(self):
        self.validate_libs_and_run_dir_exist()

        for gen in self.git_sources.installed_packages_by_names(names=self.args.generators):
            inst = self.instantiate_generator(gen)

            match self.args.format:
                case LayoutFormat.GDS:
                    lib_name = inst.implementation_library_name
                    cell_name = inst.name
                    out_file = os.path.abspath(self.args.output_path)
                    log_fname = inst.bag_project.export_layout(lib_name,
                                                               cell_name,
                                                               out_file)
                    self.logger.important(f"Exporting layout:")
                    self.logger.info(f"\tCell: {lib_name}/{cell_name}")
                    self.logger.info(f"\tOutput Path: {out_file}")
                    self.logger.info(f"\tLog: {log_fname}")

                case _:
                    raise ValueError(f"Unsupported layout format {self.args.format}")

    def validate_verification_log(self,
                                  topic: str,
                                  check_successful: Bool,
                                  log_path: str,
                                  treat_errors_as_warnings: Bool) -> Bool:
        """
        Validation distinguishes between
        1) fatal errors, like
           - no report/log created at all
           - licensing error in log, therefore no validation was done at all
        2) non-fatal issues, like
           - unsuccessful check result `check_successful`
           - or other warnings/errors in the log file

        The reason is for fatal errors it does not make sense to show the report GUI.
        For non-fatal errors by default the report GUI will be shown in case of errors,
        and errors will be logged with red colors.
        However, if the option treat_errors_as_warnings is set,
        (for example for DRC use case) issues will be logged as warnings, in yellow color.

        Parameters
        ----------
        topic
            Topic of the verification (e.g. "LVS" or "DRC")

        check_successful
            True if BAG2 LVS/DRC code reports success

        log_path
            Path to the log file

        treat_errors_as_warnings
            If true, non-fatal errors will be printed as warnings

        Returns
        -------
        False if a fatal error was detected (makes showing the report GUI useless),
        True otherwise
        """
        fatal_error_detected = False

        licensing_regex_pattern = re.compile("could not be licensed sufficiently")
        if not os.path.isfile(log_path):
            self.logger.error(f"INTERNAL Error: {topic} log file does not exist at {log_path}")
            fatal_error_detected = True
        analysis = LogAnalyzer.default().analyze_path(log_path)
        if not check_successful or not analysis.successful:
            if analysis.find_error_lines(licensing_regex_pattern):
                self.logger.error(f"ERROR: {topic} licensing error, please check {log_path}")
                fatal_error_detected = True
            elif treat_errors_as_warnings:
                self.logger.warn(f"WARNING: {topic} not clean, please check {log_path}")
            else:
                self.logger.error(f"ERROR: {topic} not clean, please check {log_path}")
            analysis.print_issues(print_warnings=False)
        return not fatal_error_detected

    def lvs_and_report(self,
                       generator: GeneratorPackage,
                       instance: sal.design_base.DesignBase):
        self.logger.step(f"Checking LVS for {instance.implementation_library_name}/{instance.name}")

        successful, log_path = instance.bag_project.run_lvs(instance.implementation_library_name, instance.name)
        if successful:
            self.logger.success(f"LVS succeeded ({log_path})")
        else:
            if not self.validate_verification_log(topic="LVS",
                                                  check_successful=successful,
                                                  log_path=log_path,
                                                  treat_errors_as_warnings=False):
                sys.exit(1)

        if self.args.show_report_gui.shall_show(successful):
            run_dir = os.path.dirname(log_path)

            # NOTE: this should be moved to the bag_framework Checker interface!
            #       subclasses can then implement template methods for opening report GUIs
            checker_cls = instance.bag_project.bag_config['database']['checker']['checker_cls']
            if checker_cls == "bag.verification.calibre.Calibre":
                report_db = os.path.join(run_dir, "svdb/")
                if not os.path.exists(report_db):
                    self.logger.error(f"\nERROR: Expected LVS report directory does not exist at: {report_db}")
                    return
                if len(os.listdir(report_db)) == 0:
                    self.logger.error(f"\nERROR: Expected LVS report directory is empty at: {result_file}")
                    return

                self.logger.step("Opening report GUI...")

                args = [
                    'calibre', '-rve', '-lvs',
                    report_db,
                    instance.name
                ]
                self.cmd(args)
            elif checker_cls == "bag.verification.pvs.PVS":
                self.logger.step("Opening report GUI...")

                args = [
                    'lvsbrowser',
                    '-runDir', run_dir,
                    '-viewer', 'none'
                ]
                self.cmd(args)
            elif checker_cls == "opensource.bag.verification.klayout.KLayoutChecker":
                gds_file = os.path.join(run_dir, f"layout.gds")
                lvsdb_file = os.path.join(run_dir, 'klayout_lvs.lvsdb')
                if not os.path.exists(lvsdb_file):
                    self.logger.error(f"\nERROR: Expected LVS report database does not exist at: {lvsdb_file}")
                    return

                self.logger.step("Opening report GUI...")

                args = [
                    'klayout',
                    gds_file,
                    '-mn',
                    lvsdb_file
                ]
                self.cmd(args)
        if not successful: sys.exit(1)

    def drc_and_report(self,
                       generator: GeneratorPackage,
                       instance: sal.design_base.DesignBase):
        self.logger.step(f"Starting DRC for {instance.implementation_library_name}/{instance.name}")

        successful, log_path = instance.bag_project.run_drc(instance.implementation_library_name, instance.name)
        if successful:
            self.logger.success(f"DRC succeeded ({log_path})")
        else:
            if not self.validate_verification_log(topic="DRC",
                                                  check_successful=successful,
                                                  log_path=log_path,
                                                  treat_errors_as_warnings=True):
                sys.exit(1)

        if self.args.show_report_gui.shall_show(successful):
            run_dir = os.path.dirname(log_path)

            # TODO: this should be moved to the bag_framework Checker interface!
            #       subclasses can then implement template methods for opening report GUIs
            checker_cls = instance.bag_project.bag_config['database']['checker']['checker_cls']
            if checker_cls == "bag.verification.calibre.Calibre":
                result_file = os.path.join(run_dir, f"{instance.name}.drc.results")
                if not os.path.exists(result_file):
                    self.logger.error(f"\nERROR: Expected DRC result file does not exist at: {result_file}")
                    return

                self.logger.step("Opening report GUI...")
                args = [
                    'calibre', '-rve', '-drc',
                    result_file
                ]
                self.cmd(args)
            elif checker_cls == "bag.verification.pvs.PVS":
                self.logger.step("Opening report GUI...")

                ascii_error_path = os.path.join(run_dir, f"{instance.name}.drc_errors.ascii")

                args = [
                    'drcbrowser',
                    '-ascii', ascii_error_path
                ]
                self.cmd(args)
            elif checker_cls == "opensource.bag.verification.klayout.KLayoutChecker":
                gds_file = os.path.join(run_dir, f"layout.gds")
                lyrdb_files = glob.glob(run_dir + "/*.lyrdb")

                if len(lyrdb_files) == 0:
                    self.logger.error(f"\nERROR: Expected at least one DRC report database *.lyrdb to exist within {run_dir}")
                    return

                self.logger.step("Opening report GUI...")

                args = [
                    'klayout',
                    gds_file,
                ]
                for lyrdb_file in lyrdb_files:
                    args.extend(['-m', lyrdb_file])
                self.cmd(args)
        if not successful: sys.exit(1)

    def run_lvs(self):
        self.validate_libs_and_run_dir_exist()

        for gen in self.git_sources.installed_packages_by_names(names=self.args.generators):
            inst = self.instantiate_generator(gen)

            # NOTE: for now, we directly call the bag project functions
            # inst.run_lvs()
            self.lvs_and_report(generator=gen, instance=inst)

    def run_drc(self):
        self.validate_libs_and_run_dir_exist()

        for gen in self.git_sources.installed_packages_by_names(names=self.args.generators):
            inst = self.instantiate_generator(gen)

            # NOTE: for now, we directly call the bag project functions
            # inst.run_drc()
            self.drc_and_report(generator=gen, instance=inst)

    def run_rcx(self):
        self.validate_libs_and_run_dir_exist()

        self.logger.step(f"Extracting parasitics from layout")

        for gen in self.git_sources.installed_packages_by_names(names=self.args.generators):
            inst = self.instantiate_generator(gen)

            rcx_params = {}

            if self.args.rcx_extraction_type:
                self.logger.info(f"Using RCX extraction type: {self.args.rcx_extraction_type}\n")
                rcx_params['netlist_type'] = self.args.rcx_extraction_type

            rcx_success, log_file = inst.bag_project.run_rcx(lib_name=inst.implementation_library_name,
                                                             cell_name=inst.name,
                                                             rcx_params=rcx_params)
            if rcx_success:
                self.logger.info(f"RCX succeeded, log file: {log_file}")
            else:
                self.logger.error(f"ERROR: RCX failed, please check {log_file}")

    def dump_defaults(self):
        self.validate_libs_and_run_dir_exist()
        
        from sal.bag_startup import BagStartup
        BagStartup.setup_sys_path()  # NOTE: add frameworks to sys.path, this influences the following imports!
        import sal.params_base

        gen = self.git_sources.installed_package_by_name(self.args.generators[0])
        inst = self.instantiate_generator(gen)

        if self.args.params_path is None:  # print to stdout
            self.logger.info(f"\nYAML dump of default parameters for {gen.name}:\n", flush=True)
            inst.params.dump(dump_format=sal.params_base.DumpFormat.YAML)
        else:
            inst.params.dump(dump_format=sal.params_base.DumpFormat.YAML, path=self.args.params_path)

    def parse_parameters(self):
        from sal.bag_startup import BagStartup
        BagStartup.setup_sys_path()  # NOTE: add frameworks to sys.path, this influences the following imports!
        import sal.params_base

        gen = self.git_sources.installed_packages_by_name(self.args.generators[0])
        self.logger.info(f"\nParse YAML dump for {gen.name}:\n", flush=True)
        obj = gen.design_class.parameter_class().parse_dump(path=self.args.params_path,
                                                            dump_format=sal.params_base.DumpFormat.YAML)
        print(obj)

    def new_generator(self):
        self.validate_libs_and_run_dir_exist()

        new_pkg_name = self.args.packages[0]
        new_pkg = self.git_sources.package_by_name(name=new_pkg_name,
                                                   default_git_source=self.args.config.default_git_source)

        if os.path.exists(new_pkg.path):
            if self.args.overwrite:
                shutil.rmtree(new_pkg.path)
            else:
                self.logger.error(f"ERROR: Already existent directory at {new_pkg.path}, please back up or remove it")
                sys.exit(1)

        prototype_pkg: PackageBase
        prototype_pkg = self.git_sources.package_by_name(name=self.args.reference_package,
                                                         default_git_source="mosaic_opamp")  # default cell is tgate

        self.logger.step(f"Creating package directory structure "
                         f"(based on {self.args.reference_package})")
        self.logger.info(f"\tat {new_pkg.path}")

        # NOTE: generator name (i.e. tgate_gen) is the dir name for the Python Source
        tmp_dir = os.path.join(new_pkg.path, 'tmp')
        tmp_git_clone = os.path.join(tmp_dir, prototype_pkg.name)

        # NOTE: relocate path to our checkout!
        prototype_pkg.path = tmp_git_clone

        try:
            self.cmd(["git", "clone", prototype_pkg.url, tmp_git_clone])

            # copy files as a starting point for the new generator

            # Copy Python scripts
            shutil.copytree(prototype_pkg.scripts_path, new_pkg.scripts_path)

            # Copy virtuoso/OA template lib
            if os.path.isdir(prototype_pkg.template_cell_path_virtuoso):  # generator has virtuoso sym/sch
                os.makedirs(os.path.join(new_pkg.path, new_pkg.templates_basename_virtuoso))
                shutil.copytree(prototype_pkg.template_cell_path_virtuoso, new_pkg.template_cell_path_virtuoso)

            # Copy xschem DB template lib
            if os.path.isdir(prototype_pkg.template_cell_path_xschem):  # generator has xschem sym/sch
                os.makedirs(os.path.join(new_pkg.path, new_pkg.template_cell_path_xschem))
                for suffix in ('sym', 'sch'):
                    prototype_xschem_file = os.path.join(prototype_pkg.template_cell_path_xschem,
                                                         f"{prototype_pkg.cell_name}.{suffix}")
                    new_xschem_file = os.path.join(new_pkg.template_cell_path_xschem,
                                                   f"{new_pkg.cell_name}.{suffix}")
                    if os.path.isfile(prototype_xschem_file):
                        shutil.copy(prototype_xschem_file, new_xschem_file)

            # Copy .gitignore
            template_gitignore_file = os.path.join(tmp_git_clone, ".gitignore")
            new_gitignore_file = os.path.join(new_pkg.path, ".gitignore")

            if os.path.exists(template_gitignore_file):
                shutil.copy(template_gitignore_file, new_gitignore_file)
            else:
                with open(new_gitignore_file, "w") as f:
                    f.writelines(["*.pyc\n", "__pycache__\n", "*~\n", "core.*\n"])

            # patch python scripts to have the new name

            self.logger.step(f"Patching Python files to use package name {new_pkg_name}")

            for root, dirs, files in os.walk(new_pkg.scripts_path):
                for f in files:
                    if f.endswith('.py'):
                        script_path = os.path.join(root, f)
                        self.cmd(["sed", "-i", f's/{prototype_pkg.cell_name}/{new_pkg.cell_name}/g', script_path])

            self.logger.step(f"Setting up GIT repo for {new_pkg_name}")

            new_pkg.source.init_repo(package=new_pkg)

        finally:
            # sanity check, we don't want to remove something like /
            if len(tmp_git_clone) >= 2 and tmp_git_clone.startswith(new_pkg.path):
                shutil.rmtree(tmp_dir, ignore_errors=True)

        self.__patch_lib_files(pkg=new_pkg)

        flow_variant = BAGFlowVariant.from_bag_config()
        match flow_variant:
            case BAGFlowVariant.BAG2_VIRTUOSO:
                self.logger.warn(f"\nPlease update Virtuoso and add Symbol / Schematic "
                                 f"to library {new_pkg.templates_basename_virtuoso}!")
            case BAGFlowVariant.BAG2_OPEN_SOURCE:
                pass
            case _:
                raise NotImplementedError(f"unsupported flow variant {flow_variant}")

    def list_simulation_options(self):
        self.logger.info("Available simulation modes [--mode or -m]:")
        for mode in SimulationMode:
            self.logger.info(f"\t- {mode.value} ... {mode.description}")

        self.logger.info("\nInstalled testbenches:")

        from sal.pkg.git_source import GitSource
        installed_pkg_names = list(map(lambda g: g.name, self.git_sources.installed_packages()))

        for source in self.git_sources:
            pkgs_grouped_by_kind = GitSource.group_packages_by_kind(source.available_packages)
            for kind, packages in pkgs_grouped_by_kind.items():
                if kind != TestbenchPackage.kind():
                    continue
                self.logger.info(f"\t-------------------- Source: {source.name} --------------------")
                for package in packages:
                    self.logger.info(f"\t- {package.name}", end='')
                    if package.name in installed_pkg_names:
                        self.logger.info("   (already installed)")
                    else:
                        self.logger.info("")
                self.logger.info('\n')

    def list_supported_measurements(self, generator: GeneratorPackage):
        gen_cls = generator.design_class
        meas_clses = gen_cls.measurement_classes()
        if self.args.measurement is None:
            self.logger.info("Supported Measurements:")
            for meas_cls in meas_clses:
                self.logger.important(f"\t{meas_cls.name()}", end='')
                self.logger.info(f": {meas_cls.description()}")

    def run_measurements(self):
        if self.args.list:
            self.list_simulation_options()
            sys.exit(0)

        self.validate_libs_and_run_dir_exist()

        gen_name = self.args.generators[0]
        pkg = self.git_sources.installed_package_by_name(gen_name)
        if pkg.kind() != GeneratorPackage.kind():
            self.logger.error(f"ERROR: Expected 'Generator' package, but {gen_name} is a package of kind '{pkg.kind().name}'")
            sys.exit(1)
        generator: GeneratorPackage = cast(GeneratorPackage, pkg)

        gen_cls = generator.design_class
        meas_clses = gen_cls.measurement_classes()
        if self.args.measurement is None:
            self.list_supported_measurements(generator=generator)
            sys.exit(1)

        meas_cls = list(filter(lambda c: c.name() == self.args.measurement, meas_clses))
        if len(meas_cls) == 0:
            self.logger.error(f"ERROR: Measurement '{self.args.measurement}' is not supported.")
            self.list_supported_measurements(generator=generator)
            sys.exit(1)

        for cls in meas_cls:
            self.run_measurement(generator=generator, measurement_class=cls)

    def run_measurement(self,
                        generator: GeneratorPackage,
                        measurement_class: Type[MeasurementBase]):
        import bag

        meas_param_class = measurement_class.parameter_class()
        # tb_setups = measurement_class.testbench_setup_list()

        self.logger.step(f"Measurement '{measurement_class.name()}' for generator package '{generator.name}'")
        self.logger.info(f"Description: {measurement_class.description()}")

        inst: DesignBase = self.instantiate_generator(generator)

        matching_params = list(filter(lambda p: p.__class__ == meas_param_class,
                                      inst.params.measurement_parameters))
        if len(matching_params) == 0:
            self.logger.error(f"ERROR: Generator package '{generator.name}' must provide "
                              f"measurement parameter class for '{measurement_class.name()}'")
            sys.exit(1)

        meas_params = matching_params[0]
        m = measurement_class(params=meas_params)

        state_cls = m.fsm_state_class()
        state = state_cls.initial_state()
        output = {}
        i = 0

        while state not in state_cls.final_states():
            self.logger.info(f"Next state: {state}")
            tb_run = m.prepare_testbench(current_state=state, previous_output=output)

            tb_name = tb_run.setup.testbench_name

            self.logger.info(f"Next testbench: {tb_name}")

            tb_pkg = cast(TestbenchPackage, self.git_sources.installed_package_by_name(tb_name))

            tb_gen_cell = f"{generator.name}_{tb_name}"  # i.e. "tgate_gen_dc_tb"
            data_dir = os.path.join(Env.virtuoso_run_dir,
                                    tb_gen_cell,
                                    "simulation_data")
            self.logger.info(f"Simulation Result Data: {data_dir}")
            state_dir_path = os.path.join(data_dir, f"{tb_gen_cell}_{i:0>4d}_{state}")
            hdf5_path = os.path.join(data_dir, state_dir_path, "result.hdf5")

            if self.args.only_postprocess:
                if not os.path.exists(hdf5_path):
                    error(f"Argument --only-postprocess was configured, but HDF5 file is missing at expected path "
                          f"{hdf5_path}")
                    sys.exit(1)
                output = SimulationResult.from_hdf5(hdf5_path)
            else:
                output = self.run_simulation(testbench=tb_pkg,
                                             testbench_run=tb_run,
                                             mode=self.args.sim_mode,
                                             generator=generator,
                                             output_hdf5_path=hdf5_path)
            output_dict = output.as_bag_dict()  # TODO: get rid of dictionary representation
            state, output = m.process_output(current_state=state,
                                             current_output=output_dict,
                                             testbench_run=tb_run)

            self.logger.info(f"Simulation output:\n{output}")

        self.logger.info(f"Reached the final state: {state}")

    def run_simulation(self,
                       testbench: TestbenchPackage,
                       testbench_run: TestbenchRun,
                       mode: SimulationMode,
                       generator: GeneratorPackage,
                       output_hdf5_path: str) -> SimulationResult:
        from sal.bag_startup import BagStartup
        BagStartup.setup_sys_path()  # NOTE: add frameworks to sys.path, this influences the following imports!
        import bag

        tb = testbench.new_instance()
        gen = generator.new_instance()

        testbench_run.instance = tb

        if mode is SimulationMode.SCHEMATIC:
            view_name = 'schematic'
        elif mode is SimulationMode.POST_LAYOUT:
            try:  # attempt to get the extracted view name configured in bag_config.yaml
                view_name = gen.bag_project.bag_config['database']['calibreview']['view_name']
            except KeyError:
                view_name = "calibre"
                self.logger.warn(f"bag_config.yaml contains no database/calibreview/view_name, using fallback {view_name}")
        else:
            raise Exception(f"Unsupported simulation mode {mode}")

        sim_envs: [str]
        try:  # attempt to get the extracted view name configured in bag_config.yaml
            default_env = gen.bag_project.bag_config['database']['testbench']['default_env']
            sim_envs = [default_env]
        except KeyError:  # fallback
            default_env = 'TT'
            self.logger.warn(f"bag_config.yaml contains no database/testbench/default_env, using fallback {default_env}")
            sim_envs = [default_env]

        impl_lib = generator.generated_basename
        gen_cell = generator.cell_name

        tb_gen_cell = f"{generator.name}_{tb.package}"  # "tgate_gen_dc_tb"

        # __________________________________________________________________________________
        # import testbench templates (generate netlist if necessary)
        self.logger.step("Importing testbench template")

        tb.import_testbench_template(bag_project=gen.bag_project,
                                     template_library=testbench.templates_basename,
                                     cell=testbench.cell_name)

        dut_wrapper = testbench_run.setup.dut_wrapper

        if dut_wrapper is None:
            self.logger.step(f"No DUT wrapper necessary, directly using DUT cell '{generator.cell_name}'")

            # Patch DUT info
            testbench_run.params.dut.lib = impl_lib
            testbench_run.params.dut.cell = generator.cell_name
        else:
            self.logger.step(f"Importing DUT wrapper template '{dut_wrapper.template_cell}'")

            dsn = gen.bag_project.create_design_module(lib_name=generator.templates_basename,
                                                       cell_name=dut_wrapper.template_cell)
            self.logger.step(f"Generating schematic for DUT wrapper '{dut_wrapper.template_cell}'")
            self.logger.info(f"\twraps DUT cell '{gen_cell}'")

            params: TestbenchParamsBase = testbench_run.params.copy()
            params.dut.lib = impl_lib
            params.dut.cell = gen_cell

            dsn.design(params=params)
            dsn.implement_design(impl_lib, top_cell_name=dut_wrapper.template_cell)

            # Patch DUT info
            testbench_run.params.dut.lib = impl_lib
            testbench_run.params.dut.cell = dut_wrapper.template_cell

            self.logger.info('Finished implementing schematic')

        # __________________________________________________________________________________
        # create the testbench schematic
        self.logger.step("Creating the testbench schematic")
        self.logger.info(f"\tplugging in DUT cell '{testbench_run.params.dut.cell}'")

        tb_dsn = gen.bag_project.create_design_module(lib_name=testbench.templates_basename,
                                                      cell_name=testbench.cell_name)
        tb_dsn.design(dut_lib=testbench_run.params.dut.lib,
                      dut_cell=testbench_run.params.dut.cell,
                      params=testbench_run.params)
        tb_dsn.implement_design(lib_name=impl_lib,
                                top_cell_name=tb_gen_cell)
        # __________________________________________________________________________________
        self.logger.step("Setting up testbench")

        # setup testbench state
        bag_tb = gen.bag_project.configure_testbench(
            tb_lib=impl_lib,
            tb_cell=tb_gen_cell
        )
        self.logger.info('tb object created!')

        sim_access_additions = SimAccessAdditions.instance_for_flow_variant()

        # set testbench parameters values
        sim_params = testbench_run.params.effective_simulation_params()
        self.logger.info(f"Effective Simulation params:\n{sim_params}")

        for key, val in sim_params.variables.items():
            bag_tb.set_parameter(key, val)

        for key, val in sim_params.sweeps.items():
            bag_tb.set_sweep_parameter(key, **val.to_bag_args())

        for key, val in sim_params.outputs.items():
            # NOTE: outputs are specified differently in different tools, for example
            #       ADEXL:
            #           getData("vout" ?result 'tran)
            #           v("vout" ?result 'tran)
            #           mag(v("vout" ?result 'tran))
            #       ngspice:
            #           v(tran.vout)
            #           vm(tran.vout)
            output = sim_access_additions.format_output(val)
            bag_tb.add_output(key, output)

        self.logger.info('parameters are set!')

        # set config view, i.e. schematic vs extracted
        bag_tb.set_simulation_view(impl_lib, gen_cell, view_name)
        self.logger.info(f'changed the config view to {view_name}!')

        # set process corners
        bag_tb.set_simulation_environments(sim_envs)
        self.logger.info(f'set tb environment to {sim_envs}')

        # commit changes to testbench state back to database
        bag_tb.update_testbench()
        self.logger.info('updated changes to testbench state')

        self.logger.info(f"Output directories:\n\tTestbench State: {bag_tb.save_dir}\n\t")

        # start simulation
        self.logger.step("Running simulation")
        sim_log_dir = bag_tb.run_simulation()
        if sim_log_dir is None:
            self.logger.warn(f"simulation was cancelled")
            raise RuntimeError("simulation was cancelled")
        else:
            self.logger.info(f"Simulation log dir: {sim_log_dir}")
            sim_access_additions.validate_simulation_log(log_dir=sim_log_dir)

            # import simulation results to Python
            self.logger.info('simulation done, load results')
            result = sim_access_additions.load_simulation_result(bag_tb=bag_tb,
                                                                 outputs=sim_params.outputs)

            # save simulation data as HDF5 format
            self.logger.step("Saving simulation result data")
            sim_access_additions.save_simulation_result(result=result, hdf5_path=output_hdf5_path)
            self.logger.info(f"Output file: {output_hdf5_path}")
            self.logger.info("Finished simulations.")

            return result

    def hdf5_convert(self):
        from sal.bag_startup import BagStartup
        BagStartup.setup_sys_path()  # NOTE: add frameworks to sys.path, this influences the following imports!

        tb_results = SimulationResult.from_hdf5(self.args.input_path)

        if self.args.output_path:
            with open(self.args.output_path, 'w') as f:
                print(tb_results, file=f)
        else:
            print(tb_results)


def main():
    args = Args()

    if args.verbose:
        info("Called with environmental variables: %s" % Env.__dict__)
        info("------------------------------------------------------")
        info("Called with Arguments: %s" % args.__dict__)
        info("------------------------------------------------------")

    tool = Tool(args=args)
    tool.run()


if __name__ == '__main__':
    main()
