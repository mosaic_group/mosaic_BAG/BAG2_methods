#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from enum import Enum
from typing import *

import bag.layout.util


class XAlignment(str, Enum):
    LEFT = 'l'
    XCENTER = 'c'
    RIGHT = 'r'


class YAlignment(str, Enum):
    TOP = 't'
    YCENTER = 'c'
    BOTTOM = 'b'


LEFT = XAlignment.LEFT
XCENTER = XAlignment.XCENTER
RIGHT = XAlignment.RIGHT

TOP = YAlignment.TOP
YCENTER = YAlignment.YCENTER
BOTTOM = YAlignment.BOTTOM


@dataclass
class Alignment:
    x: XAlignment
    y: YAlignment

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f"({str(self.x.value)}/{str(self.y.value)})"

    def align_within(self,
                     inner_bbox: bag.layout.util.BBox,
                     outer_bbox: bag.layout.util.BBox) -> bag.layout.util.BBox:
        # normalize bottom left box origin to (0, 0)
        normalized_bbox = inner_bbox.move_by(dx=-inner_bbox.left_unit,
                                             dy=-inner_bbox.bottom_unit,
                                             unit_mode=True)
        dx: int
        dy: int

        match self.x:
            case  XAlignment.LEFT:
                dx = 0
            case XAlignment.XCENTER:
                dx = (outer_bbox.width_unit - inner_bbox.width_unit) / 2
            case XAlignment.RIGHT:
                dx = outer_bbox.width_unit - inner_bbox.width_unit
            case _:
                raise ValueError(f"unknown enum case {self.x}")

        match self.y:
            case YAlignment.BOTTOM:
                dy = 0
            case YAlignment.YCENTER:
                dy = (outer_bbox.height_unit - inner_bbox.height_unit) / 2
            case YAlignment.TOP:
                dy = outer_bbox.height_unit - inner_bbox.height_unit
            case _:
                raise ValueError(f"unknown enum case {self.y}")

        return normalized_bbox.move_by(outer_bbox.left_unit + dx,
                                       outer_bbox.bottom_unit + dy,
                                       unit_mode=True)


Alignment.CENTER_CENTER = Alignment(XAlignment.XCENTER, YAlignment.YCENTER)
