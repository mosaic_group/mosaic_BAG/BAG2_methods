#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass, field
from enum import Enum
from typing import *
import numpy as np

import bag.layout.objects
from bag.layout.template import TemplateBase

from .alignment import Alignment
from .placement import InstanceToBePlaced, PlacedInstances


@dataclass
class ManualPlacement:
    instance_matrix: List[List[InstanceToBePlaced]]
    inter_column_tracks: Optional[List[List[int]]] = None
    inter_row_tracks: Optional[List[List[int]]] = None
    default_alignment: Alignment = Alignment.CENTER_CENTER
    alignment_override: List[(InstanceToBePlaced, Alignment)] = field(default_factory=list)

    @property
    def num_rows(self):
        return len(self.instance_matrix)

    @property
    def num_cols(self):
        if self.num_rows == 0:
            return 0
        return len(self.instance_matrix[0])

    def validate(self):
        if self.num_rows == 0:
            raise ValueError(f"Instance matrix expected to be non-empty!")

        if self.num_cols == 0:
            raise ValueError(f"Instance matrix first row expected to be non-empty!")

        for idx, col in enumerate(self.instance_matrix):
            num_cells = len(col)
            if num_cells != self.num_cols:
                raise ValueError(f"Instance matrix must be rectangular! "
                                 f"First row has {self.num_cols} cells, but row {idx} has {num_cells} cells")

        used_inst_names: Set[str] = set()
        for rows in self.instance_matrix:
            for inst in rows:
                if not inst:
                    continue
                if inst.name in used_inst_names:
                    raise ValueError(f"Instance names must be unique, "
                                     f"but obtained multiple instances named '{inst.name}'")
                used_inst_names.add(inst.name)

        if self.inter_row_tracks:
            expected_num_irt = self.num_rows - 1
            if len(self.inter_row_tracks) != expected_num_irt:
                raise ValueError(f"Expected {expected_num_irt} inter column track definitions, "
                                 f"but obtained {len(self.inter_row_tracks)}")

        if self.inter_column_tracks:
            expected_num_ict = self.num_cols - 1
            if len(self.inter_column_tracks) != expected_num_ict:
                raise ValueError(f"Expected {expected_num_ict} inter column track definitions, "
                                 f"but obtained {len(self.inter_column_tracks)}")

        for inst, alignment in self.alignment_override:
            if inst.name not in used_inst_names:
                raise ValueError(f"alignment_override found for instance named {inst.name}, "
                                 f"but this instance is not part of the placement instance matrix")

    def alignment_matrix(self) -> List[List[Alignment]]:
        alignment_override_by_inst_name = {inst.name: alignment for inst, alignment in self.alignment_override}

        a_matrix: List[List[Alignment]] = []
        for (i, i_row) in enumerate(self.instance_matrix):
            a_row = []
            a_matrix.append(a_row)
            for (j, inst) in enumerate(i_row):
                if inst:
                    alignment = alignment_override_by_inst_name.get(inst.name, self.default_alignment)
                else:
                    alignment = self.default_alignment
                a_row.append(alignment)
        return a_matrix


class ManualPlacer:
    def __init__(self,
                 template: TemplateBase,
                 placement: ManualPlacement):
        self.template = template
        self.placement = placement
        self._placed_instances: PlacedInstances = PlacedInstances(template=template, instances=[])  # dummy placeholder

        self.placement.validate()

    def placed_instances(self):
        return self._placed_instances

    @staticmethod
    def _get_transformed_inst_bbox(inst: Optional[InstanceToBePlaced]) -> Optional[bag.layout.objects.BBox]:
        if not inst:
            return None

        bbox = inst.master.bound_box
        if inst.orientation == '':
            return bbox
        bbox_rot = bbox.transform(orient=inst.orientation)
        return bbox_rot

    @staticmethod
    def _get_bbox_width(bbox: Optional[bag.layout.objects.BBox]) -> int:
        if not bbox:
            return 0
        return bbox.width_unit

    @staticmethod
    def _get_bbox_height(bbox: Optional[bag.layout.objects.BBox]) -> int:
        if not bbox:
            return 0
        return bbox.height_unit

    def place(self):
        # N x M instance matrix
        #
        # N x M size matrix
        # -> determine max widths for each col
        # -> determine max height for each row

        # NOTE: grid origin is bottom-left (so we place rows in reversed order)
        inst_matrix = np.matrix(self.placement.instance_matrix)
        inst_matrix = np.matrix(np.flipud(inst_matrix))

        alignment_matrix = np.matrix(self.placement.alignment_matrix())
        alignment_matrix = np.matrix(np.flipud(alignment_matrix))
        print(f"alignment matrix: {alignment_matrix}")

        get_transformed_inst_bbox = np.vectorize(self._get_transformed_inst_bbox)
        inst_bbox_matrix = get_transformed_inst_bbox(inst_matrix)
        print(f"inst_bbox_matrix: {inst_bbox_matrix}")

        get_width = np.vectorize(self._get_bbox_width)
        get_height = np.vectorize(self._get_bbox_height)

        wm = get_width(inst_bbox_matrix)
        print(f"width matrix {wm}")

        hm = get_height(inst_bbox_matrix)
        print(f"height matrix {hm}")

        max_widths_per_col = np.amax(wm.tolist(), axis=0)
        print(f"max_widths_per_col: {max_widths_per_col}")

        max_heights_per_row = np.amax(hm.tolist(), axis=1)
        print(f"max_heights_per_row: {max_heights_per_row}")

        # TODO: configurable layer numbers for h and m etc
        # TODO: remove this, hard-coded for now...
        hm_layer = 4
        vm_layer = hm_layer + 1

        pitch_hm = self.template.grid.get_track_pitch(hm_layer, unit_mode=True)
        pitch_vm = self.template.grid.get_track_pitch(vm_layer, unit_mode=True)

        # TODO: extra spacing could be added in the lambda

        inter_column_spacings: List[int]
        if self.placement.inter_column_tracks:
            inter_column_spacings = []
            for track_list in self.placement.inter_column_tracks:
                sp = 0
                for track_width in track_list:
                    sp += track_width * pitch_vm
                inter_column_spacings.append(sp)
        else:
            inter_column_spacings = [0] * (self.placement.num_cols - 1)

        # TODO: extra spacing could be added in the lambda

        inter_row_spacings: List[int]
        if self.placement.inter_row_tracks:
            inter_row_spacings = []
            for track_list in self.placement.inter_row_tracks:
                sp = 0
                for track_width in track_list:
                    sp += track_width * pitch_hm
                inter_row_spacings.append(sp)
        else:
            inter_row_spacings = [0] * (self.placement.num_rows - 1)

        x = 0
        y = 0
        cell_height = 0

        placed_instances: List[bag.layout.core.Instance] = []

        # grid origin is bottom-left (so we place rows in reversed order)
        it = np.nditer(inst_matrix, flags=['multi_index', 'refs_ok'], op_flags=['readonly'])
        for inst_matrix_cell in it:
            row, col = it.multi_index

            cell_width = max_widths_per_col[col]

            if col == 0:
                x = 0
                if row >= 1:
                    y += cell_height + inter_row_spacings[row-1]  # read before cell_height is updated!

            cell_height = max_heights_per_row[row]

            if inst_matrix_cell:  # np.array containing InstanceToBePlaced
                inst: InstanceToBePlaced = inst_matrix_cell.item()

                inst_bbox: bag.layout.objects.BBox = inst.master.bound_box.transform(orient=inst.orientation)
                cell_bbox = bag.layout.objects.BBox(left=x, bottom=y, right=x+cell_width, top=y+cell_height,
                                                    resolution=inst_bbox.resolution, unit_mode=True)

                alignment: Alignment = alignment_matrix[row, col]
                placed_inst_bbox = alignment.align_within(inner_bbox=inst_bbox, outer_bbox=cell_bbox)

                ### normalized_inst_bbox = inst_bbox.move_by(dx=-inst_bbox.left, dy=-inst_bbox.bottom)  # bottom left (0, 0)
                # NOTE: if we want to center blocks, we would need to move the location accordingly

                # NOTE: add_instance will use the anchor, we have to consider the transform (rot, etc)
                loc = (placed_inst_bbox.left_unit - inst_bbox.left_unit,
                       placed_inst_bbox.bottom_unit - inst_bbox.bottom_unit)

                print(f"Placing instance for <{it.multi_index}>: {inst.name}, x={x}, y={y}, "
                      f"cell_bbox={cell_bbox} (w={cell_bbox.width}, h={cell_bbox.height}), "
                      f"placed inst bbox={placed_inst_bbox} (w={placed_inst_bbox.width}, h={placed_inst_bbox.height}))")

                placed_inst = self.template.add_instance(master=inst.master,
                                                         inst_name=inst.name,
                                                         loc=loc,
                                                         orient=inst.orientation,
                                                         unit_mode=True)
                placed_instances.append(placed_inst)

            x += cell_width
            if col < len(inter_column_spacings):
                x += inter_column_spacings[col]

        print(".")
        # max_height = list()
        # max_width = list()
        # for (i, row) in self.placement.instance_matrix:
        #     for (j, inst) in row:
        #         print(".")

        self._placed_instances = PlacedInstances(template=self.template,
                                                 instances=placed_instances)

