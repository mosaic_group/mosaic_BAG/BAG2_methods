#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from collections import defaultdict
from dataclasses import dataclass
from enum import Enum
import re
from typing import *
import unittest

import bag.layout.template
import bag.layout.objects
import bag.layout.routing

from .pin_port_access import PinPortAccessor, PinName
from .placement import PlacedInstances


class Location(str, Enum):
    UPPER = 'upper'
    LOWER = 'lower'
    MIDDLE = 'middle'


class LayerChoice:
    BOTTOM = 'bottom'
    TOP = 'top'


# redeclared here because we want more compact routing definitions
UPPER = Location.UPPER
LOWER = Location.LOWER
MIDDLE = Location.MIDDLE


# types
NetName = str


@dataclass(eq=True, frozen=True)
class TrackIntersectionPoint:
    pin_name: PinName
    location: Location
    track_offset: Optional[int] = None


@dataclass(eq=True, frozen=True)
class TrackConnection:
    net_name: NetName
    wire_list: List[Wire]
    track_intersection_point: TrackIntersectionPoint

    def __hash__(self) -> int:
        return hash((self.net_name,
                    tuple(self.wire_list),
                    self.track_intersection_point))


@dataclass(eq=True, frozen=True)
class WirePin:
    connection: TrackConnection
    only_layers: LayerChoice

    def __hash__(self) -> int:
        return hash((self.connection,
                    self.only_layers))


Wire = Union[PinName, WirePin]


@dataclass(eq=True, frozen=True)
class ManualRouting:
    placed_instances: PlacedInstances
    connections_to_tracks: List[TrackConnection]
    net_pins: Dict[NetName, List[Wire]]
    reexported_ports: List[Tuple[NetName, PinName]]


class ManualRouter:
    def __init__(self,
                 template: bag.layout.template.TemplateBase,
                 routing: ManualRouting,
                 show_pins: bool):
        self.template = template
        self.routing = routing
        self.show_pins = show_pins

        self._track_connection_wires: Dict[TrackConnection, Tuple[Optional[WireArray], List[WireArray]]] = {}

    def route(self):
        instances = self.routing.placed_instances

        for conn in self.routing.connections_to_tracks:
            # 1) wiring and vias
            pin_ports, track_connections = self._separate_wires(conn.wire_list)

            warrs = [instances.get_port_pin_wires(p.canonical_fq_str) for p in pin_ports]
            track_warrs = [self._track_connection_wires[t][0] for t in track_connections]
            warrs.extend(track_warrs)

            layers = set([w.layer_id for w in warrs])
            assert len(layers) == 1
            wire_layer = list(layers)[0]
            track_layer_id = wire_layer + 1
            tid = self._get_track_id(track_layer_id=track_layer_id,
                                     track_intersection_point=conn.track_intersection_point)

            # If return_wires is True, returns a Tuple[Optional[WireArray], List[WireArray]].
            track_wire, extended_wires = self.template.connect_to_tracks(warrs, tid, return_wires=True)
            self._track_connection_wires[conn] = (track_wire, extended_wires)
            
        # 2) add pins
        for net_name, wire_list in self.routing.net_pins.items():
            pin_ports, wire_pins = self._separate_wires(wire_list)

            warrs = [instances.get_port_pin_wires(p.canonical_fq_str) for p in pin_ports].copy()
            for t in track_connections:
                track_warr, extended_wires = self._track_connection_wires[t]
                warrs_by_layer: Dict[int, List[WireArray]] = defaultdict(list)
                if track_warr:
                    warrs.append(track_warr)
                # NOTE: do not include wires extensions
                # warrs.extend(extended_wires)

            self.template.add_pin(net_name=net_name, wire_arr_list=warrs, show=self.show_pins)

        # 3) Reexport ports
        for net_name, fq_pin_name in self.routing.reexported_ports:
            pin_port = PinPortAccessor.parse_string(fq_pin_name)
            inst = self.routing.placed_instances.get_instance_by_name(pin_port.instance_name)
            port = inst.get_port(pin_port.pin_name)
            self.template.reexport(port, net_name=net_name, show=self.show_pins)

    def _get_track_id(self,
                      track_layer_id: int,
                      track_intersection_point: TrackIntersectionPoint) -> bag.layout.routing.TrackID:
        warr = self.routing.placed_instances.get_port_pin_wires(track_intersection_point.pin_name)

        coord: int
        match track_intersection_point.location:
            case Location.UPPER:
                coord = warr.upper
            case Location.LOWER:
                coord = warr.lower
            case Location.MIDDLE:
                coord = warr.middle
        track = self.template.grid.coord_to_nearest_track(track_layer_id, coord=coord)

        # TODO: top layer must be configurable!
        top_layer = track_layer_id + 1

        top_width = self.template.grid.get_track_width(layer_id=top_layer, width_ntr=1, unit_mode=True)
        track_width = self.template.grid.get_min_track_width(track_layer_id, top_w=top_width, unit_mode=True)
        if track_intersection_point.track_offset:
            track += track_intersection_point.track_offset * track_width
        tid = bag.layout.routing.TrackID(layer_id=track_layer_id, track_idx=track, width=track_width)
        return tid

    @staticmethod
    def _separate_wires(wires: List[Wire]) -> Tuple[List[PinPortAccessor], List[TrackConnection]]:
        port_pins = [PinPortAccessor.parse_string(w) for w in wires if isinstance(w, PinName)]
        track_conns = [w for w in wires if isinstance(w, TrackConnection)]
        return port_pins, track_conns

    def _pin_port_names(self) -> Set[str]:
        names: Set[str] = set()
        for conn in self.routing.connections_to_tracks:
            ports, track_wires = self._separate_wires(conn.wire_list)
            for p in ports:
                names.add(p.canonical_fq_str)
            p = PinPortAccessor.parse_string(conn.track_intersection_point.pin_name)
            names.add(p.canonical_fq_str)
        for _, wire_list in self.routing.net_pins.items():
            ports, wire_pins = self._separate_wires(wire_list)
            for p in ports:
                names.add(p.canonical_fq_str)
        for _, pin_name in self.routing.reexported_ports:
            names.add(pin_name)
        return names
