#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from enum import Enum
import re
from typing import *
import unittest

import bag.layout.template
import bag.layout.objects
import bag.layout.routing


PinName = str    # Pin name, e.g. VDD
FQPinName = str  # Fully qualified pin name, e.g. inv1.VDD


@dataclass(eq=True, frozen=True)
class PinPortAccessor:
    instance_name: str
    pin_name: PinName
    port_index: int

    @classmethod
    def parse_string(cls, s: FQPinName) -> PinPortAccessor:
        id_pattern = r"([A-Za-z_][A-Za-z0-9_]*)"
        number_pattern = r"\[([0-9]+)\]"
        pattern = "^" + id_pattern + r"\." + id_pattern + r"(?:" + number_pattern + r")?$"

        match = re.search(pattern, s)

        inst, pin, port = match.groups()
        if not port:  # e.g. 'inv1.VDD', and not 'inv1.VDD[1]'
            port = '0'

        accessor = PinPortAccessor(instance_name=inst,
                                   pin_name=pin,
                                   port_index=int(port))
        return accessor

    @property
    def canonical_fq_str(self) -> FQPinName:
        descr = f"{self.instance_name}.{self.pin_name}"
        if self.port_index >= 1:
            descr += f"[{self.port_index}]"
        return descr

    def __str__(self) -> str:
        return self.canonical_fq_str


# --------------------------------------- unit tests ----------------------------------------


class TestPinPortAccessor(unittest.TestCase):
    def test_parse_string__no_port(self):
        self.assertEqual(PinPortAccessor(instance_name='inv1',
                                         pin_name='in',
                                         port_index=0),
                         PinPortAccessor.parse_string('inv1.in'))

    def test_parse_string__with_port(self):
        self.assertEqual(PinPortAccessor(instance_name='inv1',
                                         pin_name='in',
                                         port_index=1),
                         PinPortAccessor.parse_string('inv1.in[1]'))

    def test_parse_string__with_underscore(self):
        self.assertEqual(PinPortAccessor(instance_name='PS_Res',
                                         pin_name='VDD',
                                         port_index=1),
                         PinPortAccessor.parse_string('PS_Res.VDD[1]'))

    def test_canonical_fq_string__no_port(self):
        self.assertEqual('inv1.in',
                         PinPortAccessor(instance_name='inv1',
                                         pin_name='in',
                                         port_index=0).canonical_fq_str)

    def test_canonical_fq_string__with_port(self):
        self.assertEqual('inv1.in[1]',
                         PinPortAccessor(instance_name='inv1',
                                         pin_name='in',
                                         port_index=1).canonical_fq_str)

    def test_canonical_fq_string__with_underscore(self):
        self.assertEqual('PS_Res.VDD[1]',
                         PinPortAccessor(instance_name='PS_Res',
                                         pin_name='VDD',
                                         port_index=1).canonical_fq_str)


if __name__ == '__main__':
    unittest.main()
