#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from enum import Enum
from typing import *

import bag.layout.template
import bag.layout.objects
from sal.layout.pin_port_access import PinPortAccessor, PinName, FQPinName


class PlacedInstances:
    def __init__(self,
                 template: bag.layout.template.TemplateBase,
                 instances: List[bag.layout.objects.Instance]):
        self.template = template
        self.instances = instances

        self._inst_map: Dict[PinName, bag.layout.objects.Instance] = {}
        self._pin_port_wire_map: Dict[PinName, bag.layout.routing.WireArray] = {}
        self._prepare_maps()

    def _prepare_maps(self):
        self._inst_map = {inst.content.name: inst for inst in self.instances}

        # pin_port_names = self._pin_port_names()
        # ports = [_PinPortAccessor.parse_string(n) for n in pin_port_names]
        # self._port_map = {p.canonical_fq_str: p for p in ports}
        #
        # for p in ports:
        #     inst = self._inst_map[p.instance_name]
        #     warr = inst.get_all_port_pins(p.pin_name)[p.port_index]
        #     fq_pin = p.canonical_fq_str
        #     self._pin_port_instance_map[fq_pin] = inst
        #     self._pin_port_wire_map[fq_pin] = warr

    def get_instance_by_name(self, name: str) -> bag.layout.objects.Instance:
        return self._inst_map[name]

    def get_port_pin_wires(self, pin_name: PinName) -> bag.layout.objects.WireArray:
        port = PinPortAccessor.parse_string(pin_name)
        fq_pin_name = port.canonical_fq_str
        warr = self._pin_port_wire_map.get(fq_pin_name, None)
        if not warr:
            inst = self._inst_map[port.instance_name]
            warr = inst.get_all_port_pins(port.pin_name)[port.port_index]
            self._pin_port_wire_map[fq_pin_name] = warr
        return warr

    def get_port(self, pin_name: PinName) -> bag.layout.routing.Port:
        pin_port = PinPortAccessor.parse_string(pin_name)
        inst = self._inst_map[pin_port.instance_name]
        port = inst.get_port(pin_port.pin_name)
        return port

    def set_size_from_bound_box(self,
                                top_layer_id: int):
        """Set Bounding Box and Size of the top-block"""

        merged_box = None
        for inst in self.instances:
            if merged_box:
                merged_box = merged_box.merge(inst.bound_box)
            else:
                merged_box = inst.bound_box

        self.template.set_size_from_bound_box(top_layer_id=top_layer_id,
                                              bbox=merged_box,
                                              round_up=True)


@dataclass
class InstanceToBePlaced:
    name: str
    master: bag.layout.template.TemplateType
    orientation: str = 'R0'
