#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from enum import Enum
from typing import *

import bag.layout.template
import bag.layout.objects
import bag.layout.util

from sal.layout.placement import PlacedInstances
from sal.layout.pin_port_access import FQPinName
from sal.layout.manual_router import NetName


@dataclass
class PowerMesh:
    bottom_layer_id: int
    top_layer_id: int   # must be adjacent to bottom_layer_id!
    vdd_net_name: str
    vss_net_name: str
    inclusion_bboxes: bag.layout.util.BBoxCollection
    exclusion_bboxes: Optional[bag.layout.util.BBoxCollection]
    width_ntr: int = 1
    extra_space_ntr: int = 0


@dataclass
class PowerMeshRoutingResult:
    template: bag.layout.template.TemplateBase
    power_mesh: PowerMesh

    vdd_bottom_warrs: List[bag.layout.objects.WireArray]
    vdd_top_warrs: List[bag.layout.objects.WireArray]
    vss_bottom_warrs: List[bag.layout.objects.WireArray]
    vss_top_warrs: List[bag.layout.objects.WireArray]

    def connect_instances(self,
                          instances: PlacedInstances,
                          vdd_port_connections: List[FQPinName],
                          vss_port_connections: List[FQPinName]):
        vdd_port_warrs = [instances.get_port_pin_wires(p) for p in vdd_port_connections]
        vss_port_warrs = [instances.get_port_pin_wires(p) for p in vss_port_connections]

        for mesh_warrs in (self.vdd_bottom_warrs, self.vdd_top_warrs):
            self.template.draw_vias_on_intersections(bot_warr_list=vdd_port_warrs, top_warr_list=mesh_warrs)

        for mesh_warrs in (self.vss_bottom_warrs, self.vss_top_warrs):
            self.template.draw_vias_on_intersections(bot_warr_list=vss_port_warrs, top_warr_list=mesh_warrs)


class PowerMeshRouter:
    def __init__(self,
                 template: bag.layout.template.TemplateBase,
                 power_mesh: PowerMesh):
        self.template = template
        self.power_mesh = power_mesh

    def route(self) -> PowerMeshRoutingResult:
        from bag.layout.util import BBox, BBoxArray
        from bag.layout.objects import WireArray
        from bag.layout.routing import TrackID

        pm = self.power_mesh
        grid = self.template.grid

        # incl_bbox = pm.inclusion_bboxes.get_bounding_box()
        #
        # # supply_layer = grid.get_layer_name(layer_id=pm.bottom_layer_id, tr_idx=1)
        # supply_layer_dir = grid.get_direction(layer_id=pm.bottom_layer_id)
        #
        # vdd_ny = 10
        # # vdd_cols = BBoxArray(vdd_bbox, nx=vdd_nx, ny=1, spx=0, spy=0, unit_mode=True)
        #
        # lower: int
        # upper: int
        # if supply_layer_dir == 'x':
        #     lower = incl_bbox.left_unit
        #     upper = incl_bbox.right_unit
        # #elif supply_layer_dir == 'y':
        # #    lower = incl_bbox.
        #
        # # NOTE: power mesh can be generated using TemplateBase.do_power_fill()
        # #
        # # sp = max(sp, int(self.grid.get_space(layer_id, width, unit_mode=True)))
        # # sp_le = max(sp_le, int(self.grid.get_line_end_space(layer_id, width, unit_mode=True)))
        #     # self.template.connect_with_via_stack()
        #     # self.template.add_via_on_grid(bot_layer_id=pm.bottom_layer_id,
        #     #                               bot_track=0,
        #     #                               top_track=0)  # NOTE: bot_width/top_width not needed

        vdd_bottom_warrs = []
        vdd_top_warrs = []
        vss_bottom_warrs = []
        vss_top_warrs = []

        for bbox in pm.inclusion_bboxes.as_bbox_array():
            bottom_vdd, bottom_vss = self.template.do_power_fill(layer_id=pm.bottom_layer_id,
                                                                 space=0,
                                                                 space_le=0,
                                                                 bound_box=bbox,
                                                                 fill_width=pm.width_ntr,
                                                                 fill_space=pm.extra_space_ntr,
                                                                 unit_mode=True)

            top_vdd, top_vss = self.template.do_power_fill(layer_id=pm.top_layer_id,
                                                           space=0,
                                                           space_le=0,
                                                           vdd_warrs=bottom_vdd,  # draw vias to adjacent layer warrs
                                                           vss_warrs=bottom_vss,  # draw vias to adjacent layer warrs
                                                           bound_box=bbox,
                                                           fill_width=pm.width_ntr,
                                                           fill_space=pm.extra_space_ntr,
                                                           unit_mode=True)

            vdd_bottom_warrs.extend(bottom_vdd)
            vss_bottom_warrs.extend(bottom_vss)
            vdd_top_warrs.extend(top_vdd)
            vss_top_warrs.extend(top_vss)

        # for bottom_warr, top_warr in [(bottom_vdd, top_vdd), (bottom_vss, top_vss)]:
        #     self.template.draw_vias_on_intersections(bot_warr_list=bottom_warr,
        #                                              top_warr_list=top_warr)

        return PowerMeshRoutingResult(template=self.template,
                                      power_mesh=self.power_mesh,
                                      vdd_bottom_warrs=vdd_bottom_warrs,
                                      vdd_top_warrs=vdd_top_warrs,
                                      vss_bottom_warrs=vss_bottom_warrs,
                                      vss_top_warrs=vss_top_warrs)

