#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
import re
from typing import *
import unittest

from sal.log import info, error, warn


@dataclass
class LogFileAnalysis:
    log_path: Optional[str]
    successful: bool
    lines: [str]
    error_line_numbers: [int]
    warning_line_numbers: [int]

    @property
    def all_issue_line_numbers(self) -> [int]:
        issue_lines = self.error_line_numbers + self.warning_line_numbers
        issue_lines.sort()
        return issue_lines

    def print_issues(self,
                     print_warnings: Bool = True):
        if print_warnings or len(self.error_line_numbers) >= 1:
            info(f"Detected issues in log file: {self.log_path}")
        for line_number in self.all_issue_line_numbers:
            line = self.lines[line_number].strip()
            # NOTE: Some log files (like Calibre) often have lines indicating output
            #       i.e. "\o ERROR (SFE-1768): "/p.../netlist/amp_cs"
            if line.startswith('\\o '):
                line = line[3:]
            msg = f"L{line_number}: {line}"
            if line_number in self.error_line_numbers:
                error(msg)
            elif print_warnings and line_number in self.warning_line_numbers:
                warn(msg)

    def find_error_lines(self, error_pattern: re.Pattern) -> List[Tuple[int, str]]:
        matches: List[Tuple[int, str]] = []
        for n in self.error_line_numbers:
            line = self.lines[n]
            if error_pattern.match(line):
                matches.append((n, line))
        return matches


class LogAnalyzer:
    def __init__(self,
                 error_pattern: re.Pattern,
                 warn_pattern: re.Pattern):
        self.error_pattern = error_pattern
        self.warn_pattern = warn_pattern

    @classmethod
    def default(cls) -> LogAnalyzer:
        return LogAnalyzer(
            error_pattern=re.compile(r"^.*ERROR(:.*|\s\(.*)$"),
            warn_pattern=re.compile(r"^.*WARNING(:.*|\s\(.*)$"),
        )

    def analyze_path(self, log_path: str) -> LogFileAnalysis:
        with open(log_path, "r") as f:
            lines = f.readlines()
            analysis = self.analyze_lines(lines=lines)
            analysis.log_path = log_path
            return analysis

    def analyze_lines(self, lines: [str]) -> LogFileAnalysis:
        error_line_numbers = []
        warning_line_numbers = []
        for idx, line in enumerate(lines):
            if self.error_pattern.match(line):
                error_line_numbers.append(idx)
            if self.warn_pattern.match(line):
                warning_line_numbers.append(idx)
        successful = len(error_line_numbers) == 0
        return LogFileAnalysis(successful=successful,
                               log_path=None,
                               error_line_numbers=error_line_numbers,
                               warning_line_numbers=warning_line_numbers,
                               lines=lines)


# -------------------------------------- Unit Tests -------------------------------------

class TestLogAnalyzer(unittest.TestCase):
    def setUp(self):
        pass

    def test_no_issue(self):
        analysis = LogAnalyzer.default().analyze_lines(lines=[])
        self.assertTrue(analysis.successful)
        self.assertListEqual(analysis.error_line_numbers, [])
        self.assertListEqual(analysis.warning_line_numbers, [])
        self.assertListEqual(analysis.lines, [])
        analysis.print_issues()

    def test_1error(self):
        lines = [
            'ERROR: problem occurred'
        ]
        analysis = LogAnalyzer.default().analyze_lines(lines=lines)
        self.assertFalse(analysis.successful)
        self.assertListEqual(analysis.error_line_numbers, [0])
        self.assertListEqual(analysis.warning_line_numbers, [])
        self.assertListEqual(analysis.lines, lines)
        analysis.print_issues()

    def test_1warning(self):
        lines = [
            'WARNING: potential issue occurred'
        ]
        analysis = LogAnalyzer.default().analyze_lines(lines=lines)
        self.assertTrue(analysis.successful)
        self.assertListEqual(analysis.error_line_numbers, [])
        self.assertListEqual(analysis.warning_line_numbers, [0])
        self.assertListEqual(analysis.lines, lines)
        analysis.print_issues()

    def test_calibre_issue(self):
        lines = [
            "\\o ERROR (SFE-1768): / p... / netlist / amp_cs"
        ]
        analysis = LogAnalyzer.default().analyze_lines(lines=lines)
        self.assertFalse(analysis.successful)
        self.assertListEqual(analysis.error_line_numbers, [0])
        self.assertListEqual(analysis.warning_line_numbers, [])
        self.assertListEqual(analysis.lines, lines)
        analysis.print_issues()


if __name__ == '__main__':
    unittest.main()
