#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from enum import Enum
from typing import *

import bag.layout.template
import bag.layout.objects
import bag.layout.routing

from .placement import PlacedInstances


@dataclass
class Pin:
    name: str
    instance: str  # TODO: change to PlacedInstance later using Python >=3.10 (weakref support)
    port_pin: WireArray
#    access_candidates: [PinAccessCandidate]  # NOTE: constructed later in autorouter

    def middle(self, grid: RoutingGrid) -> Coord:
        return Coord(grid=grid,
                     port_pin=self.port_pin,
                     coord_1D=self.port_pin.middle)


@dataclass
class Net:
    name: str
    pins: [Pin]
#    pin_connections: List[PinConnection]  # NOTE: constructed later in autorouter

    @property
    def min_coord(self) -> float:
        coords = map(lambda p: p.port_pin.lower, self.pins)
        return min(coords)


@dataclass
class Netlist:
    net_by_name: Dict[str, Net]
#    net_order: Optional[List[Net]]  # left-most net  # NOTE: constructed later in autorouter

    @classmethod
    def parse_netlist_dict(cls,
                           bag_netlist: Dict[str, Any],
                           placed_instances: PlacedInstances) -> Netlist:
        net_by_name: Dict[str, Net] = {}
        for inst_name, inst_dict in bag_netlist['instances'].items():
            for pin_name, pin_dict in inst_dict['instpins'].items():
                net_name = pin_dict['net_name']

                inst = placed_instance_by_name[inst_name]
                port_pin = inst.get_all_port_pins(pin_name)[0]
                pin = Pin(name=pin_name,
                          instance=inst_name,  # TODO: change to inst later Python >=3.10 (weakref support)
                          port_pin=port_pin)

                net = net_by_name.setdefault(net_name, Net(name=net_name, pins=[])
                net.pins.append(pin)
        return Netlist(net_by_name=net_by_name)

