#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class

from abc import ABC, abstractmethod, abstractclassmethod
import copy
from enum import Enum
import os
import sys
from typing import *
import unittest
import yaml

from sal.env import Env


# -------------------------------------- Enums -------------------------------------

class DumpFormat(Enum):
    YAML = 'yaml'
    JSON = 'json'


# -------------------------------------- Classes -------------------------------------

class ParamsBase(ABC):
    @classmethod
    def parse_dump(cls,
                   path: str,
                   dump_format: Optional[DumpFormat]) -> type[GenericParamsBase]:
        if dump_format is None:
            _, suffix = os.path.splitext(path)
            dump_format = DumpFormat[suffix]
        if dump_format == DumpFormat.YAML:
            with open(path, 'r') as f:
                obj = yaml.load(f, Loader=yaml.Loader)
                return obj
        else:
            raise Exception(f"Dump format {dump_format} is not yet implemented")

    def dump(self,
             dump_format: DumpFormat,
             path: str = None):
        if dump_format == DumpFormat.YAML:
            if path is None:
                yaml.dump(self, sys.stdout)
            else:
                with open(path, 'w') as f:
                    yaml.dump(self, f)
        else:
            raise Exception(f"Dump format {dump_format} is not yet implemented")

    def copy(self) -> ParamsBase:
        return copy.deepcopy(self)

    def get_immutable_key(self):
        """
        Required for bag.util.cache DesignMaster to_immutable_id()
        """
        from bag.util.cache import DesignMaster
        return DesignMaster.to_immutable_id(self.__dict__)

    # NOTE: if we'd move this to LayoutParamsBase, we'd break existing generators
    @classmethod
    def defaults(cls, min_lch):
        if Env.is_finfet():
            return cls.finfet_defaults(min_lch)
        else:
            return cls.planar_defaults(min_lch)


class LayoutParamsBase(ParamsBase):
    @classmethod
    @abstractmethod
    def finfet_defaults(cls, min_lch):
        raise Exception("subclasses must implement this class method")

    @classmethod
    @abstractmethod
    def planar_defaults(cls, min_lch):
        raise Exception("subclasses must implement this class method")

    # @classmethod
    # def defaults(cls, min_lch):
    #     if Env.is_finfet():
    #         return cls.finfet_defaults(min_lch)
    #     else:
    #         return cls.planar_defaults(min_lch)


class MeasurementParamsBase(ParamsBase):
    pass


class GeneratorParamsBase(ParamsBase):
#    @abstractproperty / dataclass
#    def layout_parameters(self) -> LayoutParamsBase:
#        raise Exception("subclasses must implement this method")

#    @abstractproperty / dataclass
#    def measurement_parameters(self) -> List[MeasurementParamsBase]:
#        raise Exception("subclasses must implement this method")
    pass


# -------------------------------------- Unit Tests -------------------------------------

# TODO: no tests yet
class TestParamsBase(unittest.TestCase):
    def setUp(self):
        pass

    def test_dump(self):
        pass


if __name__ == '__main__':
    unittest.main()
