#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import Enum
import os
import sys
from typing import Dict, List, Optional
from urllib.parse import urlparse

from sal.log import *
from BAG2_methods.flow_variant import BAGFlowVariant


@dataclass
class PackageKind:
    """
    name: str
        Name/noun for logging purposes, like "Generator"

    suffix: str
        Suffix of the package, like "_gen"

    package_class: Type
        Subclass of PackageBase for this package
    """

    name: str
    suffix: str
    package_class: Type[PackageBase]

    def __hash__(self):
        return self.suffix.__hash__()


@dataclass
class PackageBase(ABC):
    name: str
    url: Optional[str]
    path: Optional[str]
    source: GitSource

    @classmethod
    @abstractmethod
    def kind(cls) -> PackageKind:
        raise Exception("subclasses must implement this class method")

    @classmethod
    def __validate_url(cls,
                       url: str) -> str:
        u = urlparse(url)
        basename = os.path.basename(u.path)
        if basename.endswith(self.kind().suffix):
            pass
        elif basename.endswith(f"{self.kind().suffix}.git"):
            basename = basename[: -len('.git')]
        else:
            raise Exception(f"Expected {self.kind().name} URL to end with {self.kind().suffix}, but obtained: {url}")
        return basename

    @property
    def cell_name(self) -> str:
        return self.name[:-len(self.kind().suffix)]

    @property
    def scripts_basename(self) -> str:
        return self.name

    @property
    def templates_libname(self) -> str:
        return self.cell_name + "_templates"

    @property
    def templates_basename_virtuoso(self) -> str:
        return self.cell_name + "_templates"

    @property
    def templates_basename_xschem(self) -> str:
        return self.cell_name + "_templates_xschem"

    @property
    def scripts_path(self) -> str:
        return os.path.join(self.path, self.scripts_basename)

    @property
    def template_cell_path_virtuoso(self) -> str:
        return os.path.join(self.path, self.templates_basename_virtuoso, self.cell_name)

    @property
    def template_cell_path_xschem(self) -> str:
        return os.path.join(self.path, self.templates_basename_xschem, self.cell_name)

    @property
    def template_cell_path(self) -> str:
        path: str

        variant = BAGFlowVariant.from_bag_config()
        match variant:
            case BAGFlowVariant.BAG2_OPEN_SOURCE:
                path = self.template_cell_path_xschem
            case BAGFlowVariant.BAG2_VIRTUOSO:
                path = self.template_cell_path_virtuoso
            case _:
                raise ValueError(f"unsupported flow variant {variant}")

        return path

    def validate_path(self):
        if self.path is None:
            raise Exception(f"{self.kind().name} {self.name} is not installed")
        if not (os.path.exists(self.path) and os.path.isdir(self.path)):
            raise Exception(f"{self.kind().name} {self.name} directory does not exist at {self.path}")
        if not os.path.basename(self.path).endswith(self.kind().suffix):
            raise Exception(f"{self.kind().name} {self.name} directory basename must end with '{self.kind().suffix}'")
