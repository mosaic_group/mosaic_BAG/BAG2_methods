#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from enum import Enum
from typing import *
import unittest
from bag.layout.core import BBox, Instance
from bag.layout.routing import TrackID, WireArray, RoutingGrid
from abs_templates_ec.analog_core import AnalogBase

from .via import ViaDirection


# -------------------------------------- Enums -------------------------------------

class EdgeMode(int, Enum):
    """
    If <0, draw the pin on the lower end of the WireArray.  If >0, draw the pin
    on the upper end.  If 0, draw the pin on the entire WireArray.
    """
    LOWER_END = -1,
    ENTIRE_WARR = 0,
    UPPER_END = 1


# -------------------------------------- Classes -------------------------------------

class RoutingGridHelper:
    """
    Initialize the routing grid helper.

    Parameters
    ----------
    template_base: TemplateBase
        template base instance
    unit_mode : bool
        True if the given coordinate is in resolution units.
    layer_id : int
        the layer number.
    track_width : int
        width of one track in number of tracks.
    """
    def __init__(self,
                 template_base: TemplateBase,
                 unit_mode: bool,
                 layer_id: int,
                 track_width: int,
                 ):
        self.template_base = template_base
        self.unit_mode = unit_mode
        self.layer_id = layer_id
        self.track_width = track_width

    @property
    def grid(self) -> RoutingGrid:
        return self.template_base.grid

    def track_by_col(self,
                     col_idx: int,
                     offset: int = 0,
                     half_track: bool = False,
                     ) -> TrackID:
        """
        Returns the track for a given column index

        Parameters
        ----------
        col_idx : int
            the transistor index.  0 is left-most transistor.
        offset: int
            offset to the track index
        half_track : bool
            if True, allow half integer track numbers.

        Returns
        -------
        track : TrackID
            the track
        """
        return TrackID(
            layer_id=self.layer_id,
            track_idx=self.grid.coord_to_nearest_track(
                layer_id=self.layer_id,
                coord=self.template_base.layout_info.col_to_coord(
                    col_idx=col_idx,
                    unit_mode=self.unit_mode
                ),
                half_track=half_track,
                mode=1,
                unit_mode=self.unit_mode
            ) + offset,
            width=self.track_width
        )

    def connect_level_up(self,
                         warr_id: WireArray,
                         via_dir: ViaDirection,
                         step: float,
                         min_width: int = 1) -> WireArray:
        """
        This method takes a wire-id, adds via (according via_dir on top, bottom or in the middle of wire)
        and connect given wire to the higher adjacent metal layer.

        Parameters
        ----------
        warr_id : WireArray
            The WireArray we would like to connect to higher metal layer
        via_dir : ViaDirection
            Direction of the via
        step: float
            Number of steps you need to shift from middle of WireArray
        min_width: int
            Specifies width of metal layer track (in number of tracks) that you would want to connect.
            Useful if default routing width of metal layer is too small for creating vias to top/bottom metal layers.

        Returns
        -------
        warr_id : WireArray
            wire-id which is on the higher metal
        """
        lay_id = warr_id.layer_id
        mid_coord = self.grid.track_to_coord(lay_id, warr_id.track_id.base_index)
        lay_id += 1
        min_len = self.grid.get_min_length(lay_id, 1)

        next_tr = self.grid.find_next_track(warr_id.layer_id + 1,
                                            warr_id.middle,
                                            half_track=True,
                                            mode=via_dir)
        mid_tr = next_tr + via_dir * step
        warr_id1 = self.template_base.connect_to_tracks(warr_id,
                                                        TrackID(lay_id, mid_tr, width=min_width),
                                                        track_lower=mid_coord - min_len / 2,    
                                                        track_upper=mid_coord + min_len / 2)
        return warr_id1

    def connect_level_down(self,
                           warr_id: WireArray,
                           via_dir: ViaDirection,
                           step: float,
                           min_width: int = 1) -> WireArray:
        """
        This method takes a wire-id, adds via (according via_dir on top, bottom or in the middle of wire) and connect
        given wire to the higher adjacent metal layer.

        Parameters
        ----------
        warr_id : WireArray
            The WireArray we would like to connect to higher metal layer
        via_dir : ViaDirection
            only TOP and BOTTOM are supported
        step: float
            Number of steps you need to shift from middle of WireArray
        min_width: int
            Specifies width of metal layer track (in number of tracks) that you would want to connect.
            Useful if default routing width of metal layer is too small for creating vias to top/bottom metal layers.

        Returns
        -------
        warr_id : WireArray
        wire-id which is on the lower metal

        """
        lay_id = warr_id.layer_id
        mid_coord = self.grid.track_to_coord(lay_id, warr_id.track_id.base_index)
        lay_id -= 1
        min_len = self.grid.get_min_length(lay_id, 1)

        next_tr = self.grid.find_next_track(layer_id=warr_id.layer_id - 1,
                                            coord=warr_id.middle,
                                            half_track=False,
                                            mode=via_dir)
        mid_tr = next_tr + via_dir * step
        warr_id1 = self.template_base.connect_to_tracks(wire_arr_list=warr_id,
                                                        track_id=TrackID(lay_id, mid_tr, width=min_width),
                                                        track_lower=mid_coord - min_len / 2,
                                                        track_upper=mid_coord + min_len / 2)
        return warr_id1

    def connect_warr_to_box(self,
                            warr_id: WireArray,
                            bbox: BBox,
                            pitch,
                            layer_name: str):
        res = self.grid.resolution
        box = warr_id.get_bbox_array(self.grid)

        num = (box.right_unit - box.left_unit) // pitch
        for idx in range(num // 2):
            if bbox.bottom_unit < box.bottom_unit:
                box1 = BBox(box.left_unit + 2 * idx * pitch,
                            bbox.bottom_unit,
                            box.left_unit + (2 * idx + 1) * pitch,
                            box.bottom_unit + pitch // 2,
                            res,
                            unit_mode=True)
                self.template_base.add_rect(layer_name, box1)
                self.template_base.add_rect(layer_name,
                                            BBox(box.left_unit,
                                                 box.bottom_unit,
                                                 box.right_unit,
                                                 box.top_unit,
                                                 res,
                                                 unit_mode=True),
                                            unit_mode=True)
            else:
                box1 = BBox(box.left_unit + 2 * idx * pitch,
                            box.top_unit - pitch // 2,
                            box.left_unit + (2 * idx + 1) * pitch,
                            bbox.top_unit,
                            res,
                            unit_mode=True)
                self.template_base.add_rect(layer_name, box1)
                self.template_base.add_rect(layer_name,
                                            BBox(box.left_unit,
                                                 box.bottom_unit,
                                                 box.right_unit,
                                                 box.top_unit,
                                                 res,
                                                 unit_mode=True),
                                            unit_mode=True)

    def extend_pin(self,
                   warr_id: WireArray,
                   via_dir: ViaDirection,
                   wire_layer_id: int):
        """
        This method takes a wire-id and extends it to the direction defined by via_dir

        Parameters
        ----------
        warr_id : WireArray
            The WireArray we would like to connect to higher metal layer
        via_dir : ViaDirection
            only TOP/BOTTOM are supported
        wire_layer_id : int
            layer_id used for wires

        Returns
        -------
        """

        pitch_vm = self.grid.get_track_pitch(layer_id=warr_id.layer_id,
                                             unit_mode=True)
        if via_dir == ViaDirection.TOP:
            self.template_base.add_wires(layer_id=wire_layer_id,
                                         track_idx=warr_id.track_id.base_index,
                                         lower=warr_id.lower_unit,
                                         upper=warr_id.upper_unit + pitch_vm // 2,
                                         width=warr_id.track_id.width,
                                         unit_mode=True)
        elif via_dir == ViaDirection.BOTTOM:
            self.template_base.add_wires(layer_id=wire_layer_id,
                                         track_idx=warr_id.track_id.base_index,
                                         lower=warr_id.lower_unit - pitch_vm // 2,
                                         upper=warr_id.upper_unit,
                                         width=warr_id.track_id.width,
                                         unit_mode=True)
        else:
            raise ValueError(f"Via direction is not supported: {via_dir}")

    def extend_edge_pin(self,
                        layer_id: int,
                        pin_name: str,
                        inst: Instance,
                        via_dir: ViaDirection,
                        edge_mode: EdgeMode,
                        bottom: float,
                        top: float,
                        pin_type: str,
                        new_name: Optional[str] = None,
                        step: int = 0,
                        via_dir1: ViaDirection = ViaDirection.TOP,
                        step1: int = 0,
                        min_width: int = 0):  # mode 1 right, -1 left
        """
        This method takes the input parameters and based on that brings the pins to the edge of instance.
        This is not a general method, it is written for special case when the pins
        are in M4 and we want to bring them to M6,
        and extend them to the edge of entered instance.
        Two level_up method is required and via_dir/via_dir1 and step/step1
        are used for that.

        Parameters
        ----------
        layer_id : int
        pin_name : str
        inst : Instance
        via_dir : ViaDirection
        edge_mode : EdgeMode
            If <0, draw the pin on the lower end of the WireArray.  If >0, draw the pin
            on the upper end.  If 0, draw the pin on the entire WireArray.
        bottom : float
        top : float
        pin_type : str
        new_name : str
        step : int
        via_dir1 : ViaDirection
        step1 : int
        min_width: int
            Can be used in cases where the default routing width of metal layer is
            too small for creating vias to top/bottom metal layers.

        Returns
        -------

        """
        if new_name is None:
            new_name = pin_name

        port_pin = inst.get_all_port_pins(pin_name)[0]
        warr1 = self.connect_level_up(
            warr_id=self.connect_level_up(
                warr_id=port_pin,
                via_dir=via_dir,
                min_width=min_width,
                step=step),
            via_dir=via_dir1,
            step=step1
        )
        warr = self.template_base.add_wires(layer_id=layer_id,
                                            track_idx=warr1.track_id.base_index,
                                            lower=bottom,
                                            upper=top,
                                            unit_mode=True,
                                            width=1)
        self.template_base.add_pin(net_name=new_name,
                                   wire_arr_list=warr,
                                   label=new_name,
                                   show=True,
                                   edge_mode=edge_mode)

    def add_power_ring(self,
                       box: BBox,
                       pitch: Union[float, int],
                       extend_to_top: int = 0,
                       extend_to_bottom: int = 0):
        """
        This method takes an instance and pitch of top metal layer,
        and adds power ring using M1 (GND) and M2 (VDD) around that.

        Parameters
        ----------
        box : BBox
            the entered box.
        pitch : float
            Metal layer pitch
        extend_to_top: int 
            Distance (in terms of pitch), that we would like to extend the top of ring's BBox
        extend_to_bottom: int 
            Distance (in terms of pitch), that we would like to extend the bottom of ring's BBox

        Returns
        -------

        """
        res = self.grid.resolution
        offset = 4*pitch
        width = 3*pitch
        inst_right = box.right_unit + offset
        inst_left = box.left_unit - offset
        inst_top = box.top_unit + 2*offset + extend_to_top*pitch
        inst_bottom = box.bottom_unit - 2*offset - extend_to_bottom*pitch
        GND_layer = self.grid.tech_info.get_layer_name(1)
        VDD_layer = self.grid.tech_info.get_layer_name(2)
        box = BBox(inst_left, inst_bottom,
                   inst_left + width, inst_top,
                   res, unit_mode=True)
        self.template_base.add_rect(GND_layer, box)
        box = BBox(inst_right - width, inst_bottom,
                   inst_right, inst_top,
                   res, unit_mode=True)
        self.template_base.add_rect(GND_layer, box)
        box = BBox(inst_left, inst_bottom,
                   inst_right, inst_bottom + width,
                   res, unit_mode=True)
        self.template_base.add_rect(GND_layer, box)
        box = BBox(inst_left, inst_top - width,
                   inst_right, inst_top,
                   res, unit_mode=True)
        self.template_base.add_rect(GND_layer, box)
        box = BBox(inst_left, inst_bottom,
                   inst_left + width, inst_top,
                   res, unit_mode=True)
        self.template_base.add_rect(VDD_layer, box)
        box = BBox(inst_right - width, inst_bottom,
                   inst_right, inst_top,
                   res, unit_mode=True)
        self.template_base.add_rect(VDD_layer, box)
        box = BBox(inst_left, inst_bottom,
                   inst_right, inst_bottom + width,
                   res, unit_mode=True)
        self.template_base.add_rect(VDD_layer, box)
        box = BBox(inst_left, inst_top - width,
                   inst_right, inst_top,
                   res, unit_mode=True)
        self.template_base.add_rect(VDD_layer, box)

    # TODO: remove this by reusing add_power_ring with different/additional parameters

    def add_box_power_ring(self,
                           box: BBox,
                           pitch: Union[float, int]):
        """
        This method takes a box and pitch of top metal layer, and adds power ring using M1 (GND) and M2 (VDD) around that.

        Parameters
        ----------
        box : BBox
            the entered box.
        pitch : float
            Metal layer pitch

        Returns
        -------

        """
        grid = self.grid
        res = grid.resolution
        inst_right = box.right_unit
        inst_left = box.left_unit
        inst_top = box.top_unit
        inst_bottom = box.bottom_unit
        GND_layer = self.grid.tech_info.get_layer_name(1)
        VDD_layer = self.grid.tech_info.get_layer_name(2)
        box = BBox(inst_left - 15 * pitch, inst_bottom - 20 * pitch,
                   inst_left - 10 * pitch, inst_top + 20 * pitch,
                   res, unit_mode=True)
        self.template_base.add_rect(GND_layer, box)
        box = BBox(inst_right + 10 * pitch, inst_bottom - 20 * pitch,
                   inst_right + 15 * pitch, inst_top + 20 * pitch,
                   res, unit_mode=True)
        self.template_base.add_rect(GND_layer, box)
        box = BBox(inst_left - 15 * pitch, inst_bottom - 20 * pitch,
                   inst_right + 15 * pitch, inst_bottom - 10 * pitch,
                   res, unit_mode=True)
        self.template_base.add_rect(GND_layer, box)
        box = BBox(inst_left - 15 * pitch, inst_top + 10 * pitch,
                   inst_right + 15 * pitch, inst_top + 20 * pitch,
                   res, unit_mode=True)
        self.template_base.add_rect(GND_layer, box)
        box = BBox(inst_left - 15 * pitch, inst_bottom - 20 * pitch,
                   inst_left - 10 * pitch, inst_top + 20 * pitch,
                   res, unit_mode=True)
        self.template_base.add_rect(VDD_layer, box)
        box = BBox(inst_right + 10 * pitch, inst_bottom - 20 * pitch,
                   inst_right + 15 * pitch, inst_top + 20 * pitch,
                   res, unit_mode=True)
        self.template_base.add_rect(VDD_layer, box)
        box = BBox(inst_left - 15 * pitch, inst_bottom - 20 * pitch,
                   inst_right + 15 * pitch, inst_bottom - 10 * pitch,
                   res, unit_mode=True)
        self.template_base.add_rect(VDD_layer, box)
        box = BBox(inst_left - 15 * pitch, inst_top + 10 * pitch,
                   inst_right + 15 * pitch, inst_top + 20 * pitch,
                   res, unit_mode=True)
        self.template_base.add_rect(VDD_layer, box)

    def add_top_power_ring(self,
                           box: BBox,
                           pitch: Union[float, int],
                           top_layer: int):
        """
        This method takes a box and pitch of top metal layer,
        and adds power ring using M1 (GND) and M2 (VDD) around that.
        This method is written for highest level where VDD and VSS lines on top metals with thick tracks are added
        and other power rings are connected to the top VDD and VSS lines
        VDD on ('top_layer'-1)  and VSS on 'top_layer'

        Parameters
        ----------
        box : BBox
            the entered box.
        pitch : float
            Metal layer pitch
        top_layer : int
            Layer id of topmost metal layer in your layout

        Returns
        -------

        """
        res = self.grid.resolution
        offset = 20*pitch
        width = 3*pitch
        inst_right = box.right_unit + offset
        inst_left = box.left_unit - offset
        inst_top = box.top_unit + 2*offset
        inst_bottom = box.bottom_unit - 2*offset
        GND_layer_id = 1
        VDD_layer_id = 2
        GND_layer = self.grid.tech_info.get_layer_name(GND_layer_id)
        VDD_layer = self.grid.tech_info.get_layer_name(VDD_layer_id)

        box = BBox(inst_left, inst_bottom,
                   inst_left + width, inst_top,
                   res, unit_mode=True)
        self.template_base.add_rect(GND_layer, box)
        box = BBox(inst_right - width, inst_bottom,
                   inst_right, inst_top,
                   res, unit_mode=True)
        self.template_base.add_rect(GND_layer, box)
        box = BBox(inst_left, inst_bottom,
                   inst_right, inst_bottom + width,
                   res, unit_mode=True)
        self.template_base.add_rect(GND_layer, box)
        box = BBox(inst_left, inst_top - width,
                   inst_right, inst_top,
                   res, unit_mode=True)
        self.template_base.add_rect(GND_layer, box)
        box = BBox(inst_left + offset//2, inst_bottom,
                   inst_left + offset//2 + width, inst_top,
                   res, unit_mode=True)
        self.template_base.add_rect(VDD_layer, box)
        box = BBox(inst_right - offset//2 - width, inst_bottom,
                   inst_right - offset//2, inst_top,
                   res, unit_mode=True)
        self.template_base.add_rect(VDD_layer, box)
        box = BBox(inst_left + offset//2, inst_bottom,
                   inst_right - offset//2, inst_bottom + width,
                   res, unit_mode=True)
        self.template_base.add_rect(VDD_layer, box)
        box = BBox(inst_left + offset//2, inst_top - width,
                   inst_right - offset//2, inst_top,
                   res, unit_mode=True)
        self.template_base.add_rect(VDD_layer, box)
        num = (inst_right - inst_left) // (32 * pitch)
        for idx in range(num):
            vr_id = self.grid.coord_to_nearest_track(top_layer-1, inst_bottom,
                                                     mode=1, half_track=True, unit_mode=True)
            vr_tid = TrackID(top_layer-1, vr_id, width=10)
            warr = self.template_base.add_wires(top_layer-1, vr_tid.base_index, inst_left + idx * 32 * pitch,
                                                inst_left + (idx + 1) * 32 * pitch,
                                                width=5, unit_mode=True)
            self.template_base.add_wires(top_layer-1, vr_tid.base_index, inst_left + idx * 32 * pitch,
                                         inst_left + (idx + 1) * 32 * pitch,
                                         width=15, unit_mode=True)
            vr_id = self.grid.coord_to_nearest_track(VDD_layer_id, inst_bottom,
                                                     mode=1, half_track=True, unit_mode=True)
            vr_tid = TrackID(VDD_layer_id, vr_id, width=5)
            self.template_base.connect_with_via_stack(warr, vr_tid)
        self.template_base.add_pin('VDD', warr, show=True)
        for idx in range(num):
            vr_id = self.grid.coord_to_nearest_track(top_layer-1, inst_top,
                                                     mode=1, half_track=True, unit_mode=True)
            vr_tid = TrackID(top_layer-1, vr_id, width=10)
            warr = self.template_base.add_wires(top_layer-1, vr_tid.base_index, inst_left + idx * 32 * pitch,
                                                inst_left + (idx + 1) * 32 * pitch, width=5, unit_mode=True)
            self.template_base.add_wires(top_layer-1, vr_tid.base_index, inst_left + idx * 32 * pitch,
                                         inst_left + (idx + 1) * 32 * pitch,
                                         width=15, unit_mode=True)
            vr_id = self.grid.coord_to_nearest_track(VDD_layer_id, inst_top,
                                                     mode=1, half_track=True, unit_mode=True)
            vr_tid = TrackID(VDD_layer_id, vr_id, width=5)
            self.template_base.connect_with_via_stack(warr, vr_tid)
        self.template_base.add_pin('VDD', warr, show=True)
        num = (inst_top - inst_bottom) // (32 * pitch)
        for idx in range(num):
            vr_id = self.grid.coord_to_nearest_track(top_layer, inst_right,
                                                     mode=1, half_track=True, unit_mode=True) + 3
            vr_tid = TrackID(top_layer, vr_id, width=10)
            warr = self.template_base.add_wires(top_layer, vr_tid.base_index, inst_bottom + (idx - 1) * 32 * pitch,
                                                inst_bottom + (idx + 1) * 32 * pitch, width=2, unit_mode=True)
            self.template_base.add_wires(top_layer, vr_tid.base_index, inst_bottom + (idx - 1) * 32 * pitch,
                                         inst_bottom + (idx + 2) * 32 * pitch,
                                         width=7, unit_mode=True)
            vr_id = self.grid.coord_to_nearest_track(GND_layer_id, inst_right,
                                                     mode=1, half_track=True, unit_mode=True)
            vr_tid = TrackID(GND_layer_id, vr_id, width=5)
            self.template_base.connect_with_via_stack(warr, vr_tid)
        self.template_base.add_pin('VSS', warr, show=True)
        for idx in range(num):
            vr_id = self.grid.coord_to_nearest_track(top_layer, inst_left,
                                                     mode=1, half_track=True, unit_mode=True) - 3
            vr_tid = TrackID(top_layer, vr_id, width=10)
            warr = self.template_base.add_wires(top_layer, vr_tid.base_index, inst_bottom + (idx - 1) * 32 * pitch,
                                                inst_bottom + (idx + 1) * 32 * pitch, width=2, unit_mode=True)
            self.template_base.add_wires(top_layer, vr_tid.base_index, inst_bottom + (idx - 1) * 32 * pitch,
                                         inst_bottom + (idx + 2) * 32 * pitch,
                                         width=7, unit_mode=True)
            vr_id = self.grid.coord_to_nearest_track(GND_layer_id, inst_left,
                                                     mode=1, half_track=True, unit_mode=True)
            vr_tid = TrackID(GND_layer_id, vr_id, width=5)
            self.template_base.connect_with_via_stack(warr, vr_tid)
        self.template_base.add_pin('VSS', warr, show=True)


# -------------------------------------- Unit Tests -------------------------------------

class TestRoutingGridHelper(unittest.TestCase):
    def setUp(self):
        pass

    # NOTE: no tests yet
    # def test_foo(self):
    #     self.assertEqual(....)


if __name__ == '__main__':
    unittest.main()
