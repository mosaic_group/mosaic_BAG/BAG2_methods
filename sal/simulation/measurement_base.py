# -*- coding: utf-8 -*-

from __future__ import annotations  # allow class type hints within same class
from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import Enum
from typing import *

from sal.params_base import MeasurementParamsBase
from sal.testbench_base import *
from sal.simulation.simulation_mode import SimulationMode


class MeasurementState(Enum):
    @classmethod
    @abstractmethod
    def initial_state(cls) -> MeasurementState:
        pass

    @classmethod
    @abstractmethod
    def final_states(cls) -> Set[MeasurementState]:
        pass


class MeasurementBase(ABC):
    @classmethod
    @abstractmethod
    def name(cls) -> str:
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def description(cls) -> str:
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def parameter_class(cls) -> Type[MeasurementParamsBase]:
        raise NotImplementedError

    @classmethod
    def fsm_state_class(cls) -> Type[MeasurementState]:
        return opamp_two_stage_fullchip_measurement1_state

    @classmethod
    @abstractmethod
    def testbench_setup_list(cls) -> List[TestbenchSetup]:
        raise NotImplementedError

    # ----------------------

    @abstractmethod
    def prepare_testbench(self,
                          current_state: MeasurementState,
                          previous_output: Optional[Dict[str, Any]]) -> TestbenchRun:
        pass

    @abstractmethod
    def process_output(self,
                       current_state: MeasurementState,
                       current_output: Dict[str, Any],
                       testbench_run: TestbenchRun) -> (MeasurementState,
                                                        Dict[str, Any]):
        pass


def split_data_by_sweep(results: Dict,
                        var_list: List[str]) -> List[Tuple[str, Dict]]:
    sweep_names = results['sweep_params'][var_list[0]][:-1]
    combo_list = []
    for name in sweep_names:
        combo_list.append(range(results[name].size))

    if combo_list:
        idx_list_iter = product(*combo_list)
    else:
        idx_list_iter = [[]]

    ans_list = []
    for idx_list in idx_list_iter:
        cur_label_list = []
        for name, idx in zip(sweep_names, idx_list):
            swp_val = results[name][idx]
            if isinstance(swp_val, str):
                cur_label_list.append('%s=%s' % (name, swp_val))
            else:
                cur_label_list.append('%s=%.4g' % (name, swp_val))

        if cur_label_list:
            label = ', '.join(cur_label_list)
        else:
            label = ''

        cur_idx_list = list(idx_list)
        cur_idx_list.append(slice(None))

        cur_results = {var: results[var][tuple(cur_idx_list)] for var in var_list}
        ans_list.append((label, cur_results))

    return ans_list

