#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import annotations
from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import Enum
import os
from typing import *
import re
import subprocess
import sys
import unittest

import bag.core
import bag.data
import bag.io

from BAG2_methods.flow_variant import BAGFlowVariant
from sal.log_analyzer import *

from .simulation_result import SimulationResult
from .simulation_output import *
from ..testbench_params import *


# TODO: this should be moved to SimAccess interface / subclasses
class SimAccessAdditions(ABC):
    @abstractmethod
    def format_output(self,
                      output: SimulationOutputBase) -> str:
        pass

    @abstractmethod
    def validate_simulation_log(self,
                                log_dir: str):
        pass

    @abstractmethod
    def load_simulation_result(self,
                               bag_tb: bag.core.Testbench,
                               outputs: Dict[str, SimulationOutputBase]) -> SimulationResult:
        pass

    @abstractmethod
    def save_simulation_result(self,
                               result: SimulationResult,
                               hdf5_path: str):
        pass

    @classmethod
    def instance_for_flow_variant(cls) -> SimulationResultLoader:
        flow_variant = BAGFlowVariant.from_bag_config()
        match flow_variant:
            case BAGFlowVariant.BAG2_VIRTUOSO:
                return OceanSimAccessAdditions()

            case BAGFlowVariant.BAG2_OPEN_SOURCE:
                return NGSpiceSimAccessAdditions()

            case _:
                raise ValueError(f"unsupported flow variant {flow_variant}")


# TODO: this should be moved to SimAccess subclasses ...
class OceanSimAccessAdditions(SimAccessAdditions):
    def format_output(self, output: SimulationOutputBase) -> str:
        match output:
            case SimulationOutputRawExpression():
                o: SimulationOutputRawExpression = output
                return o.expr

            case SimulationOutputVoltage():
                o: SimulationOutputVoltage = output
                formatted: str
                match o.quantity:
                    case VoltageQuantity.V:
                        formatted = f"v(\"{o.signal}\" ?result '{o.analysis})"
                    case VoltageQuantity.V_REAL:
                        formatted = f"real(v(\"{o.signal}\" ?result '{o.analysis}))"
                    case VoltageQuantity.V_IMAGINARY:
                        formatted = f"imag(v(\"{o.signal}\" ?result '{o.analysis}))"
                    case VoltageQuantity.V_MAGNITUDE:
                        formatted = f"mag(v(\"{o.signal}\" ?result '{o.analysis}))"
                    case VoltageQuantity.V_PHASE:
                        formatted = f"phase(v(\"{o.signal}\" ?result '{o.analysis}))"
                    case VoltageQuantity.V_DB20log10:
                        formatted = f"dB20(v(\"{o.signal}\" ?result '{o.analysis}))"
                    case _:
                        raise NotImplementedError(f"unsupported quantity kind {o}")
                return formatted

            case SimulationOutputCurrent():
                o: SimulationOutputCurrent = output
                formatted = f"getData(\"{o.terminal}\" ?result '{o.analysis})"
                return formatted

            case SimulationOutputDeviceParameter():
                o: SimulationOutputDeviceParameter = output
                formatted = f"getData(\"{o.device}:{o.parameter}\" ?result '{o.analysis})"
                return formatted

            case _:
                raise NotImplementedError(f"unsupported type {val.__class__}")

    def validate_simulation_log(self,
                                log_dir: str):
        log_path = os.path.join(log_dir, "ocn_output.log")

        if not os.path.exists(log_path):
            self.logger.error(f"ERROR: simulation log does not exist at {log_path}")
            sys.exit(1)

        analysis = LogAnalyzer.default().analyze_path(log_path)
        analysis.print_issues()
        if not analysis.successful:
            raise Exception("Stopping due to simulation errors")

    def load_simulation_result(self,
                               bag_tb: bag.core.Testbench,
                               outputs: Dict[str, SimulationOutputBase]) -> SimulationResult:
        result_dict = bag.data.load_sim_results(bag_tb.save_dir)
        return SimulationResult.from_bag_dict(result_dict)

    def save_simulation_result(self,
                               result: SimulationResult,
                               hdf5_path: str):
        result.save_to_hdf5(hdf5_path=hdf5_path)


# TODO: this should be moved to SimAccess subclasses ...
class NGSpiceSimAccessAdditions(SimAccessAdditions):
    def format_output(self, output: SimulationOutputBase) -> str:
        match output:
            case SimulationOutputRawExpression():
                o: SimulationOutputRawExpression = output
                return o.expr

            case SimulationOutputVoltage():
                o: SimulationOutputVoltage = output
                formatted: str
                match o.quantity:
                    case VoltageQuantity.V:
                        formatted = f"v({o.analysis}.{o.signal})"
                    case VoltageQuantity.V_REAL:
                        formatted = f"vr({o.analysis}.{o.signal})"
                    case VoltageQuantity.V_IMAGINARY:
                        formatted = f"vi({o.analysis}.{o.signal})"
                    case VoltageQuantity.V_MAGNITUDE:
                        formatted = f"vm({o.analysis}.{o.signal})"
                    case VoltageQuantity.V_PHASE:
                        formatted = f"vp({o.analysis}.{o.signal})"
                    case VoltageQuantity.V_DB20log10:
                        formatted = f"vdb({o.analysis}.{o.signal})"
                    case _:
                        raise NotImplementedError(f"unsupported quantity kind {o}")
                return formatted

            case SimulationOutputCurrent():
                o: SimulationOutputCurrent = output
                # for whatever reason, ngspice does not accept the plot name as a qualifier for currents,
                # i.e. this does not work:
                #     i(dc.vss)
                # while this works:
                #     i(vss)
                formatted = f"i({o.signal})"
                return formatted

            case SimulationOutputDeviceParameter():
                o: SimulationOutputDeviceParameter = output
                # device = o.device   # TODO: technology dependent transistor model etc
                # formatted = f"\"{o.device}:{o.parameter}\"#"
                # return formatted
                raise NotImplementedError("TODO: not yet implemented for ngspice")

            case _:
                raise NotImplementedError(f"unsupported type {val.__class__}")

    # NOTE: parsing the output expressions is not really possible
    #       instead we rather ensure that load_simulation_result gets passed the SimulationOutputBase objects
    # -----------------
    # @staticmethod
    # def parse_output(expr: str) -> SimulationOutputBase:
    #     """
    #
    #     Parameters
    #     ----------
    #     expr
    #         Expression as documented in the ngspice-manual, chapter 17.2ff
    #
    #     Returns
    #     -------
    #         Tuple with the parsed expression and the Analysis name involved
    #     """
    #     # vm(tran.vout) -> SimulationOutputSignalData()
    #     # more complex expressions -> SimulationOutputRawExpression
    #     get_data_pattern = r'(?P<quantity>\w+)\((?P<analysis>\w+)\.(?P<vector>\w+)\)'
    #     matches = re.findall(get_data_pattern, expr)
    #     if len(matches) == 1:  # Example: v(tran.vin1)
    #         quantity, analysis, vector = matches[0]
    #         parsed_quantity = SignalQuantity(quantity)
    #         return SimulationOutputSignalData(analysis=analysis,
    #                                           signal=vector,
    #                                           quantity=parsed_quantity)
    #     elif len(matches) >= 2:  # Example: v(tran.vin1) + v(tran.vin2)
    #         plots = set()
    #         for quantity, analysis, vector in matches:
    #             plots.add(analysis)
    #         if len(plots) >= 2:
    #             raise ValueError(f"invalid output expression '{expr}' depends on multiple plots: {plots}")
    #         return SimulationOutputRawExpression(analysis=plots.pop(), expr=expr)
    #     else:
    #         analysis = 'previous'  # TODO: find out which analysis is meant
    #         return SimulationOutputRawExpression(analysis=analysis, expr=expr)

    def validate_simulation_log(self,
                                log_dir: str):
        log_path = os.path.join(log_dir, "ngspice_run.log")

        if not os.path.exists(log_path):
            self.logger.error(f"ERROR: simulation log does not exist at {log_path}")
            sys.exit(1)

        analysis = LogAnalyzer.default().analyze_path(log_path)
        analysis.print_issues()
        if not analysis.successful:
            raise Exception("Stopping due to simulation errors")

    def load_simulation_result(self,
                               bag_tb: bag.core.Testbench,
                               outputs: Dict[str, SimulationOutputBase]) -> SimulationResult:
        """Load exported simulation results from the given directory.

        Parameters
        ----------
        bag_tb
            the testbench for this simulation run

        outputs
            output expressions for the final result vectors

        Returns
        -------
        results : SimulationResult
            contains sweep parameters and output signals.
            the values are the corresponding data as a numpy array.

            In addition, results has a key called 'sweep_params', which maps an
            output signal name to a list of sweep parameters of that output.
        """

        from opensource.ngspice.spice_raw_file_reader import SPICERawContent

        result: SimulationResult
        sweep_params: Dict[str, List[str]] = {}
        results: Dict[str, np.array] = {}

        save_dir = bag_tb.save_dir

        # NOTE: result.raw contains the full simulation results for all analyses
        raw_path = os.path.join(save_dir, "result.raw")

        # NOTE: output.raw contains exactly the configured output expressions
        output_path = os.path.join(save_dir, "output.raw")

        # *** Auto-generated script to generate vectors from a list of output expressions
        #
        # *** (BEGIN ignore)
        # * NOTE: dummy op analysis to avoid ngspice return exit code 1
        # Vdummy 5V GND 5
        # .op
        # *** (END ignore)
        #
        # .control
        # load "result.raw"
        # setplot tran
        # let vin_tran = v(tran.vin)
        # let vout_tran = v(tran.vout)
        # let vdd_tran = v(tran.vdd)
        # write output.raw vin_tran vout_tran vdd_tran
        # set appendwrite
        # setplot ac
        # let vout_ac = vm(ac.vout)
        # write output.raw vout_ac
        # .endc

        spice_script = os.path.join(save_dir, "extract-outputs.spice")
        spice = "*** Auto-generated script to generate vectors from a list of output expressions\n" \
                "\n" \
                "*** (BEGIN ignore)\n" \
                "* NOTE: dummy op analysis to avoid ngspice return exit code 1\n" \
                "Vdummy 5V GND 5\n" \
                ".op\n" \
                "*** (END ignore)\n" \
                "\n" \
                ".control\n" \
                f"load \"{os.path.basename(raw_path)}\"\n"

        #
        # NOTE:
        #     - ngspice has the concept of a current plot, and it's vectors
        #     - current plot context can be switched, i.e. using "setplot ac" / "setplot tran"
        #     - evaluation of exprs must happen in the context of the respective plot,
        #       to retain the plot attributes
        #        - plot number data type (real vs complex)
        #        - plot analysis type
        #        - etc
        #
        PlotName = str
        VectorName = str
        outputs_grouped_by_plot: Dict[PlotName, Dict[VectorName, SimulationOutputBase]] = {}

        for vector, output in outputs.items():
            plot = output.analysis
            outputs = outputs_grouped_by_plot.setdefault(plot, {})
            outputs[vector] = output

        already_added_set_appendwrite = False
        for plot, outputs in outputs_grouped_by_plot.items():
            spice += f"setplot {plot}\n"
            for vector in outputs.keys():
                expr = bag_tb.outputs[vector]
                spice += f"let {vector} = {expr}\n"
            spice += f"write {os.path.basename(output_path)} {' '.join(outputs.keys())}\n"
            if not already_added_set_appendwrite:
                spice += f"set appendwrite\n"
                already_added_set_appendwrite = True

        spice += ".endc\n"

        with open(spice_script, "w") as f:
            f.write(spice)

        # get the simulation command.
        sim_kwargs = bag_tb.sim.sim_config['kwargs']
        cfg_cmd = sim_kwargs['command']
        cmd = [
            cfg_cmd,
            '-b',
            spice_script
        ]

        s = subprocess.check_output(cmd, cwd=save_dir)

        raw_content = SPICERawContent.read_file(output_path)

        # NOTE: we normalize scale vector names to be compatible switch ocean
        scale_name_normalization = {
            'frequency': 'freq',
        }
        for plot in raw_content.plots:
            scale_vector_name = plot.scale_vector.variable.name
            if scale_vector_name in scale_name_normalization:
                scale_vector_name = scale_name_normalization[scale_vector_name]
            results[scale_vector_name] = plot.scale_vector.data
            for data_vector in plot.data_vectors:
                data_vector_name = data_vector.variable.name
                # NOTE: ngspice data vector name will include i or v as a quantity/"function", i.e.
                #       v(vout_tran)
                #       so we want to extract the vector name without the "function" wrapped around it
                pattern = r'\w+\((?P<vector>\w+)\)'
                m = re.match(pattern, data_vector_name)
                if m:
                    data_vector_name = m.group('vector')
                results[data_vector_name] = data_vector.data
                sweep_params[data_vector_name] = [scale_vector_name]

        result = SimulationResult(sweep_params=sweep_params, results=results)
        return result

    def save_simulation_result(self,
                               result: SimulationResult,
                               hdf5_path: str):
        result.save_to_hdf5(hdf5_path=hdf5_path)


# --------------------------------------- tests ----------------------------------------
class TestNGSpiceSimAccessAdditions(unittest.TestCase):
    def assertSymmetrical(self, output: SimulationOutputBase):
        sa = NGSpiceSimAccessAdditions()
        formatted_output = sa.format_output(output)
        parsed_output = sa.parse_output(formatted_output)
        self.assertEqual(output, parsed_output)

    def test_output_format_parse1(self):
        output = SimulationOutputVoltage(analysis='tran', signal='vout', quantity=VoltageQuantity.V_MAGNITUDE)
        self.assertSymmetrical(output)

    def test_output_format_parse2(self):
        output = SimulationOutputVoltage(analysis='tran', signal='vin', quantity=VoltageQuantity.V_MAGNITUDE)
        self.assertSymmetrical(output)

    def test_output_format_complex_expr(self):
        output = SimulationOutputRawExpression(analysis='tran', expr='v(tran.vin1)+v(tran.vin2)')
        self.assertSymmetrical(output)


if __name__ == '__main__':
    unittest.main()