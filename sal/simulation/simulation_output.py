from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from enum import Enum
from typing import *

from ..params_base import ParamsBase


@dataclass
class SimulationOutputBase(ParamsBase):
    analysis: str  # i.e. 'tran'


@dataclass
class SimulationOutputRawExpression(SimulationOutputBase):
    """
    For now, this is tool-specific, so for ADEXL i.e. the expr could be
    'getData("/vdd", ?result tran)'
    """
    expr: str


class VoltageQuantity(str, Enum):
    V = 'v'
    V_REAL = 'vr'
    V_IMAGINARY = 'vi'
    V_MAGNITUDE = 'vm'
    V_PHASE = 'vp'
    V_DB20log10 = 'vdb'


@dataclass
class SimulationOutputVoltage(SimulationOutputBase):
    signal: str     # i.e. 'vout'
    quantity: VoltageQuantity   # I, V, V_MAGNITUDE


@dataclass
class SimulationOutputCurrent(SimulationOutputBase):
    signal: str     # i.e. 'vout'     # used by Xschem
    terminal: str   # i.e. /GND/PLUS  # used by Virtuoso


@dataclass
class SimulationOutputDeviceParameter(SimulationOutputBase):
    device: str     # i.e. 'XDUT.XM.M0'
    parameter: str   # i.e. 'cds'
