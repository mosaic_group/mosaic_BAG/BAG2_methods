#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import annotations
from abc import ABC, abstractmethod
from dataclasses import dataclass
from functools import cached_property
import numpy as np
import os
import tempfile
from typing import *
import unittest

import bag.data
import bag.io.sim_data


@dataclass
class SweepParameter:
    name: str
    values: bag.io.sim_data.SweepArray


@dataclass
class SimulationResult:
    sweep_params: Dict[str, List[str]]
    results: Dict[str, np.array]

    @classmethod
    def from_hdf5(cls, hdf5_path: str) -> SimulationResult:
        bag_dict = bag.data.load_sim_file(hdf5_path)
        return cls.from_bag_dict(bag_dict)

    @classmethod
    def from_bag_dict(cls, bag_dict: Dict[str, Any]) -> SimulationResult:
        """

        Returns
        -------
        SimulationResult instance
        """
        sweep_params: Dict[str, List[str]] = {}
        results: Dict[str, np.array] = {}

        for key, value in bag_dict.items():
            if key == "sweep_params":
                sweep_params = value
            else:
                results[key] = value

        return SimulationResult(sweep_params=sweep_params, results=results)

    def as_bag_dict(self) -> Dict[str, Any]:
        bag_dict: Dict[str, Any] = dict(self.results)
        if len(self.sweep_params) >= 1:
            bag_dict['sweep_params'] = self.sweep_params
        return bag_dict

    def save_to_hdf5(self, hdf5_path: str):
        result_dict = self.as_bag_dict()
        bag.data.save_sim_results(result_dict, hdf5_path)


# --------------------------------------- tests ----------------------------------------
class TestSimulationResult(unittest.TestCase):
    @cached_property
    def test_hdf5_path(self) -> str:
        THIS_DIR = os.path.dirname(os.path.abspath(__file__))
        test_data_path = os.path.join(THIS_DIR, 'testdata', 'amp_cs_gen_ac_tran_tb_0000_plot.hdf5')
        return test_data_path

    def test_from_hdf5(self):
        result = SimulationResult.from_hdf5(hdf5_path=self.test_hdf5_path)
        print(result)

    def assert_equal_files(self, path1: str, path2: str):
        with open(path1, "r") as f:
            with open(path2, "r") as g:
                a = f.read()
                b = g.read()
                self.assertEqual(a, b)

    def test_conversion_symmetry(self):
        result = SimulationResult.from_hdf5(hdf5_path=self.test_hdf5_path)
        out_hdf5_path = tempfile.mktemp(suffix='.hdf5', dir=os.environ["BAG_TEMP_DIR"])
        result.save_to_hdf5(hdf5_path=out_hdf5_path)

        self.assert_equal_files(self.test_hdf5_path, out_hdf5_path)

    def test_bag_conversion_symmetry(self):
        bag_dict = bag.data.load_sim_file(self.test_hdf5_path)
        out_hdf5_path = tempfile.mktemp(suffix='.hdf5', dir=os.environ["BAG_TEMP_DIR"])
        bag.data.save_sim_results(bag_dict, out_hdf5_path)

        self.assert_equal_files(self.test_hdf5_path, out_hdf5_path)

