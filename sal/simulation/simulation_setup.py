from dataclasses import dataclass

import sal.design_base
from sal.testbench_base import TestbenchBase, TestbenchSetup
from sal.pkg.generator_package import GeneratorPackage
from sal.pkg.testbench_package import TestbenchPackage
from .simulation_mode import SimulationMode


@dataclass
class SimulationSetup:
    generator_package: GeneratorPackage
    testbench_package: TestbenchPackage
    generator_instance: sal.design_base.DesignBase
    testbench_instance: TestbenchBase
    simulation_mode: SimulationMode
    testbench_setup: TestbenchSetup
