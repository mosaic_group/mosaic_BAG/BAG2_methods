from __future__ import annotations  # allow class type hints within same class
from abc import abstractmethod
from dataclasses import dataclass
from typing import *

from ..params_base import ParamsBase
from .simulation_scalar import SimulationScalar

@dataclass
class SimulationSweepBase(ParamsBase):
    name: str

    @abstractmethod
    def to_bag_args(self) -> Any:
        """
        defines how to prepare sweep for Testbench.set_sweep_parameter()
        """
        pass


@dataclass
class SimulationSweepList(SimulationSweepBase):
    values: List[SimulationScalar]

    def to_bag_args(self) -> Any:
        """
        defines how to prepare sweep for Testbench.set_sweep_parameter()
        """
        return dict(values=self.values)


@dataclass
class SimulationSweepDecade(SimulationSweepBase):
    start: SimulationScalar
    stop: SimulationScalar
    num_decade: int

    def to_bag_args(self) -> Any:
        """
        defines how to prepare sweep for Testbench.set_sweep_parameter()
        """
        return dict(start=self.values, stop=self.stop, num_decade=self.num_decade)


@dataclass
class SimulationSweepLinStep(SimulationSweepBase):
    start: SimulationScalar
    stop: SimulationScalar
    step: SimulationScalar

    def to_bag_args(self) -> Any:
        """
        defines how to prepare sweep for Testbench.set_sweep_parameter()
        """
        return dict(start=self.start, stop=self.stop, step=self.step)
