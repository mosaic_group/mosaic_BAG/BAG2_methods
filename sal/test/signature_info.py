#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
import inspect
from typing import *


class AttributeCache:
    def __init__(self):
        self.attributes_by_class: Dict[ClassVar, List[inspect.Attribute]] = {}

    def cached_attributes(self, cls: ClassVar) -> List[inspect.Attribute]:
        if cls not in self.attributes_by_class:
            attributes = inspect.classify_class_attrs(cls)
            supported_attribute_types = {'property', 'method'}
            attributes = list(filter(lambda a: a.kind in supported_attribute_types, attributes))
            attributes = list(filter(lambda a: not a.name.startswith('_'), attributes))
            self.attributes_by_class[cls] = attributes
            return attributes
        return self.attributes_by_class[cls]

    def cached_attribute(self, cls: ClassVar, name: str) -> Optional[inspect.Attribute]:
        attributes = self.cached_attributes(cls)
        matches = list(filter(lambda a: a.name == name, attributes))
        if len(matches) == 0:
            return None
        return matches[0]


@dataclass
class AttributeDefinitionInfo:
    attribute: inspect.Attribute
    signature: inspect.Signature

    def __init__(self,
                 attribute: inspect.Attribute):
        self.attribute = attribute
        self.signature = inspect.signature(self.getter)

        # NOTE: sometimes parameters or return annotations can be strings

        if isinstance(self.signature.return_annotation, str):
            return_type = eval(self.signature.return_annotation)
            self.signature = self.signature.replace(return_annotation=return_type)

    @property
    def name(self) -> str:
        return self.attribute.name

    @property
    def defining_class(self) -> ClassVar:
        return self.attribute.defining_class

    @property
    def is_abstract(self) -> bool:
        return hasattr(self.getter, '__isabstractmethod__')

    @property
    def file(self) -> str:
        return inspect.getsourcefile(self.getter)

    @property
    def line(self) -> int:
        _, line = inspect.getsourcelines(self.getter)
        return line

    @property
    def source(self) -> List[str]:
        src, _ = inspect.getsourcelines(self.getter)
        return src

    @property
    def getter(self) -> function:
        if self.attribute.kind == 'property':
            return self.attribute.object.fget
        elif self.attribute.kind == 'method':
            return self.attribute.object
        else:
            raise Exception(f"unsupported attribute kind {self.attribute.kind}")

    @property
    def description(self) -> str:
        return f"{self.defining_class} defines: {self.signature}\n\t\t{self.file}:{self.line}"


@dataclass
class AttributeSignatureInfo:
    cls: ClassVar
    attribute_name: str
    definitions: [AttributeDefinitionInfo]

    def __init__(self,
                 cls: ClassVar,
                 attribute_name: str,
                 cache: AttributeCache):
        self.cls = cls
        self.attribute_name = attribute_name
        self.definitions = []

        mro_classes = inspect.getmro(cls)
        for c in mro_classes:
            a = cache.cached_attribute(cls=c, name=attribute_name)
            if a is None:
                continue

            is_inherited = a.defining_class != c
            if is_inherited:
                continue

            definition_info = AttributeDefinitionInfo(attribute=a)
            self.definitions.append(definition_info)

    @property
    def has_concrete_definition(self) -> bool:
        return len(self.definitions) > 0 and \
               not self.definitions[0].is_abstract

    @property
    def has_redeclared_abstract_methods(self) -> bool:
        return len(list(filter(lambda d: d.is_abstract, self.definitions))) > 1

    @property
    def return_annotations(self) -> List[str]:
        return list(map(lambda d: d.signature.return_annotation, self.definitions))

    @property
    def description(self) -> str:
        lines = map(lambda d: f"\t{d.description}", self.definitions)
        return '\n'.join(lines)


