#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from abc import abstractmethod
from dataclasses import dataclass
import inspect
import pprint
import sys
from typing import *
import typing_extensions
import unittest

import sal.bag_startup
sal.bag_startup.BagStartup.setup_sys_path()  # NOTE: add frameworks to sys.path, this influences the following imports!

import bag

from sal.log import warn, error, success, info
from type_checker import *
from signature_info import *


class BAG2TechnologyDefinitionTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.project = bag.BagProject()
        cls.tech_info = cls.project.tech_info
        cls.attribute_cache = AttributeCache()

        # Show full diff in self.assertEqual.
        if 'unittest.util' in __import__('sys').modules:
            __import__('sys').modules['unittest.util']._MAX_LENGTH = 999999999

    def test_techInfoAvailable(self):
        self.assertIsNotNone(self.tech_info)
        self.assertIsInstance(self.tech_info, bag.layout.TechInfo)

    def validate_return_type_hints(self,
                                   sig: AttributeSignatureInfo):

        if len(sig.definitions) == 0:
            raise Exception(f"Found no definitions for attribute {sig.attribute_name} "
                            f"in class {sig.cls}")

        self.assertTrue(sig.has_concrete_definition,
                        f"ERROR: No concrete definition for {sig.attribute_name}, still abstract\n"
                        f"Definitions found:\n{sig.description}")

        self.assertFalse(sig.has_redeclared_abstract_methods,
                         f"ERROR: Superfluous redeclared abstract methods detected "
                         f"for {sig.attribute_name}\n"
                         f"Definitions found:\n{sig.description}")

        self.assertSetEqual(set(sig.return_annotations),
                            {sig.definitions[0].signature.return_annotation},
                            f"\nattribute '{sig.attribute_name}' has inconsistent return type signatures\n"
                            f"Conflicting definitions found:\n{sig.description}")

    def validate_return_type(self,
                             obj: object,
                             attribute: Union[str, Callable, property],
                             test_kwargs: Dict[str, Any]) -> Any:
        if isinstance(attribute, Callable):
            attribute = attribute.__name__
        elif isinstance(attribute, property):
            attribute = attribute.fget.__name__

        self.assertIsNotNone(obj)
        self.assertIsInstance(obj, object)
        cls = obj.__class__
        a = self.attribute_cache.cached_attribute(cls, attribute)

        sig_info = AttributeSignatureInfo(cls=cls, attribute_name=attribute, cache=self.attribute_cache)

        self.validate_return_type_hints(sig=sig_info)

        definition = sig_info.definitions[0]

        obtained_result: Any

        if test_kwargs is None or len(test_kwargs) == 0:
            obtained_result = definition.getter(obj)
        else:
            obtained_result = definition.getter(obj, **test_kwargs)

        return_type_name = definition.signature.return_annotation
        self.assertIsNot(return_type_name, definition.signature.empty)

        ch = TypeChecker(obj=obtained_result, type_annotation=return_type_name)
        result = ch.check()
        self.assertEqual(result.reason, TypeCheckResult.MATCH,
                         f"[Context] {result.request}")

        return obtained_result

    # TODO: find out res types
    @property
    def res_types(self) -> List[str]:
        return ['standard']

    @property
    def bm_types(self) -> List[str]:
        return ['']

    @property
    def tm_types(self) -> List[str]:
        return ['']

    @property
    def layer_id_to_name_map(self) -> Dict[int, str]:
        name_map = self.tech_info.process_config['layer_name']
        return name_map

    @property
    def layer_ids(self) -> List[int]:
        return list(self.layer_id_to_name_map.keys())

    @property
    def layer_names(self) -> List[str]:
        return list(self.layer_id_to_name_map.values())

    @property
    def metal_exclude_layer_ids(self) -> List[int]:
        return list(self.tech_info.process_config['metal_exclude_table'].keys())

    def test_layer_name_part_of_config(self) -> Dict[int, str]:
        try:
            name_map = self.layer_id_to_name_map
        except KeyError:
            self.fail("layer_name dictionary is missing in config")
        return name_map

    def test_get_well_layers(self):
        for sub_type in ['ntap', 'ptap']:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_well_layers,
                                      test_kwargs=dict(
                                          sub_type=sub_type
                                      ))

    def test_get_implant_layers(self):
        for mos_type in ['nch', 'pch']:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_implant_layers,
                                      test_kwargs=dict(
                                          mos_type=mos_type,
                                          res_type=None
                                      ))

    def test_get_threshold_layers(self):
        for mos_type in ['nch', 'pch']:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_threshold_layers,
                                      test_kwargs=dict(
                                          mos_type=mos_type,
                                          threshold='standard',
                                          res_type=None
                                      ))

    def test_get_exclude_layer(self):
        for layer_id in self.metal_exclude_layer_ids:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_exclude_layer,
                                      test_kwargs=dict(
                                          layer_id=layer_id
                                      ))

    def test_get_dnw_margin_unit(self):
        for dnw_mode in ['normal', 'adjacent', 'compact']:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_dnw_margin_unit,
                                      test_kwargs=dict(
                                          dnw_mode=dnw_mode
                                      ))

    def test_get_dnw_layers(self):
        self.validate_return_type(obj=self.tech_info,
                                  attribute=bag.layout.TechInfo.get_dnw_layers,
                                  test_kwargs={})

    def test_get_res_metal_layers(self):
        for layer_id in range(1, 5):
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_res_metal_layers,
                                      test_kwargs=dict(
                                          layer_id=layer_id
                                      ))

    def test_get_metal_dummy_layers(self):
        for layer_id in range(1, 5):
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_metal_dummy_layers,
                                      test_kwargs=dict(
                                          layer_id=layer_id
                                      ))

    # TODO: test missing for add_cell_boundary
    # TODO: test missing for draw_device_blockage

    def test_get_via_drc_info(self):
        layer_id_bot = 4
        via_name = self.tech_info.get_via_name(layer_id_bot)

        self.validate_return_type(obj=self.tech_info,
                                  attribute=bag.layout.TechInfo.get_via_drc_info,
                                  test_kwargs=dict(
                                      vname=via_name,
                                      vtype='square',
                                      mtype='top',
                                      mw_unit=1,
                                      is_bot=False
                                  ))

    def test_get_min_space(self):
        layer_name = self.tech_info.get_layer_name(1)
        layer_type = self.tech_info.get_layer_type(layer_name)

        self.validate_return_type(obj=self.tech_info,
                                  attribute=bag.layout.TechInfo.get_min_space,
                                  test_kwargs=dict(
                                      layer_type=layer_type,
                                      width=10.0,
                                      unit_mode=False,
                                      same_color=False
                                  ))

    def test_get_min_line_end_space(self):
        layer_name = self.tech_info.get_layer_name(1)
        layer_type = self.tech_info.get_layer_type(layer_name)

        self.validate_return_type(obj=self.tech_info,
                                  attribute=bag.layout.TechInfo.get_min_line_end_space,
                                  test_kwargs=dict(
                                      layer_type=layer_type,
                                      width=10.0,
                                      unit_mode=False
                                  ))

    def test_get_min_length(self):
        layer_name = self.tech_info.get_layer_name(1)
        layer_type = self.tech_info.get_layer_type(layer_name)

        self.validate_return_type(obj=self.tech_info,
                                  attribute=bag.layout.TechInfo.get_min_length,
                                  test_kwargs=dict(
                                      layer_type=layer_type,
                                      width=10.0
                                  ))

    def test_get_layer_id(self):
        for layer_name in self.layer_names:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_layer_id,
                                      test_kwargs=dict(
                                          layer_name=layer_name,
                                      ))

    def test_get_layer_name(self):
        for layer_id in self.layer_ids:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_layer_name,
                                      test_kwargs=dict(
                                          layer_id=layer_id,
                                      ))

    def test_get_layer_type(self):
        for layer_name in self.layer_names:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_layer_type,
                                      test_kwargs=dict(
                                          layer_name=layer_name,
                                      ))

    def test_get_via_name(self):
        for layer_id in range(1, 3):
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_via_name,
                                      test_kwargs=dict(
                                          bot_layer_id=layer_id,
                                      ))

    def test_get_metal_em_specs(self):
        for layer_name in self.layer_names[:5]:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_metal_em_specs,
                                      test_kwargs=dict(
                                          layer_name=layer_name,
                                          w=10.0,
                                          l=-1.0,
                                          vertical=False
                                      ))

    def test_get_via_em_specs(self):
        layer_id_bot = 4
        layer_name_bot = self.tech_info.get_layer_name(layer_id_bot)
        layer_name_top = self.tech_info.get_layer_name(5)
        via_name = self.tech_info.get_via_name(layer_id_bot)

        self.validate_return_type(obj=self.tech_info,
                                  attribute=bag.layout.TechInfo.get_via_em_specs,
                                  test_kwargs=dict(
                                      via_name=via_name,
                                      bm_layer=layer_name_bot,
                                      tm_layer=layer_name_top,
                                      via_type='square',
                                      bm_dim=(-1, -1),
                                      tm_dim=(-1, -1),
                                      array=False
                                  ))

    def test_get_res_rsquare(self):
        for res_type in self.res_types:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_res_rsquare,
                                      test_kwargs=dict(
                                          res_type=res_type
                                      ))

    def test_get_res_width_bounds(self):
        for res_type in self.res_types:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_res_width_bounds,
                                      test_kwargs=dict(
                                          res_type=res_type
                                      ))

    def test_get_res_length_bounds(self):
        for res_type in self.res_types:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_res_length_bounds,
                                      test_kwargs=dict(
                                          res_type=res_type
                                      ))

    def test_get_res_min_nsquare(self):
        for res_type in self.res_types:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_res_min_nsquare,
                                      test_kwargs=dict(
                                          res_type=res_type
                                      ))

    def test_get_res_em_specs(self):
        for res_type in self.res_types:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_res_em_specs,
                                      test_kwargs=dict(
                                          res_type=res_type,
                                          w=10.0,
                                          l=-1.0
                                      ))

    def test_via_tech_name(self):
        self.validate_return_type(obj=self.tech_info,
                                  attribute=bag.layout.TechInfo.via_tech_name,
                                  test_kwargs={})

    def test_pin_purpose(self):
        self.validate_return_type(obj=self.tech_info,
                                  attribute=bag.layout.TechInfo.pin_purpose,
                                  test_kwargs={})

    def test_resolution(self):
        self.validate_return_type(obj=self.tech_info,
                                  attribute=bag.layout.TechInfo.resolution,
                                  test_kwargs={})

    def test_layout_unit(self):
        self.validate_return_type(obj=self.tech_info,
                                  attribute=bag.layout.TechInfo.layout_unit,
                                  test_kwargs={})

    # TODO: test missing for merge_well

    def test_use_flip_parity(self):
        self.validate_return_type(obj=self.tech_info,
                                  attribute=bag.layout.TechInfo.use_flip_parity,
                                  test_kwargs={})

    # TODO: test missing for finalize_template

    def test_get_res_info(self):
        res_type = 'standard'
        for res_type in self.res_types:
            self.validate_return_type(obj=self.tech_info,
                                      attribute=bag.layout.TechInfo.get_res_info,
                                      test_kwargs=dict(
                                          res_type=res_type,
                                          w=10.0,
                                          l=10.0
                                      ))

    def test_get_via_types(self):
        for bm_type in self.bm_types:
            for tm_type in self.tm_types:
                self.validate_return_type(obj=self.tech_info,
                                          attribute=bag.layout.TechInfo.get_via_types,
                                          test_kwargs=dict(
                                              bmtype=bm_type,
                                              tmtype=tm_type
                                          ))

    # TODO: test missing for get_best_via_array
    # TODO: test missing for _via_better

    def test_get_via_id(self):
        layer_name_bot = self.tech_info.get_layer_name(4)
        layer_name_top = self.tech_info.get_layer_name(5)
        self.validate_return_type(obj=self.tech_info,
                                  attribute=bag.layout.TechInfo.get_via_id,
                                  test_kwargs=dict(
                                      bot_layer=layer_name_bot,
                                      top_layer=layer_name_top
                                  ))

    # TODO: test missing for get_via_info
    # TODO: test missing for design_resistor

    def test_grid_opts(self):
        self.validate_return_type(obj=self.tech_info,
                                  attribute="grid_opts",
                                  test_kwargs={})


if __name__ == '__main__':
    unittest.main()
