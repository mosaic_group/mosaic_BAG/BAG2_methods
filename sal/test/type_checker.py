#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class

import typing
from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from enum import Enum
import inspect
import pprint
from typing import *
import typing_extensions
import unittest


@dataclass
class TypeCheckRequest:
    obj: object
    type_annotation: Union[str, type, Any]


@dataclass
class TypeCheckResult:
    MATCH: ClassVar[str] = "MATCH"

    reason: str
    request: TypeCheckRequest

    @classmethod
    def match(cls, request: TypeCheckRequest) -> TypeCheckResult:
        return TypeCheckResult(reason=cls.MATCH, request=request)

    @property
    def is_match(self) -> bool:
        return self.reason == self.MATCH


class TypeChecker:
    def __init__(self,
                 obj: object,
                 type_annotation: Union[str, type, Any],
                 root_request: Optional[TypeCheckRequest] = None):
        # NOTE: python 3.11 should offer typing.reveal_type()
        #       for now, let's use the basic type
        self.expected_type = None
        self.type_args = None

        if isinstance(type_annotation, type):
            self.expected_type = type_annotation
        elif type_annotation is typing.Any:
            self.expected_type = type_annotation
        else:
            type_annotation, self.expected_type, self.type_args = self.evaluate_annotation_str(type_annotation)

        self.obj = obj
        self.type_annotation = type_annotation
        if root_request is None:
            root_request = TypeCheckRequest(obj=obj, type_annotation=type_annotation)
        self.root_request = root_request

    @property
    def match(self) -> TypeCheckResult:
        return TypeCheckResult.match(request=self.root_request)

    @staticmethod
    def evaluate_annotation_str(type_annotation: str) -> (str, type, Optional[Tuple]):
        expected_type: type
        type_args: Optional[Tuple] = None

        if not isinstance(type_annotation, str):
            # _GenericAlias, like List[Tuple[int, int]]
            type_annotation = str(type_annotation)
            type_annotation = type_annotation.replace("typing.", "")

        if isinstance(type_annotation, str):
            NoneType = type(None)   # NOTE: the eval() might use this
            try:
                evaluated_annotation = eval(type_annotation)
            except Exception:
                raise

            if evaluated_annotation is None:
                raise Exception(f"ERROR: unable to obtain type from {evaluated_annotation}, evaluated to None")

            if isinstance(evaluated_annotation, type):  # f.ex. eval('float') will return a float type
                expected_type = evaluated_annotation
            else:
                expected_type = typing_extensions.get_origin(evaluated_annotation)
                type_args = typing_extensions.get_args(evaluated_annotation)
                if expected_type is None:
                    raise Exception(f"ERROR: unable to obtain type from {evaluated_annotation}, got None")
        else:
            raise ValueError(f"ERROR: Unexpected argument {type_annotation} of type {type(type_annotation)}")

        return type_annotation, expected_type, type_args

    def check(self) -> TypeCheckResult:
        if self.expected_type is typing.Any:
            return self.match

        if self.expected_type != typing.Union:
            isai: bool
            try:
                isai = isinstance(self.obj, self.expected_type)
            except TypeError:
                raise Exception(f"ERROR: expected type or tuple of types, "
                                f"but got {self.expected_type} of type {type(self.expected_type)}")
            if not isai:
                return TypeCheckResult(
                    reason=f"ERROR: obtained object {self.obj} of type {type(self.obj)}, "
                           f"expected type conforming to {self.type_annotation}",
                    request=self.root_request
                )
            else:
                # info(f"\nTEST [{context}] obtained object {obj} of type {type(obj)}, "
                #      f"expected type conforming to {type_annotation}?")
                pass

        if self.type_args:  # NOTE: for List[Tuple[int, int]], this would be Tuple[int, int]
            if self.expected_type == list:
                for elem in self.obj:
                    # in case of list we have only 1 argument
                    ch = TypeChecker(obj=elem, type_annotation=self.type_args[0], root_request=self.root_request)
                    result = ch.check()
                    if not result.is_match:
                        return result
            elif self.expected_type == dict:
                for (k, v) in self.obj.items():
                    # in case of dict we have 2 arguments (key, value)
                    ch1 = TypeChecker(obj=k, type_annotation=self.type_args[0], root_request=self.root_request)
                    ch2 = TypeChecker(obj=v, type_annotation=self.type_args[1], root_request=self.root_request)
                    result = ch1.check()
                    if not result.is_match:
                        return result
                    result = ch2.check()
                    if not result.is_match:
                        return result
            elif self.expected_type == tuple:
                # number of elements in the tuple must match args
                if len(self.obj) != len(self.type_args):
                    return TypeCheckResult(
                        reason=f"Tuple spec has types {self.type_args}, "
                               f"but object only {self.obj} has {len(self.obj)} elements",
                        request=self.root_request
                    )
                for o, t in zip(self.obj, self.type_args):
                    ch = TypeChecker(obj=o, type_annotation=t, root_request=self.root_request)
                    result = ch.check()
                    if not result.is_match:
                        return result
            elif self.expected_type == typing.Union:
                for candidate_type in self.type_args:
                    ch = TypeChecker(obj=self.obj, type_annotation=candidate_type, root_request=self.root_request)
                    result = ch.check()
                    if result.is_match:
                        return self.match
                # at this point: no candidate was matching
                return TypeCheckResult(
                    reason=f"No candidate type of {self.type_annotation} "
                           f"was matching object {self.obj} of type {type(self.obj)}",
                    request=self.root_request
                )
            else:
                raise Exception(f"Can't check unsupported expected type {self.expected_type}")

        return self.match


# --------------------------------------------------------------------------


class TypeCheckerTest(unittest.TestCase):
    def test_match__int_int(self):
        ch = TypeChecker(obj=1, type_annotation='int')
        self.assertEqual(ch.check(), ch.match)

    def test_mismatch__int_float(self):
        ch = TypeChecker(obj=1, type_annotation='float')
        self.assertNotEqual(ch.check(), ch.match)

    def test_mismatch__int_str(self):
        ch = TypeChecker(obj=1, type_annotation='str')
        self.assertNotEqual(ch.check(), ch.match)

    def test_match__optionalInt(self):
        ch = TypeChecker(obj=1, type_annotation='Optional[int]')
        self.assertEqual(ch.check(), ch.match)

        ch = TypeChecker(obj=None, type_annotation='Optional[int]')
        self.assertEqual(ch.check(), ch.match)

    def test_mismatch__optionalInt(self):
        ch = TypeChecker(obj='a', type_annotation='Optional[int]')
        self.assertNotEqual(ch.check(), ch.match)

    def test_match__list_list(self):
        ch = TypeChecker(obj=[1], type_annotation='List')
        self.assertEqual(ch.check(), ch.match)

    def test_match__listOfTuple(self):
        ch = TypeChecker(obj=[(1, 2)], type_annotation='List[Tuple[int, int]]')
        self.assertEqual(ch.check(), ch.match)

    def test_mismatch__tuple_listOfTuple(self):
        ch = TypeChecker(obj=(1, 2), type_annotation='List[Tuple[int, int]]')
        self.assertNotEqual(ch.check(), ch.match)

    def test_match__tuple_length(self):
        ch = TypeChecker(obj=(1,), type_annotation='Tuple[int]')
        self.assertEqual(ch.check(), ch.match)

    def test_mismatch__tuple_length(self):
        ch = TypeChecker(obj=(1,), type_annotation='Tuple[int, int]')
        self.assertNotEqual(ch.check(), ch.match)

    def test_mismatch__tuple_type(self):
        ch = TypeChecker(obj=(1,), type_annotation='Tuple[str]')
        self.assertNotEqual(ch.check(), ch.match)

    def test_match__listOfTupleOfInt(self):
        ch = TypeChecker(obj=[(1, 2)], type_annotation='List[Tuple[int, int]]')
        self.assertEqual(ch.check(), ch.match)

    def test_mismatch__listOfInt_listOfTuple(self):
        ch = TypeChecker(obj=[1, 2], type_annotation='List[Tuple[int, int]]')
        self.assertNotEqual(ch.check(), ch.match)

    def test_match__dictOfStrToInt(self):
        ch = TypeChecker(obj={'k1': 1}, type_annotation='Dict[str, int]')
        self.assertEqual(ch.check(), ch.match)

    def test_mismatch__dictOfStrToStr_dictOfStrToInt(self):
        ch = TypeChecker(obj={'k1': 'v1'}, type_annotation='Dict[str, int]')
        self.assertNotEqual(ch.check(), ch.match)

    def test_match__unionOfIntAndStr(self):
        ch = TypeChecker(obj=1, type_annotation='Union[int, str]')
        self.assertEqual(ch.check(), ch.match)

        ch = TypeChecker(obj='a', type_annotation='Union[int, str]')
        self.assertEqual(ch.check(), ch.match)

    def test_mismatch__unionOfIntAndStr(self):
        ch = TypeChecker(obj=1.0, type_annotation='Union[int, str]')
        self.assertNotEqual(ch.check(), ch.match)

        ch = TypeChecker(obj=[1], type_annotation='Union[int, str]')
        self.assertNotEqual(ch.check(), ch.match)

    def test_match__complex(self):
        complex_annotation = 'Union[Tuple[Dict[str, int], List[List[float]]], int]'

        ch = TypeChecker(obj=1, type_annotation=complex_annotation)
        self.assertEqual(ch.check(), ch.match)

        ch = TypeChecker(obj=({}, []), type_annotation=complex_annotation)
        self.assertEqual(ch.check(), ch.match)

        ch = TypeChecker(obj=({'a': 2}, [[1.0]]), type_annotation=complex_annotation)
        self.assertEqual(ch.check(), ch.match)

    def test_mismatch__complex(self):
        complex_annotation = 'Union[Tuple[Dict[str, int], List[List[float]]], int]'

        ch = TypeChecker(obj=(1,), type_annotation=complex_annotation)
        self.assertNotEqual(ch.check(), ch.match)

        ch = TypeChecker(obj='a', type_annotation=complex_annotation)
        self.assertNotEqual(ch.check(), ch.match)

        ch = TypeChecker(obj=[1], type_annotation=complex_annotation)
        self.assertNotEqual(ch.check(), ch.match)

