import abc
from dataclasses import dataclass
import os
import re
import shutil
import sys
from typing import *

import bag.core
from bag.design import Module
from bag.interface.script_renderer import ScriptRenderer

from sal.env import Env
from sal.log import warn
from sal.testbench_params import TestbenchParamsBase


@dataclass
class DUTWrapper:
    template_cell: str
    schematic_generator_class: Type[Module]


class TestbenchBase(ScriptRenderer, metaclass=abc.ABCMeta):
    def __init__(self, *arg):
        """ Overload if you need to refine this
        """
        pass

    @property
    def _classfile(self):
        """
        Find the path of the subclass python file, add the subclass name to the containing directory
        f.ex.
            subclass script: <path>/virtuoso_template/inverter2_gen/inverter2_gen/inverter2_gen.py
            _classfile returns: <path>/virtuoso_template/inverter2_gen/inverter2_gen/inverter2_gen
        """
        path_of_subclass = os.path.dirname(os.path.abspath(sys.modules[self.__class__.__module__].__file__))
        subclass_name = self.__class__.__name__
        return os.path.join(path_of_subclass, subclass_name)

    def print_log(self, **kwargs):
        # TODO: extract log from DesignBase to be more reusable
        type = kwargs.get('type', 'I')
        msg = kwargs.get('msg', 'Print this to the log')
        print(msg)

    def render_schematic_generator_script(self,
                                          lib_name: str,
                                          cell_name: str) -> str:
        """
        Callback for DbAccess to render the BagModules schematic generator script
        for newly imported schematics.
        As we use generator/testbench packages, we only import the schematic generator located in the package.

        Parameters
        ----------
        lib_name : str
            DB Library name
        cell_name
            DB Cell name

        Returns
        -------
        Rendered script as a string
        """
        script = f"from {self.package}.schematic import schematic as {lib_name}__{cell_name}"
        return script

    def import_testbench_template(self,
                                  bag_project: bag.core.BagProject,
                                  template_library: str,
                                  cell: str):
        """
        Method to import Virtuoso templates to BAG environment
        """

        # Import the templates
        self.print_log(msg='Importing netlist from virtuoso\n')

        # Path definitions
        new_lib_path = bag_project.bag_config['new_lib_path']

        # NOTE: we now have an absolute new_lib_path set up in bag_config.yaml
        output_template_lib_path = os.path.abspath(os.path.join(new_lib_path, template_library))
        packagename = os.path.join(output_template_lib_path, cell + '.py')

        if not os.path.isfile(packagename):
            os.makedirs(output_template_lib_path, exist_ok=True)
            self.print_log(msg="output template library path: %s" % output_template_lib_path)

        # Importing template library
        bag_project.import_design_library(template_library, custom_script_renderer=self)

        self.print_log(msg='Netlist import done')


@dataclass
class TestbenchSetup:
    """
    Testbench setup for a specific generator.
    """
    testbench_name: str
    testbench_class: Type[TestbenchBase]
    dut_wrapper: Optional[DUTWrapper]


@dataclass
class TestbenchRun:
    """
    Run of a testbench
    """
    setup: TestbenchSetup
    params: TestbenchParamsBase
    instance: Optional[TestbenchBase] = None
