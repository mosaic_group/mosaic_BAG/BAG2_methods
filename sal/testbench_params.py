from __future__ import annotations  # allow class type hints within same class
from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import Enum
from typing import *

from sal.env import Env
from sal.params_base import ParamsBase
from .simulation.simulation_scalar import SimulationScalar
from .simulation.simulation_sweep import SimulationSweepBase
from .simulation.simulation_output import SimulationOutputBase


@dataclass
class DCSignalSource(ParamsBase):
    source_name: str
    plus_net_name: str
    minus_net_name: str
    bias_value: Union[str, float]
    cdf_parameters: Dict[str, Any]

    def to_bag_dict(self) -> Dict[str, Any]:
        """
        Returns
        -------
        a dictionary prepared for calling bag.design.Module.design_dc_bias_sources()
        """
        return {
            self.source_name: [self.plus_net_name, self.minus_net_name, self.bias_value, self.cdf_parameters]
        }


@dataclass
class DUTTerminal(ParamsBase):
    term_name: str
    net_name: Union[str, List[str]]


@dataclass
class TestbenchSchematicParams(ParamsBase):
    dut_conns: List[DUTTerminal]
    v_sources: List[DCSignalSource]
    i_sources: List[DCSignalSource]
    instance_cdf_parameters: Dict[str, Dict]

    def to_bag_vbias_dict(self) -> Dict[str, List[str]]:
        vbias_dict = {}
        for v_source in self.v_sources:
            vbias_dict.update(v_source.to_bag_dict())
        return vbias_dict

    def to_bag_ibias_dict(self) -> Dict[str, List[str]]:
        ibias_dict = {}
        for i_source in self.i_sources:
            ibias_dict.update(i_source.to_bag_dict())
        return ibias_dict


@dataclass
class TestbenchSimulationParams(ParamsBase):
    """

    """
    variables: Dict[str, SimulationScalar]
    sweeps: Dict[str, SimulationSweepBase]
    outputs: Dict[str, SimulationOutputBase]


@dataclass
class DUT(ParamsBase):
    lib: str
    cell: str

    @classmethod
    def placeholder(cls) -> DUT:
        """
        The DUT is part of the testbench parameters,
        but set up by the framework depending on the generated library.
        Therefor a placeholder is used for the default parameters.
        """
        return DUT(lib="lib", cell="cell")


@dataclass
class TestbenchParamsBase(ParamsBase):
    dut: DUT
    dut_wrapper_params: Optional[Any]
    sch_params: TestbenchSchematicParams
    simulation_params: TestbenchSimulationParams

    @classmethod
    @abstractmethod
    def builtin_outputs(cls) -> Dict[str, SimulationOutputBase]:
        pass

    def __post_init__(self):
        merged_outputs = self.builtin_outputs()
        merged_outputs.update(self.simulation_params.outputs)
        self.simulation_params.outputs = merged_outputs

    def effective_simulation_params(self) -> TestbenchSimulationParams:
        blacklist = {'dut', 'sch_params', 'dut_wrapper_params', 'simulation_params'}
        remainder = dict(filter(lambda e: e[0] not in blacklist, self.__dict__.items()))
        variables = {}
        variables.update(**remainder)
        if self.dut_wrapper_params:
            variables.update(**self.dut_wrapper_params)
        variables.update(**self.simulation_params.variables)

        return TestbenchSimulationParams(variables=variables,
                                         sweeps=self.simulation_params.sweeps,
                                         outputs=self.simulation_params.outputs)
