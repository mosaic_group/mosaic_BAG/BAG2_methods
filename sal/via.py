#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from enum import Enum
from typing import *
import unittest


# -------------------------------------- Enums -------------------------------------

class ViaDirection(int, Enum):
    """
    How to align the transistor within the column/stack.
    """
    TOP = 1
    BOTTOM = -1


# -------------------------------------- Unit Tests -------------------------------------

# None yet
