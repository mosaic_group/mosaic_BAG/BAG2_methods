#This script perfoms the required string replacements in every generator submodule
#when doing the class migration from bag_ecd to BAG2_methods
# Source this to update your references, but remember to commit and psuh the changes.

sed -i 's/from bag_ecd import bag_startup/from BAG2_methods.bag_startup import bag_startup/g' \
    `find ./ -name \*.py -exec grep -l 'from bag_ecd import bag_startup' {} \;`
sed -i 's/from bag_ecd.bag_design import bag_design/from BAG2_methods.bag_design import bag_design/g' \
    `find ./ -name \*.py -exec grep -l 'from bag_ecd.bag_design import bag_design' {} \;`

